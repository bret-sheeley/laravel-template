<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

$applicationMode = request()->header('application-mode');
if ($applicationMode) {
    define('APPLICATION_MODE', $applicationMode);
}

// Routes without authentication token checks
Route::middleware(['exceptions'])->group(function () {
    Route::prefix('v1')->group(function () {

        Route::post('auth/login', 'AuthController@loginAction');
        Route::post('auth/request-password-reset', 'AuthController@requestPasswordResetAction');
        Route::post('auth/reset-password', 'AuthController@resetPasswordAction');

        Route::post('user/create', 'UserController@createAction');
    });
});

// Routes with authentication token checks
Route::middleware(['auth'])->group(function () {
    Route::prefix('v1')->group(function () {
        Route::any('auth/logout', 'AuthController@logoutAction');
        Route::any('user/change-password', 'UserController@changePasswordAction');
    });
});
