# password blahblahblah => $2y$10$lTkPj6fGLFj25S7AKNKIdu9nGiVcZ6egAc6qCM2gcP0qXJNrG1GUS
insert into users 
(`id`, `email`, `password`, `first_name`, `last_name`)
values 
( '2', 'user_login_test@email.com', '$2y$10$lTkPj6fGLFj25S7AKNKIdu9nGiVcZ6egAc6qCM2gcP0qXJNrG1GUS', 'Bret', 'Sheeley');
