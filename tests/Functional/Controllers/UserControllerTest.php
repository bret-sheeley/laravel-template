<?php
namespace Tests\Functional\Controllers;

class UserControllerTest extends AbstractDatabaseTestCase
{
	/**
	 * /user/create
	 * 
	 * Ensure the user/create call properly adds the new user
	 * record to the user database.
	 */
	public function testCreate()
	{
		// create
		$email = 'carteeg@gmail.com';
		$password = '9pytholgih34w9ogh4ouhwe98fh4ufh4fh9fh498h4f.3984t349hf298pfj239j';
		$firstName = 'Bob';
		$lastName = 'NotBob';
		
		// run test
		$params = [
			'email' => $email,
			'password' => $password,
		    'firstName' => $firstName,
		    'lastName' => $lastName
		];
		$response = $this->callApi('v1/user/create', $params);

		// Get response
        $this->assertArrayHasKey('token', $response);

		// get table
		$conn = $this->getApplicationDatabase();
		
		foreach ($conn->query('SELECT * FROM users') as $row) {
			$userResults = $row;
		}
		
		// make assertions
		$expectedUserResults = array(
			'email' => $email,
            'password' => $password,
            'firstName' => $firstName,
            'lastName' => $lastName
		);
		
		$this->assertArrayHasKey('id', $userResults);
	}
}
