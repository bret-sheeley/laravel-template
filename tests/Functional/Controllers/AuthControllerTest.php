<?php
namespace Tests\Functional\Controllers;

class AuthControllerTest extends AbstractDatabaseTestCase
{
    /**
     * /auth/login
     */
    public function testLogin()
    {
        $this->loadFixture('user_login');

        $email = 'user_login_test@email.com';
        $password = 'blahblahblah'; // hashes to $2y$10$lTkPj6fGLFj25S7AKNKIdu9nGiVcZ6egAc6qCM2gcP0qXJNrG1GUS

        $params = [
            'email' => $email,
            'password' => $password
        ];

        // run test
        $response = $this->callApi('v1/auth/login', $params);

        if (isset($response['error'])) {
            echo $response['error'];
            return;
        }

        // assert
        $this->assertArrayHasKey('token', $response);
    }

    /**
     * /auth/logout
     */
    public function testLogout()
    {
        $response = $this->loginUser();

        $headers = [
            'Authorization' => 'Bearer ' . $response['token']
        ];

        $response = $this->callAPI('v1/auth/logout', [], $headers);

        $this->assertEquals(1, $response['status']);
    }

}