<?php
namespace Tests\Functional\Controllers;

if (!defined('ROOT_PATH')) {
    define('ROOT_PATH', realpath(__dir__ . '/../../../'));
}
if (!defined('APP_PATH')) {
	define('APP_PATH', realpath(ROOT_PATH . '/app'));
}
if (!defined('TEST_PATH')) {
    define('TEST_PATH', realpath(ROOT_PATH . '/Tests'));
}

use Dotenv\Dotenv;
use PDO;

use App\Exceptions\{
    AppException
};

use PHPUnit\DbUnit\{
    TestCase,
    DataSet\CompositeDataSet
};


//abstract class AbstractDatabaseTestCase extends \PHPUnit_Extensions_Database_TestCase
abstract class AbstractDatabaseTestCase extends TestCase
{
	const DOUBLE_DELTA_RANGE = 0.000001;
	
    private static $pdo = [];
    private $conn = [];

    /**
     * initialize app
     */
    protected function initializeApp()
    {
    	$bootstrapDir = ROOT_PATH . '/bootstrap/';
    	require_once $bootstrapDir . 'autoload.php';
    	$app = require_once $bootstrapDir . 'app.php';

    	ob_start();
    	$kernel = $app->make(\Illuminate\Contracts\Http\Kernel::class);
    	$kernel->handle(
    		$request = \Illuminate\Http\Request::capture()
    	);
    	ob_end_clean();
    }

    /**
     * /user/login
     */
    public function loginUser()
    {
    	$this->loadFixture('user_login');

    	$email = 'user_login_test@email.com';
    	$password = 'blahblahblah'; // hashes to $2y$10$lTkPj6fGLFj25S7AKNKIdu9nGiVcZ6egAc6qCM2gcP0qXJNrG1GUS

    	$params = [
			'email' => $email,
			'password' => $password
    	];

    	// run test
    	$response = $this->callAPI('v1/auth/login', $params);

    	if (isset($response['error'])) {
    		echo $response['error'];
    		return;
    	}

    	return $response;
    }

    /**
     * getApplicationDatabase
     */
    public function getApplicationDatabase()
    {
    	return $this->getConnection()->getConnection();
    }

    /**
     * getConnection
     */
    public function getConnection()
    {
    	$config = require(ROOT_PATH . '/config/database.php');
    	
    	$testDatabaseConnection = env('DB_TEST_CONNECTION');
    	$dbSettings = $config['connections'][$testDatabaseConnection];
    	$database = $dbSettings['database'];

		$dotenv = new Dotenv(ROOT_PATH);
		$dotenv->load();

		$dbHostname = env('DB_HOST');
		$dbUsername = env('DB_ROOT_USERNAME');
		$dbPassword = env('DB_ROOT_PASSWORD');


		if (!array_key_exists($database, self::$pdo)) {
            self::$pdo[$database] = new PDO($dbSettings['driver'] . ':host=' . $dbHostname, $dbUsername,  $dbPassword);
        }
        $this->conn[$database] = $this->createDefaultDBConnection(self::$pdo[$database], $database);

        return $this->conn[$database];
    }

    /**
     * getDataSet
     *
     * @param array $fixtures
     * @return CompositeDataSet
     */
    public function getDataSet($fixtures = array())
    {
	    if (empty($fixtures) && property_exists($this, 'fixtures')) {
	        $fixtures = $this->fixtures;
	    }
	    $compositeDs = new CompositeDataSet([]);
	    $fixturePath = TEST_PATH . '/Functional/Fixtures';

	    foreach ($fixtures as $fixture) {
	        $path =  $fixturePath . '/' . $fixture . '.xml';
	        $ds = $this->createMySQLXMLDataSet($path);
	        $compositeDs->addDataSet($ds);
	    }
	    return $compositeDs;
	}

	/**
	 * loadDataSet
	 */
	public function loadDataSet($dataSet)
	{
    	// set the new dataset
    	$this->getDatabaseTester()->setDataSet($dataSet);

    	// call setUp which adds the rows
    	$this->getDatabaseTester()->onSetUp();
	}
	
	/**
	 * block live db
	 */
	public function blockLiveDb()
	{
	    $dotenv = new Dotenv(ROOT_PATH);
	    $dotenv->load();

	    $testDatabaseConnection = env('DB_TEST_CONNECTION');
	    $liveDatabaseConnection = env('DB_CONNECTION');
	    
	    $config = require(ROOT_PATH . '/config/database.php');
	    
	    $dbTestDatabase = $config['connections'][$testDatabaseConnection]['database'];
	    $dbLiveDatabase = $config['connections'][$liveDatabaseConnection]['database'];
	    
	    if ($dbTestDatabase == $dbLiveDatabase) {
	        throw new AppException('DONT_RUN_TESTS_ON_APP_DATABASES', 'tests', 'Stuff');
	    }
	}

	/**
	 * Load Fixture
	 */
	public function loadFixture($fixtureName = null)
	{
		if (is_null($fixtureName)) {
			return;
		}

        $fixtureFile =  TEST_PATH . '/Functional/Fixtures/' . $fixtureName . '.sql';
		$pdo = $this->getConnection()->getConnection();
		$pdo = $this->getApplicationDatabase();

		try {
			$schema = file_get_contents($fixtureFile);
			$pdo->query($schema);

			$errorInfo = $pdo->errorInfo();
			if ($errorInfo[0] !== 0 && $errorInfo[0] !== '00000') {
				throw new \Exception('sql_error');
			}
		} catch (\Exception $e) {
			throw new AppException($e->getMessage(), 'Sql Fixure Error.');
		}
	}

    /**
     * setUpBeforeClass
     */
    public static function setUpBeforeClass(): void
    {
        $tests = new static();
        $tests->blockLiveDb();

        $pdo = $tests->getConnection()->getConnection();
        $pdo = $tests->getApplicationDatabase();

        $config = require(ROOT_PATH . '/config/database.php');
        $testDatabaseConnection = env('DB_TEST_CONNECTION');

        $dbTestDatabase = $config['connections'][$testDatabaseConnection]['database'];

        // clear test application database
        $sql = "drop database if exists $dbTestDatabase; "
            . "create database $dbTestDatabase; "
            . "use $dbTestDatabase; ";

        $statement = $pdo->prepare($sql);
        $statement->execute();

        // migrate test database (w/root privileges)
        $migrate = 'php artisan migrate --force --database=' . env('DB_TEST_CONNECTION');
        exec($migrate);
    }

    /**
     * Set Up
     *
     * Set up the databases and fixtures
     */
    public function setUp(): void
    {
        // Run PHPUnit SetUp
        parent::setUp();
    }

    /**
     * tearDown
     *
     * Clears out the fixtures and database
     */
    public function tearDown(): void
    {
        $this->blockLiveDb();

        $config = require(ROOT_PATH . '/config/database.php');
        $testDatabaseConnection = env('DB_TEST_CONNECTION');
        $dbTestDatabase = $config['connections'][$testDatabaseConnection]['database'];

        $excepts = ['migrations'];
        $tables = $this->getConnection()
            ->getConnection()
            ->query("SHOW FULL TABLES")
            ->fetchAll();

        $tableNames = [];

        $keys = array_keys($tables[0]);

        $keyName = $keys[0];
        $keyType = $keys[2];

        $conn = $this->getConnection()->getConnection();

        foreach ($tables as $name) {

            //if you don't want to truncate migrations
            if (in_array($name[$keyName], $excepts))
                continue;

            // truncate tables only
            if('BASE TABLE' !== $name[$keyType])
                continue;

            $statement = $conn->prepare('truncate ' . $name[$keyName]);
            $statement->execute();
        }

        parent::tearDown();
    }

    /**
     * tearDownAfterClass
     */
    public static function tearDownAfterClass()
    {
        $tests = new static();
        $tests->blockLiveDb();

        $pdo = $tests->getConnection()->getConnection();

        $config = require(ROOT_PATH . '/config/database.php');
        $testDatabaseConnection = env('DB_TEST_CONNECTION');
        $dbTestDatabase = $config['connections'][$testDatabaseConnection]['database'];

        $sql = 'drop database if exists ' . $dbTestDatabase . ';';

        $statement = $pdo->prepare($sql);
        $statement->execute();
    }

	/**
	 * Call Api
	 */
	public function callApi($route, $data = null, $newHeaders = [])
	{
		$headers = [
			'Content-Type: application/json',
			'Application-Mode: test'
		];

        // allow for headers to be formatted either:
        // [
        //   'Blah: blah/blah'
        // ]
        // or
        // [
        //   'Blah' => 'blah/blah'
        // ]
		foreach ($newHeaders as $type => $header) {
            $headers[] = (!is_numeric($type))
                ? $type . ': ' . $header
                : $header;
		}
		
		$urlHost = env('APP_URL', 'http://localhost');
		
		$url = sprintf($urlHost . "/api/%s", $route);
		
        $channel = curl_init();
		curl_setopt($channel, CURLOPT_HTTPHEADER, $headers);
	    curl_setopt($channel, CURLOPT_POST, true);
		curl_setopt($channel, CURLOPT_POSTFIELDS, json_encode($data));
		curl_setopt($channel, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($channel, CURLOPT_SSL_VERIFYHOST, 0);
		curl_setopt($channel, CURLOPT_SSL_VERIFYPEER, 0);
	    curl_setopt($channel, CURLOPT_TIMEOUT, 30);
	    curl_setopt($channel, CURLOPT_URL, $url);

		// make api call
	    $results = curl_exec($channel);
	    
	    curl_close($channel);

	    // see if results are in json or not
	    $jsonResults = json_decode($results, true);

	    return (is_null($jsonResults)) ? ['error' => $results] : $jsonResults;
	}

	/**
	 * Assert Within Delta
	 */
	public function assertDoubleEquals($expected, $testValue)
	{
		$absDelta = abs((double)$expected - (double)$testValue);

		$this->assertLessThan(self::DOUBLE_DELTA_RANGE, $absDelta);
	}

	/**
	 * Get Parameters
	 */
	public function getParameter($key, $response)
	{
		if (!array_key_exists($key, $response)) {
			throw new \Exception($key . ' not found in response: ' . print_r($response, true));
		}

		return $response[$key];
	}
}
?>