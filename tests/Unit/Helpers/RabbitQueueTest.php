<?php
namespace Tests\Unit\Helpers;

use Tests\TestCase;

class RabbitQueueTest extends TestCase
{
    /**
     * test: getQueue
     */
    public function testGetQueue()
    {
        // mocks
        $queueName = 'a_queue';
        $routeKey = 'a_route';

        $channel = 'a_channel';
        $exchangeName = 'an_exchange';

        $queue = $this->getMockBuilder('\stdClass')
            ->disableOriginalConstructor()
            ->setMethods(['setName', 'setFlags', 'declareQueue', 'bind'])
            ->getMock();
        $queue->expects($this->once())
            ->method('setName')
            ->with($this->equalTo($queueName));
        $queue->expects($this->once())
            ->method('setFlags')
            ->with($this->equalTo(AMQP_DURABLE));
        $queue->expects($this->once())
            ->method('declareQueue');
        $queue->expects($this->once())
            ->method('bind')
            ->with($this->equalTo($exchangeName),
                $this->equalTo($routeKey));

        $rabbitExchange = $this->getMockBuilder('\stdClass')
            ->disableOriginalConstructor()
            ->setMethods(['getChannel', 'getExchangeName'])
            ->getMock();
        $rabbitExchange->expects($this->once())
            ->method('getChannel')
            ->willReturn($channel);
        $rabbitExchange->expects($this->any())
            ->method('getExchangeName')
            ->willReturn($exchangeName);

        $rabbitQueue = $this->getMockBuilder('App\Helpers\RabbitQueue')
            ->setConstructorArgs([$queueName, $routeKey, $rabbitExchange, 'fakeQueue'])
            ->setMethods(['generateAmqpQueue'])
            ->getMock();
        $rabbitQueue->expects($this->once())
            ->method('generateAmqpQueue')
            ->with($this->equalTo($channel))
            ->willReturn($queue);

        // run
        $rabbitQueue->initQueue();

        // post-run assertions
        $this->assertEquals($queue, $rabbitQueue->getQueue());
    }
}