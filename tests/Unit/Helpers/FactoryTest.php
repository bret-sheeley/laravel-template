<?php
namespace Tests\Unit\Helpers;

use Tests\TestCase;

class FactoryTest extends TestCase
{
    /**
     * test: getDomain without args
     */
    public function testGetDomainWithoutArgs()
    {
        // param
        $domainName = 'User';

        // mock
        $factory = $this->getMockBuilder('App\Helpers\Factory')
            ->disableOriginalConstructor()
            ->setMethods(['dummy'])
            ->getMock();

        // run
        $results = $factory->getDomain($domainName);

        // post-run assertions
        $this->assertEquals('App\Models\Domains\User', get_class($results));
    }
    /**
     * test: getDomain with args
     */
    public function testGetDomainWithArgs()
    {
        // param
        $domainName = 'User';
        $dataset = ['id' => 321];

        // mock
        $factory = $this->getMockBuilder('App\Helpers\Factory')
            ->disableOriginalConstructor()
            ->setMethods(['dummy'])
            ->getMock();

        // run
        $results = $factory->getDomain($domainName, [$dataset]);

        // post-run assertions
        $this->assertEquals('App\Models\Domains\User', get_class($results));
        $this->assertEquals(321, $results->getId());
    }
}