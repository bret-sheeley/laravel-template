<?php
namespace Tests\Unit\Helpers;

use Tests\TestCase;

use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;

class LoggerTest extends TestCase
{
    /**
     * test: log
     */
    public function testLog()
    {
        // param
        $logLevel = LogLevel::EMERGENCY;
        $message = 'a message';
        $context = [
            'random' => 'data',
            'and' => 'stuff',
            'exception' => new \App\Exceptions\AppException('an_error', 'Blah')
        ];

        // mocks
        $now = 'NOW!!!';
        $date = '20180315';
        $hour = '2018031523';

        $time = $this->getMockBuilder('App\Helpers\Logger')
            ->disableOriginalConstructor()
            ->setMethods(['now', 'reformatTime'])
            ->getMock();
        $time->expects($this->at(0))
            ->method('now')
            ->willReturn($now);
        $time->expects($this->at(1))
            ->method('reformatTime')
            ->with($this->equalTo($now), $this->equalTo('Ymd'))
            ->willReturn($date);
        $time->expects($this->at(2))
            ->method('reformatTime')
            ->with($this->equalTo($now), $this->equalTo('YmdH'))
            ->willReturn($hour);

        $this->container->set('Time', $time);

        $dependencies = [
            'container' => $this->container
        ];

        $dir = '/a/directory/path';

        $log = $this->getMockBuilder('App\Helpers\Logger')
            ->setConstructorArgs([$dependencies])
            ->setMethods(['buildDateDirectory', 'write', 'isDebug'])
            ->getMock();
        $log->expects($this->once())
            ->method('buildDateDirectory')
            ->with($this->equalTo($date))
            ->willReturn($dir);
        $log->expects($this->any())
            ->method('isDebug')
            ->willReturn(false);
        $log->expects($this->once())
            ->method('write')
            ->with($this->equalTo(strtoupper($logLevel)),
                $this->equalTo($now),
                $this->equalTo('a message, {"random":"data","and":"stuff","exception":"an_error"}'),
                $this->equalTo('/a/directory/path/cerebus_2018031523.log'));

        // run
        $log->log($logLevel, $message, $context);
    }

    // ==================================
    //
    // Log Level Calls

    /**
     * test: logLevelCalls
     */
    public function testLogLevelCalls()
    {
        $logLevels = [
            'debug' => LogLevel::DEBUG,
            'info' => LogLevel::INFO,
            'notice' => LogLevel::NOTICE,
            'warning' => LogLevel::WARNING,
            'error' => LogLevel::ERROR,
            'critical' => LogLevel::CRITICAL,
            'alert' => LogLevel::ALERT,
            'emergency' => LogLevel::EMERGENCY
        ];

        foreach ($logLevels as $logLevel => $level) {

            // params
            $message = 'a_message';
            $context = ['an_array'];

            // mock
            $aPrependedMessage = 'blahblahblah:a_message';

            $log = $this->getMockBuilder('App\Helpers\Logger')
                ->disableOriginalConstructor()
                ->setMethods(['prependMethodToMessage', 'log'])
                ->getMock();
            $log->expects($this->once())
                ->method('prependMethodToMessage')
                ->with($this->equalTo($message))
                ->willReturn($aPrependedMessage);
            $log->expects($this->once())
                ->method('log')
                ->with($this->equalTo($logLevel),
                    $this->equalTo($aPrependedMessage),
                    $this->equalTo($context));

            // run
            $log->$logLevel($message, $context);
        }

    }
}