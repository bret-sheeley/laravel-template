<?php
namespace Tests\Unit\Helpers;

use Tests\TestCase;

use App\Helpers\RabbitWrapper;

class RabbitWrapperTest extends TestCase
{
	/**
	 * Test: Publish Message
	 */
	public function testPublishMessage()
	{
		// params
		$exchangeName = 'exchange.name';
		$routeKey = 'route.key';
		$message = array('data' => 'and stuff');
		
		// mock
		$foundExchange = $this->getMockBuilder('\App\Helpers\RabbitExchange')
			->setMethods(array('publish'))
			->disableOriginalConstructor()
			->getMock();
		$foundExchange->expects($this->once())
			->method('publish')
			->with(
				$this->equalTo($message),
				$this->equalTo($routeKey)
			);
		
		$rabbitWrapper = $this->getMockBuilder('\App\Helpers\RabbitWrapper')
			->setMethods(array('getExchange'))
			->disableOriginalConstructor()
			->getMock();
		$rabbitWrapper->expects($this->once())
			->method('getExchange')
			->with($this->equalTo($exchangeName))
			->willReturn($foundExchange);
			
		// run test
		$rabbitWrapper->publishMessage($exchangeName, $routeKey, $message);
	}
	
	/**
	 * Test: Run Subscriber for Emails
	 */
	public function testRunSubscriberForEmails()
	{
		// param
		$queueName = 'email';
		
		// mocks
		$handlerName = 'EmailHandler';
		$failureRouteKey = 'email.failure';
		
		$foundQueue = $this->getMockBuilder('App\Helpers\RabbitQueue')
			->setMethods(['run'])
			->disableOriginalConstructor()
			->getMock();
		$foundQueue->expects($this->once())
			->method('run')
			->with(
				$this->equalTo($handlerName),
				$this->equalTo($failureRouteKey)
			);
			
		$rabbitWrapper = $this->getMockBuilder('App\Helpers\RabbitWrapper')
			->setMethods(array('getQueue'))
			->disableOriginalConstructor()
			->getMock();
		$rabbitWrapper->expects($this->once())
			->method('getQueue')
			->with($this->equalTo($queueName))
			->willReturn($foundQueue);
			
		// run test
		$rabbitWrapper->runSubscriber($queueName);
	}
	
	/**
	 * Test: Run Subscriber for Emails Failure
	 */
	public function testRunSubscriberForEmailsFailure()
	{
		// param
		$queueName = 'email-failure';
		
		// mocks
		$handlerName = 'EmailHandler';
		$failureRouteKey = null;
		
		$foundQueue = $this->getMockBuilder('App\Helpers\RabbitQueue')
			->setMethods(array('run'))
			->disableOriginalConstructor()
			->getMock();
		$foundQueue->expects($this->once())
			->method('run')
			->with(
				$this->equalTo($handlerName),
				$this->equalTo($failureRouteKey)
			);
			
		$rabbitWrapper = $this->getMockBuilder('App\Helpers\RabbitWrapper')
			->setMethods(array('getQueue'))
			->disableOriginalConstructor()
			->getMock();
		$rabbitWrapper->expects($this->once())
			->method('getQueue')
			->with($this->equalTo($queueName))
			->willReturn($foundQueue);
			
		// run test
		$rabbitWrapper->runSubscriber($queueName);
	}
	
	/**
	 * Test: Get Exchange Returns Exchange
	 */
	public function testGetExchangeReturnsExchange()
	{
		// param
		$exchangeName = 'an_exchange_name';
		
		// mock	
		$exchange = $this->getMockBuilder('App\Helpers\RabbitExchange')
			->setMethods(array('dummy'))
			->disableOriginalConstructor()
			->getMock();
		
		$rabbitWrapper = $this->getMockBuilder('App\Helpers\RabbitWrapper')
			->setMethods(array('createExchange'))
			->disableOriginalConstructor()
			->getMock();
		$rabbitWrapper->expects($this->once())
			->method('createExchange')
			->with($this->equalTo($exchangeName))
			->willReturn($exchange);
			
		// run test
		$results = $rabbitWrapper->getExchange($exchangeName);
		
		// post-run assertions
		$this->assertEquals($exchange, $results);
	}
	
	/**
	 * Test: getQueue Returns Rabbit Queue
	 */
	public function testGetQueueReturnsQueue()
	{
		// param
		$queueName = 'a_queue';
		
		// mock	
		$queue = $this->getMockBuilder('\App\Helpers\RabbitQueue')
			->setMethods(array('dummy'))
			->disableOriginalConstructor()
			->getMock();
			
		$rabbitWrapper = $this->getMockBuilder('\App\Helpers\RabbitWrapper')
			->setMethods(array('createQueue'))
			->disableOriginalConstructor()
			->getMock();
		$rabbitWrapper->expects($this->once())
			->method('createQueue')
			->with($this->equalTo($queueName))
			->willReturn($queue);
			
		// run test
		$results = $rabbitWrapper->getQueue($queueName);
		
		// post-run assertions
		$this->assertEquals($queue, $results);
	}
}