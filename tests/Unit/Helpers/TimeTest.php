<?php
namespace Tests\Unit\Helpers;

use Tests\TestCase;

use App\Helpers\Time;

class TimeTest extends TestCase
{
	/**
	 * test: add adds seconds to given time
	 */
	public function testAddAddsSecondsToGivenTime()
	{
		$now = '2015-12-31 23:00:05';
		$secondsToAdd = 3600; 
		
		$time = $this->getMockBuilder('\App\Helpers\Time')
			->setMethods(array('dummy'))
			->disableOriginalConstructor()
			->getMock();
		
		$returnedTime = $time->add($secondsToAdd, $now);
		
		$expectedTime = '2016-01-01 00:00:05';
		$this->assertEquals($expectedTime, $returnedTime);
	}

	/**
	 * test: add adds seconds to now if no time is given
	 */
	public function testAddAddsSecondsToNowIfNoTimeIsGiven()
	{
		$now = '2015-12-31 23:00:06';
		$secondsToAdd = 3600; 
		
		$time = $this->getMockBuilder('\App\Helpers\Time')
			->disableOriginalConstructor()
			->setMethods(array('now'))
			->getMock();
			
		$time->expects($this->once())
			->method('now')
			->willReturn($now);
		
		$returnedTime = $time->add($secondsToAdd);
		
		$expectedTime = '2016-01-01 00:00:06';
		$this->assertEquals($expectedTime, $returnedTime);
	}
	
	/**
	 * test: addMonths
	 */
	public function testAddMonths()
	{
		$now = '2015-12-31 23:00:06';
		$monthsToAdd = 2; 
		
		$time = $this->getMockBuilder('\App\Helpers\Time')
			->disableOriginalConstructor()
			->setMethods(array('now'))
			->getMock();
			
		$time->expects($this->once())
			->method('now')
			->willReturn($now);
		
		$returnedTime = $time->addMonths($monthsToAdd);
		
		$expectedTime = '2016-02-29 23:00:06';
		$this->assertEquals($expectedTime, $returnedTime);
	}
	
	/**
	 * Test: Ago
	 */
	public function testAgo()
	{
		// param
		$sub = 86400;
		
		// mocks
		$now = '2014-01-01 12:00:00';
		
		$time = $this->getMockBuilder('\App\Helpers\Time')
			->disableOriginalConstructor()
			->setMethods(array('now'))
			->getMock();
		$time->expects($this->once())
			->method('now')
			->willReturn($now);
			
		// run test
		$results = $time->ago($sub);
		
		// post-run assertions
		$this->assertEquals('2013-12-31 12:00:00', $results);
	}
	
	/**
	 * test: until later
	 */
	public function testUntilLater()
	{
		// params
		$later = '2014-01-02 13:00:00';
		
		// mocks
		$now = '2014-01-01 12:00:00';
		$localTimeToday = '01:00';
		$timeZoneShift = -4;
		
		$time = $this->getMockBuilder('\App\Helpers\Time')
			->disableOriginalConstructor()
			->setMethods(array('now'))
			->getMock();
		$time->expects($this->once())
			->method('now')
			->willReturn($now);
			
		// run
		$results = $time->until($later);
		
		// post-run assertions
		$this->assertEquals(90000, $results);
	}
	
	/**
	 * test: until earlier returns zero
	 */
	public function testUntilEarlierReturnsZero()
	{
		// params
		$later = '2014-01-02 13:00:00';
		
		// mocks
		$now = '2014-03-01 12:00:00';
		
		$time = $this->getMockBuilder('\App\Helpers\Time')
			->disableOriginalConstructor()
			->setMethods(array('now'))
			->getMock();
		$time->expects($this->once())
			->method('now')
			->willReturn($now);
			
		// run
		$results = $time->until($later);
		
		// post-run assertions
		$this->assertEquals(0, $results);
	}

	/**
	 * test: reformat time
	 */
	public function testReformatTime()
	{
		// params
		$now = '2014-01-02 13:00:00';
		$format = 'y-m-d h a';
		
		// mocks
		$now = '2014-03-01 12:00:00';
		$localTimeToday = '01:00';
		$timeZoneShift = -4;
		
		$time = $this->getMockBuilder('\App\Helpers\Time')
			->disableOriginalConstructor()
			->setMethods(array('now'))
			->getMock();
			
		// run
		$results = $time->reformatTime($now, $format);
		
		// post-run assertions
		$this->assertEquals('14-03-01 12 pm', $results);
	}
	
	/**
	 * test: reformat time
	 */
	public function testFormatTime()
	{
		// params
		$unixtime = '1434552314';
		$format = 'y-m-d h a';
		
		// mocks
		$now = '2014-03-01 12:00:00';
		$localTimeToday = '01:00';
		$timeZoneShift = -4;

		$time = $this->getMockBuilder('\App\Helpers\Time')
			->disableOriginalConstructor()
			->setMethods(array('now'))
			->getMock();
			
		
		// run
		$results = $time->formatTime($unixtime, $format);
		
		// post-run assertions
		$this->assertEquals('15-06-17 02 pm', $results);
	}
	
	/**
	 * test: to Last Mod
	 */
	public function testToLastMod()
	{
		$now = '2014-01-01 12:15:00';
		
		$time = $this->getMockBuilder('\App\Helpers\Time')
			->setMethods(array('now'))
			->disableOriginalConstructor()
			->getMock();
		$time->expects($this->any())
			->method('now')
			->willReturn($now);
			
		$this->assertEquals('2014-01-01 12:00:00', $time->toLastMod(1800));
		$this->assertEquals('2014-01-01 12:15:00', $time->toLastMod(900));
		$this->assertEquals('2014-02-04 18:00:00', $time->toLastMod(900, '2014-02-04 18:13:40'));
	}
	
	/**
	 * test: get end of month timestamp
	 */
	public function testGetEndOfMonthTimestamp()
	{
		$time = $this->getMockBuilder('\App\Helpers\Time')
			->setMethods(array('now'))
			->disableOriginalConstructor()
			->getMock();
			
		$this->assertEquals('2001-01-31 23:59:59', $time->getEndOfMonthTimestamp(1,1));
		$this->assertEquals('2012-08-31 23:59:59', $time->getEndOfMonthTimestamp(8,12));
		$this->assertEquals('2042-06-30 23:59:59', $time->getEndOfMonthTimestamp(6,42));
		$this->assertEquals('2013-02-28 23:59:59', $time->getEndOfMonthTimestamp(2,13));
		$this->assertEquals('2016-02-29 23:59:59', $time->getEndOfMonthTimestamp(2,16));
		$this->assertEquals('2016-02-29 23:59:59', $time->getEndOfMonthTimestamp(2,2016));
	}
	
	/**
	 * test: sub returns difference of time
	 */
	public function testSubReturnsDifferenceOfTime()
	{
		// param
		$time1 = '2015-02-01 01:00:01';
		$time2 = '2015-01-31 23:59:59';
		
		// mock
		$time = $this->getMockBuilder('\App\Helpers\Time')
			->disableOriginalConstructor()
			->setMethods(array('now'))
			->getMock();
		$time->expects($this->never())
			->method('now');
		
		// run
		$results = $time->sub($time1, $time2);
		
		// post-run assertion
		$expectedDelta = 3602;
		$this->assertEquals($expectedDelta, $results);
	}
	
	/**
	 * test: sub returns uses now for first missing time
	 */
	public function testSubUsesNowForFirstMissingTime()
	{
		// param
		$time2 = '2015-01-31 23:59:59';
		
		// mock
		$time1 = '2015-02-01 01:00:01';
		
		$time = $this->getMockBuilder('\App\Helpers\Time')
			->disableOriginalConstructor()
			->setMethods(array('now'))
			->getMock();
		$time->expects($this->once())
			->method('now')
			->willReturn($time1);
		
		// run
		$results = $time->sub(null, $time2);
		
		// post-run assertion
		$expectedDelta = 3602;
		$this->assertEquals($expectedDelta, $results);
	}
	
	/**
	 * test: sub returns uses now for second missing time
	 */
	public function testSubUsesNowForSecondMissingTime()
	{
		// param
		$time1 = '2015-02-01 01:00:01';
		
		// mock
		$time2 = '2015-01-31 23:59:59';
		
		$time = $this->getMockBuilder('\App\Helpers\Time')
			->disableOriginalConstructor()
			->setMethods(array('now'))
			->getMock();
		$time->expects($this->once())
			->method('now')
			->willReturn($time2);
		
		// run
		$results = $time->sub($time1);
		
		// post-run assertion
		$expectedDelta = 3602;
		$this->assertEquals($expectedDelta, $results);
	}
	
	/**
	 * test: sub returns uses now for both missing times
	 */
	public function testSubUsesNowForBothMissingTimes()
	{
		// mock
		$time1 = '2015-01-31 23:59:59';
		
		$time = $this->getMockBuilder('\App\Helpers\Time')
			->disableOriginalConstructor()
			->setMethods(array('now'))
			->getMock();
		$time->expects($this->once())
			->method('now')
			->willReturn($time1);
		
		// run
		$results = $time->sub();
		
		// post-run assertion
		$expectedDelta = 0;
		$this->assertEquals($expectedDelta, $results);
	}
	
	/**
	 * test: getDateWithoutString
	 */
	public function testGetDateWithoutString()
	{
		// param
		$format = 'Y-m-d H:i:s';
		
		// mock
		$time = $this->getMockBuilder('\App\Helpers\Time')
			->disableOriginalConstructor()
			->setMethods(array('dummy'))
			->getMock();
		
		// run
		$results = $time->getDate($format);
		
		// post-run assertion
		$expectedResults = date($format);
		$this->assertEquals($expectedResults, $results);
	}
	
	/**
	 * test Get Date with string
	 */
	public function testGetDateWithString()
	{
		// param
		$format = 'Y-m-d H:i:s';
		$string = 'first day of previous month';
		
		// mock
		$time = $this->getMockBuilder('\App\Helpers\Time')
			->disableOriginalConstructor()
			->setMethods(array('dummy'))
			->getMock();
		
		// run
		$results = $time->getDate($format, $string);
		
		// post-run assertion
		$expectedResults = date($format, strtotime($string));
		$this->assertEquals($expectedResults, $results);
	}
	
	/**
	 * test: convertToUnixtime
	 */
	public function testConvertToUnixtime()
	{
		// param
		$timestamp = '2015-06-01 12:00:13';
		
		// mock
		$time = $this->getMockBuilder('\App\Helpers\Time')
			->disableOriginalConstructor()
			->setMethods(array('dummy'))
			->getMock();
		
		// run
		$results = $time->convertToUnixtime($timestamp);
		
		// post-run assertion
		$expectedResults = 1433160013;
		$this->assertEquals($expectedResults, $results);
	}

	// ==========================
    //
    // unixtime <--> mysql datetime

    /**
     * test: unixtimeToMysqlDatetime
     */
    public function testUnixtimeToMysqlDatetime()
    {
        $time = new Time();

        $unixtime = 1234567890;
        $results = $time->unixtimeToMysqlDatetime($unixtime);

        $this->assertEquals('2009-02-13 23:31:30', $results);
    }

    /**
     * test: mysqlDatetimeToUnixtime
     */
    public function testMysqlDatetimeToUnixtime()
    {
        $time = new Time();

        $mysqlDatetime = '2009-02-13 23:31:30';
        $results = $time->mysqlDatetimeToUnixtime($mysqlDatetime);

        $this->assertEquals(1234567890, $results);
    }

    // ===========================
    //
    // get month name

    /**
     * test: getMonthName
     */
    public function testGetMonthName()
    {
        $time = new Time();
        $results = $time->getMonthName(3);

        $this->assertEquals('March', $results);
    }

    // ===========================
    //
    // convertFromUnixtime

    /**
     * test: convertFromUnixtime
     */
    public function testConvertFromUnixtime()
    {
        $time = new Time();
        $results = $time->convertFromUnixtime(1234567890, 'Y m d H:i:s');

        $this->assertEquals('2009 02 13 23:31:30', $results);
    }
}