<?php
namespace Tests\Unit\Helpers;

use Tests\TestCase;
use App\Helpers\DependencyContainer;
use App\Exceptions\NotFoundException;
use App\Helpers\Time;

class DependencyContainerTest extends TestCase
{
    /**
     * test: set and clear
     */
    public function testSetAndClear()
    {
        // init
        $aString = 'a_string';
        $container = new DependencyContainer();
        $container->set('Something', $aString);

        // case #1: set
        $results = $container->get('Something');
        $this->assertEquals('a_string', $results);

        // case #2: clear
        $container->clear();
        try {
            $container->get('Something');
            $results = false;
        } catch (NotFoundException $e) {
            $results = true;
        }
        $this->assertTrue($results);
    }

    /**
     * test: has
     */
    public function testHas()
    {
        $container = new DependencyContainer();
        $this->assertFalse($container->has('Something'));

        $aString = 'a_string';
        $container->set('Something', $aString);
        $this->assertTrue($container->has('Something'));
    }

    // =================================
    //
    // initialize
    //

    /**
     * test: initialize finds helpers
     */
    public function testInitializeFindsHelpers()
    {
        $container = new DependencyContainer();

        $time = $container->get('Time');
        $this->assertEquals('App\Helpers\Time', get_class($time));
    }

    /**
     * test: initialize finds domains
     */
    public function testInitializeFindsDomains()
    {
        $container = new DependencyContainer();

        $time = $container->get('User');
        $this->assertEquals('App\Models\Domains\User', get_class($time));
    }

    /**
     * test: initialize finds mappers
     */
    public function testInitializeFindsMappers()
    {
        $container = new DependencyContainer();

        $time = $container->get('UserMapper');
        $this->assertEquals('App\Models\Mappers\UserMapper', get_class($time));
    }

    /**
     * test: initialize finds services
     */
    public function testInitializeFindsServices()
    {
        $container = new DependencyContainer();

        $time = $container->get('UserService');
        $this->assertEquals('App\Services\UserService', get_class($time));
    }

    /**
     * test: set with dependencies
     */
    public function testSetWithDependencies()
    {
        $container = new DependencyContainer();

        $time = new Time();
        $container->set('Time', $time, [ 'something' => 'fake_value' ]);

        $newTime = $container->get('Time');
        $this->assertEquals('App\Helpers\Time', get_class($newTime));
    }
}