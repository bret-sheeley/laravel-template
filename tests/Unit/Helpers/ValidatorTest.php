<?php
namespace Tests\Unit\Helpers;

use Tests\TestCase;

class ValidatorTest extends TestCase
{
    // ===============================
    //
    // validate email

    /**
     * test: validateEmail throws exception if given invalid email
     *
     * @expectedException App\Exceptions\AppException
     * @expectedExceptionMessage invalid_email
     */
    public function testValidateEmailThrowsExceptionIfGivenInvalidEmail()
    {
        // param
        $email = 'hi$email.com';

        // run
        $validator = new \App\Helpers\Validator();
        $validator->validateEmail($email);
    }

    /**
     * test: validateEmail does nothing if given valid email
     */
    public function testValidateEmailDoesNothingIfGivenValidEmail()
    {
        // param
        $email = 'hi@email.com';

        // run
        $validator = new \App\Helpers\Validator();
        $validator->validateEmail($email);

        $this->assertTrue(true);
    }

    // ========================================
    //
    // validate name


    /**
     * test: validateName throws exception if given invalid name
     *
     * @expectedException App\Exceptions\AppException
     * @expectedExceptionMessage invalid_name
     */
    public function testValidateNameThrowsExceptionIfGivenInvalidName()
    {
        // param
        $name = 'x';

        // run
        $validator = new \App\Helpers\Validator();
        $validator->validateName($name);
    }

    /**
     * test: validateName does nothing if given valid name
     */
    public function testValidateNameDoesNothingIfGivenValidName()
    {
        // param
        $name = 'ed';

        // run
        $validator = new \App\Helpers\Validator();
        $validator->validateName($name);

        $this->assertTrue(true);
    }

    // ========================================
    //
    // validate password

    /**
     * test: validatePassword throws exception if given invalid password
     *
     * @expectedException App\Exceptions\AppException
     * @expectedExceptionMessage invalid_password
     */
    public function testValidatePasswordThrowsExceptionIfGivenInvalidPassword()
    {
        // param
        $password = '2short';

        // run
        $validator = new \App\Helpers\Validator();
        $validator->validatePassword($password);
    }

    /**
     * test: validatePassword does nothing if given valid password
     */
    public function testValidatePasswordDoesNothingIfGivenValidPassword()
    {
        // param
        $password = 'long_enough';

        // run
        $validator = new \App\Helpers\Validator();
        $validator->validatePassword($password);

        $this->assertTrue(true);
    }

}