<?php
namespace Tests\Unit\Helpers;

use App\Helpers\BaseHelper;
use Tests\TestCase;

class BaseHelperTest extends TestCase
{
    /**
     * test: getApplicationMode
     */
    public function testGetApplicationMode()
    {
        // init
        $mode = defined('APPLICATION_MODE') ? APPLICATION_MODE : null;

        // run
        $baseHelper = new BaseHelper();
        $results = $baseHelper->getApplicationMode();

        // post-run assertions
        $this->assertEquals($mode, $results);
    }

    /**
     * test: isTestMode returns false if not in test mode
     */
    public function testIsTestModeReturnsFalseIfNotInTestMode()
    {
        $baseHelper = $this->getMockBuilder('App\Helpers\BaseHelper')
            ->disableOriginalConstructor()
            ->setMethods(['getApplicationMode'])
            ->getMock();
        $baseHelper->expects($this->once())
            ->method('getApplicationMode')
            ->willReturn('not_test');
        $results = $baseHelper->isTestMode();

        // post-run assertions
        $this->assertFalse($results);
    }

    /**
     * test: isTestMode returns true if in test mode
     */
    public function testIsTestModeReturnsTrueIfInTestMode()
    {
        // run
        $baseHelper = $this->getMockBuilder('App\Helpers\BaseHelper')
            ->disableOriginalConstructor()
            ->setMethods(['getApplicationMode'])
            ->getMock();
        $baseHelper->expects($this->once())
            ->method('getApplicationMode')
            ->willReturn('test');
        $results = $baseHelper->isTestMode();

        // post-run assertions
        $this->assertTrue($results);
    }

    /**
     * test: getEnvironment
     */
    public function testGetEnvironment()
    {
        $appEnv = defined('APP_ENVIRONMENT') ? APP_ENVIRONMENT : null;

        // run
        $baseHelper = new BaseHelper();
        $results = $baseHelper->getEnvironment();

        // post-run assertion
        $this->assertEquals($appEnv, $results);

    }
}