<?php
namespace Tests\Unit\Helpers;

use Tests\TestCase;

use \Exception;
use \SendGrid;

use \App\Helpers\{
    EmailWrapper,
    ConfigWrapper,
    RabbitWrapper
};

class EmailWrapperTest extends TestCase
{
	/**
	 * test: queue without ccs
	 */
	public function testQueueWithoutCCs()
	{
		// param
		$to = 'email@email.com';
		$emailType = 'test';
		$data = [
			'key' => 'value'
		];

		// mock
		$rabbitWrapper = $this->getMockBuilder('\App\Helpers\RabbitWrapper')
			->disableOriginalConstructor()
			->setMethods(['publishMessage'])
			->getMock();
		$rabbitWrapper->expects($this->once())
			->method('publishMessage')
			->with(
				$this->equalTo(RabbitWrapper::APP_EXCHANGE),
				$this->equalTo(RabbitWrapper::EMAIL_ROUTE_KEY),
				$this->equalTo([
					'to' => $to,
					'email_type' => $emailType,
					'data' => $data
				])
			);

		$dependencies = (object) [
            'queue' => $rabbitWrapper
        ];

		$emailWrapper = $this->getMockBuilder('\App\Helpers\EmailWrapper')
			->setConstructorArgs([$dependencies])
			->setMethods(['dummy'])
			->getMock();

		// run
		$emailWrapper->queue($to, $emailType, $data);
	}

	/**
	 * test: queue without ccs
	 */
	public function testQueueWithoutCCsAndData()
	{
		// param
		$to = 'email@email.com';
		$emailType = 'test';

		// mock
		$rabbitWrapper = $this->getMockBuilder('\App\Helpers\RabbitWrapper')
			->disableOriginalConstructor()
			->setMethods(['publishMessage'])
			->getMock();
		$rabbitWrapper->expects($this->once())
			->method('publishMessage')
			->with(
				$this->equalTo(RabbitWrapper::APP_EXCHANGE),
				$this->equalTo(RabbitWrapper::EMAIL_ROUTE_KEY),
				$this->equalTo([
					'to' => $to,
					'email_type' => $emailType
				])
			);

        $dependencies = (object) [
            'queue' => $rabbitWrapper
        ];

		$emailWrapper = $this->getMockBuilder('\App\Helpers\EmailWrapper')
            ->setConstructorArgs([$dependencies])
			->setMethods(['dummy'])
			->getMock();

		// run
		$emailWrapper->queue($to, $emailType);
	}
	/**
	 * test: queue without ccs
	 */
	public function testQueueWithDataAndCCs()
	{
		// param
		$to = 'email@email.com';
		$emailType = 'test';
		$data = array(
			'key' => 'value'
		);
		$ccs = array(
			'another_email@email.com', 'a_third_email@email.com'
		);
		$bccs = array(
			'more_emails@email.com'
		);

		// mock
		$rabbitWrapper = $this->getMockBuilder('\App\Helpers\RabbitWrapper')
			->disableOriginalConstructor()
			->setMethods(array('publishMessage'))
			->getMock();
		$rabbitWrapper->expects($this->once())
			->method('publishMessage')
			->with(
				$this->equalTo(RabbitWrapper::APP_EXCHANGE),
				$this->equalTo(RabbitWrapper::EMAIL_ROUTE_KEY),
				$this->equalTo(array(
					'to' => $to,
					'email_type' => $emailType,
					'data' => $data,
					'ccs' => $ccs,
					'bccs' => $bccs
				))
			);

        $dependencies = (object) [
            'queue' => $rabbitWrapper
        ];

		$emailWrapper = $this->getMockBuilder('\App\Helpers\EmailWrapper')
            ->setConstructorArgs([$dependencies])
			->setMethods(array('dummy'))
			->getMock();

		// run
		$emailWrapper->queue($to, $emailType, $data, $ccs, $bccs);
	}

	/**
	 * test: processMessage processes reset password email
	 */
	public function testProcessMessageProcessesResetPasswordEmail()
	{
		// param
		$to = 'email@email.com';
		$emailType = EmailWrapper::RESET_PASSWORD;
		$data = ['some_data' => 'some_values', 'invitedFirstName' => 'Bob'];
		$ccs = [];

		// mock
		$emailWrapper = $this->getMockBuilder('\App\Helpers\EmailWrapper')
			->disableOriginalConstructor()
			->setMethods(['send'])
			->getMock();
		$emailWrapper->expects($this->once())
			->method('send')
			->with(
				$this->equalTo($to),
				$this->equalTo('Forgotten Password?'),
				$this->equalTo('emails.user.forgotten-password'),
				$this->equalTo($data),
				$this->equalTo($ccs)
			);

		// run
		$emailWrapper->processMessage($to, $emailType, $data, $ccs);
	}

	/**
	 * test: processMessage throws exception for invalid email type
	 *
	 * @expectedException        \App\Exceptions\AppException
	 * @expectedExceptionMessage invalid_email_type
	 */
	public function testProcessMessageThrowsExceptionForInvalidEmailType()
	{
		// param
		$to = 'email@email.com';
		$emailType = 'invalid_email_type';
		$data = array('some_data' => 'some_values', 'alert' => 'error');
		$ccs = array();

		// mock
		$emailWrapper = $this->getMockBuilder('\App\Helpers\EmailWrapper')
			->disableOriginalConstructor()
			->setMethods(array('send'))
			->getMock();
		$emailWrapper->expects($this->never())
			->method('send');

		// run
		$emailWrapper->processMessage($to, $emailType, $data, $ccs);
	}

	/**
	 * test: getSendgrid
	 */
	public function testGetSendgrid()
	{
		// mock
		$sendgridKey = 'a_sendgrid_key';

		$emailWrapper = $this->getMockBuilder('\App\Helpers\EmailWrapper')
			->disableOriginalConstructor()
			->setMethods(['send'])
			->getMock();

		// run
		$results = $emailWrapper->getSendgrid();

		// post-run assertion
		$this->assertEquals('SendGrid', get_class($results));
	}

	/**
	 * testResetSendgrid
	 */
	public function testResetSendgrid()
	{
		// mock
		$emailWrapper = $this->getMockBuilder('\App\Helpers\EmailWrapper')
			->disableOriginalConstructor()
			->setMethods(array('send'))
			->getMock();

		// run
		$results = $emailWrapper->resetSendgrid();

		// post-run assertion
		$this->assertEquals(null, $results);  // asserts no exception thrown
	}

	/**
	 * test: send succeeds with sendGrid
	 */
	public function testSendSucceedsWithSendGrid()
	{
		// param
		$subject = 'a_subject';
		$to = 'email@email.com';
		$view = 'a_view';
		$data = ['data'];
		$ccs = ['another_email@email.com'];
		$bccs = ['a_third_email@email.com'];

		// mock
		$supportEmailAddress = 'support@videwell.com';
        $emailFromAddress = 'an+email+from@email.com';
		$message = 'an_html_message';
        $appEnvironment = 'an_app_environment';

        $toEmail = 'to_sendgrid_email_object';
        $fromEmail = 'from_sendgrid_email_object';
        $content = 'content';
        $replyToEmail = 'sendgrid_reply_to';

        $ccObj = 'cc_object';
        $bccObj = 'bcc_object';

        $personalization = $this->getMockBuilder('\stdClass')
            ->disableOriginalConstructor()
            ->setMethods(['addTo'])
            ->getMock();
        $personalization->expects($this->once())
            ->method('addTo')
            ->with($this->equalTo($ccObj));

        $personalization2 = $this->getMockBuilder('\stdClass')
            ->disableOriginalConstructor()
            ->setMethods(['addTo'])
            ->getMock();
        $personalization2->expects($this->once())
            ->method('addTo')
            ->with($this->equalTo($bccObj));

		$email = $this->getMockBuilder('\stdClass')
            ->disableOriginalConstructor()
            ->setMethods(['setReplyTo',
                'addPersonalization',
                'addCategory'])
            ->getMock();
        $email->expects($this->at(0))
            ->method('setReplyTo')
            ->with($this->equalTo($replyToEmail));
        $email->expects($this->at(1))
            ->method('addCategory')
            ->with($this->equalTo('a_view'));
        $email->expects($this->at(2))
            ->method('addCategory')
            ->with($this->equalTo($appEnvironment));
        $email->expects($this->at(3))
            ->method('addPersonalization')
            ->with($this->equalTo($personalization));
        $email->expects($this->at(4))
            ->method('addPersonalization')
            ->with($this->equalTo($personalization2));

		$post = $this->getMockBuilder('\stdClass')
			->disableOriginalConstructor()
			->setMethods(array('post'))
			->getMock();
		$post->expects($this->once())
			->method('post')
			->with($email);

		$send = $this->getMockBuilder('\stdClass')
			->disableOriginalConstructor()
			->setMethods(array('send'))
			->getMock();
		$send->expects($this->once())
			->method('send')
			->willReturn($post);

		$client = $this->getMockBuilder('\stdClass')
			->disableOriginalConstructor()
			->setMethods(array('mail'))
			->getMock();
		$client->expects($this->once())
			->method('mail')
			->willReturn($send);

		$sendgrid = new \stdClass();
		$sendgrid->client = $client;

		$logger = $this->mockLog();
		$dependencies = (object) [
		    'logger' => $logger
        ];

		$emailWrapper = $this->getMockBuilder('\App\Helpers\EmailWrapper')
			->setConstructorArgs([$dependencies])
			->setMethods([
			    'getSendGridEmail',
                'getSendGridMail',
                'getSendGridContent',
                'getSendGridReplyTo',
                'getSendGridPersonalization',
                'getEmailSystem',
                'renderView',
                'getSendgrid',
                'buildEmail',
                'getEnv',
                'isEnvDefined'])
			->getMock();
        $emailWrapper->expects($this->at(0))
            ->method('getEmailSystem')
            ->willReturn(EmailWrapper::EMAIL_SENDGRID);
        $emailWrapper->expects($this->at(1))
            ->method('getEnv')
            ->with($this->equalTo('SUPPORT_EMAIL_ADDRESS'))
            ->willReturn($supportEmailAddress);
        $emailWrapper->expects($this->at(2))
            ->method('getEnv')
            ->with($this->equalTo('EMAIL_FROM_ADDRESS'))
            ->willReturn($emailFromAddress);
        $emailWrapper->expects($this->at(3))
            ->method('renderView')
            ->with(
                $this->equalTo($view),
                $this->equalTo($data)
            )
            ->willReturn($message);
        $emailWrapper->expects($this->at(4))
            ->method('isEnvDefined')
            ->with($this->equalTo('APP_ENVIRONMENT'))
            ->willReturn(true);
        $emailWrapper->expects($this->at(5))
            ->method('getEnv')
            ->with($this->equalTo('APP_ENVIRONMENT'))
            ->willReturn($appEnvironment);
        // in sendgrid trait
        $emailWrapper->expects($this->at(6))
            ->method('getSendGridEmail')
            ->with($this->equalTo(null),
                $this->equalTo($to))
            ->willReturn($toEmail);
        $emailWrapper->expects($this->at(7))
            ->method('getSendGridEmail')
            ->with(
                $this->equalTo(EmailWrapper::FROM_NAME),
                $this->equalTo($emailFromAddress))
            ->willReturn($fromEmail);
        $emailWrapper->expects($this->at(8))
            ->method('getSendGridContent')
            ->with($this->equalTo('text/html'))
            ->willReturn($content);
        $emailWrapper->expects($this->at(9))
            ->method('getSendGridMail')
            ->with($this->equalTo($fromEmail),
                $this->equalTo($subject),
                $this->equalTo($toEmail),
                $this->equalTo($content))
            ->willReturn($email);
        $emailWrapper->expects($this->at(10))
            ->method('getSendGridReplyTo')
            ->with($this->equalTo($supportEmailAddress))
            ->willReturn($replyToEmail);
        $emailWrapper->expects($this->at(11))
            ->method('getSendgridPersonalization')
            ->willReturn($personalization);
        $emailWrapper->expects($this->at(12))
            ->method('getSendgridEmail')
            ->with($this->equalTo(null), $this->equalTo('another_email@email.com'))
            ->willReturn($ccObj);
        $emailWrapper->expects($this->at(13))
            ->method('getSendgridPersonalization')
            ->willReturn($personalization2);
        $emailWrapper->expects($this->at(14))
            ->method('getSendgridEmail')
            ->with($this->equalTo(null), $this->equalTo('a_third_email@email.com'))
            ->willReturn($bccObj);

		$emailWrapper->expects($this->at(15))
			->method('getSendgrid')
			->willReturn($sendgrid);

		// run
		$emailWrapper->send($to, $subject, $view, $data, $ccs, $bccs);
	}

    /**
     * test: send succeeds with ses
     */
    public function testSendSucceedsWithSes()
    {
        // param
        $subject = 'a_subject';
        $to = 'email@email.com';
        $view = 'a_view';
        $data = ['data'];
        $ccs = ['another_email@email.com'];
        $bccs = ['a_third_email@email.com'];

        // mock
        $supportEmailAddress = 'support@videwell.com';
        $emailFromAddress = 'an+email+from@email.com';
        $message = 'an_html_message';

        $awsAccessKey = 'aws_access_key';
        $awsSecretKey = 'aws_secret_key';

        $client = $this->getMockBuilder('\stdClass')
            ->disableOriginalConstructor()
            ->setMethods(['sendEmail'])
            ->getMock();
        $client->expects($this->once())
            ->method('sendEmail')
            ->with($this->equalTo([
                'Source' => $emailFromAddress,
                'Destination' => [
                    'ToAddresses' => [$to],
                    'CcAddresses' => $ccs,
                    'BccAddresses' => $bccs
                ],
                'Message' => [
                    'Subject' => [
                        'Data' => $subject
                    ],
                    'Body' => [
                        'Html' => [
                            'Data' => $message,
                            'Charset' => 'UTF-8'
                        ]
                    ]
                ]
            ]));

        $sendgrid = new \stdClass();
        $sendgrid->client = $client;

        $logger = $this->mockLog();
        $dependencies = (object) [
            'logger' => $logger
        ];

        $emailWrapper = $this->getMockBuilder('\App\Helpers\EmailWrapper')
            ->setConstructorArgs([$dependencies])
            ->setMethods([
                'getSesClient',
                'getEmailSystem',
                'renderView',
                'buildEmail',
                'getEnv',
                'isEnvDefined'])
            ->getMock();
        $emailWrapper->expects($this->at(0))
            ->method('getEmailSystem')
            ->willReturn(EmailWrapper::EMAIL_SES);
        $emailWrapper->expects($this->at(1))
            ->method('getEnv')
            ->with($this->equalTo('SUPPORT_EMAIL_ADDRESS'))
            ->willReturn($supportEmailAddress);
        $emailWrapper->expects($this->at(2))
            ->method('getEnv')
            ->with($this->equalTo('EMAIL_FROM_ADDRESS'))
            ->willReturn($emailFromAddress);
        $emailWrapper->expects($this->at(3))
            ->method('renderView')
            ->with(
                $this->equalTo($view),
                $this->equalTo($data)
            )
            ->willReturn($message);
        $emailWrapper->expects($this->at(4))
            ->method('getEnv')
            ->with($this->equalTo('AWS_ACCESS_KEY'))
            ->willReturn($awsAccessKey);
        $emailWrapper->expects($this->at(5))
            ->method('getEnv')
            ->with($this->equalTo('AWS_SECRET_KEY'))
            ->willReturn($awsSecretKey);
        $emailWrapper->expects($this->at(6))
            ->method('getSesClient')
            ->with($this->equalTo([
                'credentials' => [
                    'key' => $awsAccessKey,
                    'secret' => $awsSecretKey
                ],
                'region' => 'us-west-2',
                'version' => 'latest'
            ]))
            ->willReturn($client);

        // run
        $emailWrapper->send($to, $subject, $view, $data, $ccs, $bccs);
    }

    /**
     * test: send succeeds with mail
     */
    public function testSendSucceedsWithMail()
    {
        // param
        $subject = 'a_subject';
        $to = 'email@email.com';
        $view = 'a_view';
        $data = ['data'];
        $ccs = ['another_email@email.com'];
        $bccs = ['a_third_email@email.com'];

        // mock
        $supportEmailAddress = 'support@videwell.com';
        $emailFromAddress = 'an+email+from@email.com';
        $message = 'an_html_message';

        $logger = $this->mockLog();
        $dependencies = (object) [
            'logger' => $logger
        ];

        $headers = [
            'MIME-Version: 1.0',
            'Content-type: text/html; charset=iso-8859-1',
            'From: ' . $emailFromAddress,
            'X-Mailer: PHP/' . phpversion(),
            'Cc: another_email@email.com',
            'Bcc: a_third_email@email.com'
        ];

        $emailWrapper = $this->getMockBuilder('\App\Helpers\EmailWrapper')
            ->setConstructorArgs([$dependencies])
            ->setMethods([
                'sendEmail',
                'getEmailSystem',
                'renderView',
                'buildEmail',
                'getEnv',
                'isEnvDefined'])
            ->getMock();
        $emailWrapper->expects($this->at(0))
            ->method('getEmailSystem')
            ->willReturn(EmailWrapper::EMAIL_MAIL);
        $emailWrapper->expects($this->at(1))
            ->method('getEnv')
            ->with($this->equalTo('SUPPORT_EMAIL_ADDRESS'))
            ->willReturn($supportEmailAddress);
        $emailWrapper->expects($this->at(2))
            ->method('getEnv')
            ->with($this->equalTo('EMAIL_FROM_ADDRESS'))
            ->willReturn($emailFromAddress);
        $emailWrapper->expects($this->at(3))
            ->method('renderView')
            ->with(
                $this->equalTo($view),
                $this->equalTo($data)
            )
            ->willReturn($message);
        $emailWrapper->expects($this->at(4))
            ->method('sendEmail')
            ->with($this->equalTo($to),
                $this->equalTo($subject),
                $this->equalTo($message),
                $this->equalTo($headers));

        // run
        $emailWrapper->send($to, $subject, $view, $data, $ccs, $bccs);
    }

	/**
	 * test: send resets and throws exception upon failure
	 *
	 * @expectedException Exception
	 * @expectedExceptionMessage some_exception
	 */
	public function testSendResetsAndThrowsExceptionUponFailure()
	{
        // param
        $subject = 'a_subject';
        $to = 'email@email.com';
        $view = 'a_view';
        $data = ['data'];
        $ccs = ['another_email@email.com'];
        $bccs = ['a_third_email@email.com'];

        // mock
        $supportEmailAddress = 'support@videwell.com';
        $emailFromAddress = 'an+email+from@email.com';
        $message = 'an_html_message';
        $appEnvironment = 'an_app_environment';

        $logger = $this->mockLog();
        $dependencies = (object) [
            'logger' => $logger
        ];

        $exception = new \Exception('some_exception');

        $emailWrapper = $this->getMockBuilder('\App\Helpers\EmailWrapper')
            ->setConstructorArgs([$dependencies])
            ->setMethods([
                'getSendgridEmail',
                'getEmailSystem',
                'renderView',
                'resetSendgrid',
                'buildEmail',
                'getEnv',
                'isEnvDefined'])
            ->getMock();
        $emailWrapper->expects($this->at(0))
            ->method('getEmailSystem')
            ->willReturn(EmailWrapper::EMAIL_SENDGRID);
        $emailWrapper->expects($this->at(1))
            ->method('getEnv')
            ->with($this->equalTo('SUPPORT_EMAIL_ADDRESS'))
            ->willReturn($supportEmailAddress);
        $emailWrapper->expects($this->at(2))
            ->method('getEnv')
            ->with($this->equalTo('EMAIL_FROM_ADDRESS'))
            ->willReturn($emailFromAddress);
        $emailWrapper->expects($this->at(3))
            ->method('renderView')
            ->with(
                $this->equalTo($view),
                $this->equalTo($data)
            )
            ->willReturn($message);
        $emailWrapper->expects($this->at(4))
            ->method('isEnvDefined')
            ->with($this->equalTo('APP_ENVIRONMENT'))
            ->willReturn(true);
        $emailWrapper->expects($this->at(5))
            ->method('getEnv')
            ->with($this->equalTo('APP_ENVIRONMENT'))
            ->willReturn($appEnvironment);
        $emailWrapper->expects($this->at(6))
            ->method('getSendGridEmail')
            ->willThrowException($exception);
        $emailWrapper->expects($this->at(7))
            ->method('resetSendgrid');

        // run
        $emailWrapper->send($to, $subject, $view, $data, $ccs, $bccs);
	}

}