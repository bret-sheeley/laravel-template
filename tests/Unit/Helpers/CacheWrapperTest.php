<?php
namespace Tests\Unit\Helpers;

use Tests\TestCase;
use App\Helpers\CacheWrapper;

class CacheWrapperTest extends TestCase
{
    // =====================================
    //
    // get

    /**
     * test: get with a tag and no value
     */
    public function testGetWithATagAndNoValue()
    {
        // param
        $key = 'a_key';
        $tag = 'a_tag';

        // mock
        $cacheWrapper = new CacheWrapper();

        // run
        $results = $cacheWrapper->get('a_key', 'a_tag');

        // post-run assertions
        $this->assertNull($results);
    }

    /**
     * test: get with a key
     */
    public function testGetWithAKey()
    {
        // param
        $key = 'a_key';

        // mock
        $cacheWrapper = new CacheWrapper();
        $cacheWrapper->clear($key);

        // init
        $cacheWrapper->set($key, 'a_value', 120);

        // run
        $results = $cacheWrapper->get($key);

        // post-run assertions
        $this->assertEquals('a_value', $results);
    }
}