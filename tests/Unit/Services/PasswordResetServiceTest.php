<?php
namespace Tests\Unit\Services;

use Tests\TestCase;
use App\Helpers\{
    DependencyContainer,
    EmailWrapper
};
use App\Services\AuthenticationService;
use App\Models\Domains\{
    Token,
    User
};

class PasswordResetServiceTest extends TestCase
{
    // ==================================
    //
    // Set
    //

    /**
     * test: set
     */
    public function testSet()
    {
        // params
        $token = 'a_token';
        $password = 'a_password';

        // mock
        $email = 'bob@email.com';
        $userId = 3;

        $user = $this->getMockBuilder('App\Models\Domains\User')
            ->disableOriginalConstructor()
            ->setMethods(['getId'])
            ->getMock();
        $user->expects($this->once())
            ->method('getId')
            ->willReturn($userId);

        $passwordResetMapper = $this->getMockBuilder('App\Models\Mappers\PasswordResetMapper')
            ->disableOriginalConstructor()
            ->setMethods(['findEmailByToken', 'removeByEmail'])
            ->getMock();
        $passwordResetMapper->expects($this->once())
            ->method('findEmailByToken')
            ->with($this->equalTo($token))
            ->willReturn($email);
        $passwordResetMapper->expects($this->once())
            ->method('removeByEmail')
            ->with($this->equalTo($email));

        $userMapper = $this->getMockBuilder('App\Models\Mappers\UserMapper')
            ->disableOriginalConstructor()
            ->setMethods(['findByEmail', 'changePassword'])
            ->getMock();
        $userMapper->expects($this->once())
            ->method('findByEmail')
            ->with($this->equalTo($email))
            ->willReturn($user);
        $userMapper->expects($this->once())
            ->method('changePassword')
            ->with($this->equalTo($userId));

        $this->container->set('PasswordResetMapper', $passwordResetMapper);
        $this->container->set('UserMapper', $userMapper);

        $dependencies = [
            'container' => $this->container
        ];

        $passwordResetService = $this->getMockBuilder('App\Services\PasswordResetService')
            ->setConstructorArgs([$dependencies])
            ->setMethods(['dummy'])
            ->getMock();

        // run
        $passwordResetService->set($token, $password);
    }

    // ==================================
    //
    // Make Request
    //

    /**
     * test: make request
     */
    public function testMakeRequest()
    {
        // params
        $email = 'bob@email.com';

        // mock
        $firstName = 'Bob';
        $token = 'a_token';

        $user = $this->getMockBuilder('App\Models\Domains\User')
            ->disableOriginalConstructor()
            ->setMethods(['getFirstName'])
            ->getMock();
        $user->expects($this->once())
            ->method('getFirstName')
            ->willReturn($firstName);

        $passwordResetMapper = $this->getMockBuilder('App\Models\Mappers\PasswordResetMapper')
            ->disableOriginalConstructor()
            ->setMethods(['add'])
            ->getMock();
        $passwordResetMapper->expects($this->once())
            ->method('add')
            ->with($this->equalTo($email))
            ->willReturn($token);

        $userMapper = $this->getMockBuilder('App\Models\Mappers\UserMapper')
            ->disableOriginalConstructor()
            ->setMethods(['findByEmail'])
            ->getMock();
        $userMapper->expects($this->once())
            ->method('findByEmail')
            ->with($this->equalTo($email))
            ->willReturn($user);

        $emailWrapper = $this->getMockBuilder('App\Helpers\EmailWrapper')
            ->disableOriginalConstructor()
            ->setMethods(['queue'])
            ->getMock();
        $emailWrapper->expects($this->once())
            ->method('queue')
            ->with($this->equalTo($email),
                $this->equalTo(EmailWrapper::RESET_PASSWORD),
                $this->equalTo([
                    'email' => $email,
                    'firstName' => $firstName,
                    'token' => $token
                ]));

        $this->container->set('PasswordResetMapper', $passwordResetMapper);
        $this->container->set('UserMapper', $userMapper);
        $this->container->set('EmailWrapper', $emailWrapper);

        $dependencies = [
            'container' => $this->container
        ];

        $passwordResetService = $this->getMockBuilder('App\Services\PasswordResetService')
            ->setConstructorArgs([$dependencies])
            ->setMethods(['dummy'])
            ->getMock();

        // run
        $passwordResetService->makeRequest($email);
    }
}