<?php
namespace Tests\Unit\Service;

use App\Exceptions\AppException;
use Tests\TestCase;
use App\Helpers\DependencyContainer;
use App\Services\AuthenticationService;
use App\Models\Domains\{
    Token,
    User
};

class UserServiceTest extends TestCase
{
    // ============================================
    //
    // create
    //

    /**
     * test: create returns login information
     */
    public function testCreateReturnsLoginInformation()
    {
        // params
        $email = 'bob@email.com';
        $firstName = 'Bob';
        $lastName = 'Notbob';
        $password = 'a_password';

        // mocks
        $userId = 'abcdef123456';
        $token = 'a_token';

        $user = $this->getMockBuilder('App\Models\Domains\User')
            ->disableOriginalConstructor()
            ->setMethods(['getId'])
            ->getMock();
        $user->expects($this->any())
            ->method('getId')
            ->willReturn($userId);

        $tokenMapper = $this->getMockBuilder('App\Models\Mappers\TokenMapper')
            ->disableOriginalConstructor()
            ->setMethods(['insert'])
            ->getMock();
        $tokenMapper->expects($this->once())
            ->method('insert')
            ->with($this->equalTo($user))
            ->willReturn($token);

        $userMapper = $this->getMockBuilder('App\Models\Mappers\UserMapper')
            ->disableOriginalConstructor()
            ->setMethods(['isEmailAvailable', 'create'])
            ->getMock();
        $userMapper->expects($this->once())
            ->method('isEmailAvailable')
            ->with($this->equalTo($email))
            ->willReturn(true);
        $userMapper->expects($this->once())
            ->method('create')
            ->with($this->equalTo($email),
                $this->equalTo($firstName),
                $this->equalTo($lastName),
                $this->equalTo($password))
            ->willReturn($user);

        $this->container->set('TokenMapper', $tokenMapper);
        $this->container->set('UserMapper', $userMapper);

        $dependencies = [
            'container' => $this->container
        ];

        $userService = $this->getMockBuilder('App\Services\UserService')
            ->setConstructorArgs([$dependencies])
            ->setMethods(['dummy'])
            ->getMock();

        // run
        $results = $userService->create($email, $firstName, $lastName, $password);

        // post-run assertions
        $expectedResults = $token;
        $this->assertEquals($expectedResults, $results);
    }

    /**
     * test: create throws exception if email is not available
     *
     * @expectedException App\Exceptions\AppException
     * @expectedExceptionMessage email_unavailable
     */
    public function testCreateThrowsExceptionIfEmailIsNotAvailable()
    {
        // params
        $email = 'bob@email.com';
        $firstName = 'Bob';
        $lastName = 'Notbob';
        $password = 'a_password';

        // mocks
        $userId = 'abcdef123456';
        $token = 'a_token';

        $tokenMapper = $this->getMockBuilder('App\Models\Mappers\TokenMapper')
            ->disableOriginalConstructor()
            ->setMethods(['insert'])
            ->getMock();
        $tokenMapper->expects($this->never())
            ->method('insert');

        $userMapper = $this->getMockBuilder('App\Models\Mappers\UserMapper')
            ->disableOriginalConstructor()
            ->setMethods(['isEmailAvailable', 'create'])
            ->getMock();
        $userMapper->expects($this->once())
            ->method('isEmailAvailable')
            ->with($this->equalTo($email))
            ->willReturn(false);
        $userMapper->expects($this->never())
            ->method('create');

        $this->container->set('TokenMapper', $tokenMapper);
        $this->container->set('UserMapper', $userMapper);

        $dependencies = [
            'container' => $this->container
        ];

        $userService = $this->getMockBuilder('App\Services\UserService')
            ->setConstructorArgs([$dependencies])
            ->setMethods(['dummy'])
            ->getMock();

        // run
        $userService->create($email, $firstName, $lastName, $password);
    }
}