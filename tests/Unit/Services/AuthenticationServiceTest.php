<?php
namespace Tests\Unit\Service;

use Tests\TestCase;
use App\Helpers\DependencyContainer;
use App\Services\AuthenticationService;
use App\Models\Domains\{
    Token,
    User
};

class AuthenticationServiceTest extends TestCase
{
    /**
     * test: clear old tokens
     */
    public function testClearOldTokens()
    {
        // mock
        $threshold = '2018-01-01 00:00:00';

        $time = $this->getMockBuilder('App\Helpers\Time')
            ->disableOriginalConstructor()
            ->setMethods(['ago'])
            ->getMock();
        $time->expects($this->once())
            ->method('ago')
            ->willReturn($threshold);

        $tokenMapper = $this->getMockBuilder('App\Models\Mappers\TokenMapper')
            ->disableOriginalConstructor()
            ->setMethods(['deleteOldTokens'])
            ->getMock();
        $tokenMapper->expects($this->once())
            ->method('deleteOldTokens')
            ->with($this->equalTo($threshold));

        $this->container->set('Time', $time);
        $this->container->set('TokenMapper', $tokenMapper);

        $authenticationService = new AuthenticationService($this->container);

        // run
        $authenticationService->clearOldTokens();
    }

    /**
     * test: findAuthenticatedUser
     */
    public function testFindAuthenticatedUser()
    {
        // param
        $tokenString = '123abc';
        
        // mock
        $userId = 3;
        $dataset = new \stdClass();
        $dataset->userId = $userId;
        $user = new User($dataset);
        
        $dataset = new \stdClass();
        $dataset->userId = $userId;
        $token = new Token($dataset);
        
        $tokenMapper = $this->getMockBuilder('\App\Models\Mappers\TokenMapper')
            ->disableOriginalConstructor()
            ->setMethods(['findByToken', 'updateLastAccessed'])
            ->getMock();
        $tokenMapper->expects($this->once())
            ->method('findByToken')
            ->with($this->equalTo($tokenString))
            ->willReturn($token);
        $tokenMapper->expects($this->once())
            ->method('updateLastAccessed')
            ->with($this->equalTo($tokenString));
            
        $userMapper = $this->getMockBuilder('\App\Models\Mappers\UserMapper')
            ->disableOriginalConstructor()
            ->setMethods(['findById'])
            ->getMock();
        $userMapper->expects($this->once())
            ->method('findById')
            ->with($this->equalTo($userId))
            ->willReturn($user);
        
        $container = new DependencyContainer();
        $container->set('TokenMapper', $tokenMapper);
        $container->set('UserMapper', $userMapper);
        $dependencies = (object) [
            'container' => $container
        ];
        
        // run
        $authenticationService = new AuthenticationService($dependencies);
        $authenticationService->findAuthenticatedUser($tokenString);
    }

    // ================================
    //
    // Login
    //

    /**
     * test: login
     */
    public function testLogin()
    {
        // param
        $email = 'bob@email.com';
        $password = 'a_password';

        // mock
        $token = 'a_token';
        $user = 'a_user';

        $tokenMapper = $this->getMockBuilder('\App\Models\Mappers\TokenMapper')
            ->disableOriginalConstructor()
            ->setMethods(['insert'])
            ->getMock();
        $tokenMapper->expects($this->once())
            ->method('insert')
            ->with($this->equalTo($user))
            ->willReturn($token);

        $userMapper = $this->getMockBuilder('\App\Models\Mappers\UserMapper')
            ->disableOriginalConstructor()
            ->setMethods(['login'])
            ->getMock();
        $userMapper->expects($this->once())
            ->method('login')
            ->with($this->equalTo($email), $this->equalTo($password))
            ->willReturn($user);

        $this->container->set('TokenMapper', $tokenMapper);
        $this->container->set('UserMapper', $userMapper);

        $dependencies = [
            'container' => $this->container
        ];

        $authenticationService = $this->getMockBuilder('\App\Services\AuthenticationService')
            ->setConstructorArgs([$dependencies])
            ->setMethods(['dummy'])
            ->getMock();

        // run
        $results = $authenticationService->login($email, $password);

        // post-run assertions
        $expectedResults = [
            $token, $user
        ];
        $this->assertEquals($expectedResults, $results);
    }
}