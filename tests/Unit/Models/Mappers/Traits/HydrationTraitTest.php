<?php
namespace Tests\Unit\Models\Mappers\Traits;

use Tests\TestCase;
use App\Models\Domains\User;

class HydrationTraitTest extends TestCase
{
    /**
     * test: toObject
     */
    public function testToObject()
    {
        // param
        $className = 'User';
        $record = ['id' => 'abc'];

        // mock
        $trait = $this->getMockForTrait('App\Models\Mappers\Traits\HydrationTrait');

        // run
        $user = $trait->toObject($className, $record);

        // post-run assertions
        $this->assertEquals('App\Models\Domains\\' . $className, get_class($user));
        $this->assertEquals('abc', $user->getId());
    }

    /**
     * test: toObjects
     */
    public function testToObjects()
    {
        // param
        $className = 'User';
        $records = [
            ['id' => 'abc'],
            ['id' => '123']
        ];

        // mock
        $trait = $this->getMockForTrait('App\Models\Mappers\Traits\HydrationTrait');

        // run
        $users = $trait->toObjects($className, $records, 'id');

        // post-run assertions
        $this->assertTrue(isset($users['123']));
        $this->assertTrue(isset($users['abc']));
        $this->assertEquals('App\Models\Domains\\' . $className, get_class($users['123']));
        $this->assertEquals('abc', $users['abc']->getId());
    }

    /**
     * test: toGroupedObjects
     */
    public function testToGroupedObjects()
    {
        // param
        $className = 'User';
        $records = [
            'a' => [
                'abc' => ['id' => 'abc', 'something' => 'a'],
                'abc123' => ['id' => 'abc123', 'something' => 'a']
            ],
            'b' => [
                '123' => ['id' => '123', 'something' => 'b']
            ]
        ];

        // mock
        $trait = $this->getMockForTrait('App\Models\Mappers\Traits\HydrationTrait');

        // run
        $users = $trait->toGroupedObjects($className, $records);

        // post-run assertions
        $expectedResults = [
            'a' => [
                'abc' => new User($records['a']['abc']),
                'abc123' => new User($records['a']['abc123'])
            ],
            'b' => [
                '123' => new User($records['b']['123'])
            ]
         ];
        $this->assertEquals('App\Models\Domains\\' . $className, get_class($users['b']['123']));
    }
}