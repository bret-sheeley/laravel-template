<?php
namespace Tests\Unit\Models\Mappers\Traits;

use App\Exceptions\AppException;
use App\Models\Mappers\Traits\DatabaseTrait;
use Tests\TestCase;

/**
 * Class DatabaseTraitClass
 * @package Tests\Unit\Models\Mappers\Trait
 *
 * test class meant to expose methods to unit testing
 */
class DatabaseTraitClass
{
    use DatabaseTrait;

    public function getQueryCall($tableName)
    {
        return $this->getQuery($tableName);
    }
}

class DatabaseTraitTest extends TestCase
{


    // =============================
    //
    // find
    //

    /**
     * test: find throws given exception if records not found
     *
     * @expectedException App\Exceptions\AppException
     * @expectedExceptionMessage random_exception
     */
    public function testFindThrowsGivenExceptionIfRecordsNotFound()
    {
        // params
        $object = $this->getMockBuilder('\stdClass')
            ->disableOriginalConstructor()
            ->setMethods(['a_method'])
            ->getMock();

        $cacheTag = 'a_cache_tag';
        $parameters = (object) [
            'cache' => (object) [
                'key' => $cacheTag,
                'ttl' => 321
            ],
            'database' => [
                $object, 'a_method'
            ]
        ];

        $object->expects($this->once())
            ->method('a_method')
            ->with($this->equalTo($parameters))
            ->willReturn(null);

        $exception = new AppException('random_exception', 'Something');

        // mock
        $cache = $this->getMockBuilder('\stdClass')
            ->disableOriginalConstructor()
            ->setMethods(['get', 'set'])
            ->getMock();
        $cache->expects($this->once())
            ->method('get')
            ->with($this->equalTo($cacheTag))
            ->willReturn(null);
        $cache->expects($this->never())
            ->method('set');

        $databaseTrait = $this->getMockForTrait('App\Models\Mappers\Traits\DatabaseTrait');
        $databaseTrait->cache = $cache;

        // run
        $databaseTrait->find($parameters, $exception);
    }

    /**
     * test: find returns null if no records are found with no exception given
     */
    public function testFindReturnsNullIfNoRecordsAreFoundWithNoExceptions()
    {
        // params
        $object = $this->getMockBuilder('\stdClass')
            ->disableOriginalConstructor()
            ->setMethods(['a_method'])
            ->getMock();

        $cacheTag = 'a_cache_tag';
        $parameters = (object) [
            'cache' => (object) [
                'key' => $cacheTag,
                'ttl' => 321
            ],
            'database' => [
                $object, 'a_method'
            ]
        ];

        $object->expects($this->once())
            ->method('a_method')
            ->with($this->equalTo($parameters))
            ->willReturn(null);

        // mock
        $cache = $this->getMockBuilder('\stdClass')
            ->disableOriginalConstructor()
            ->setMethods(['get', 'set'])
            ->getMock();
        $cache->expects($this->once())
            ->method('get')
            ->with($this->equalTo($cacheTag))
            ->willReturn(null);
        $cache->expects($this->never())
            ->method('set');

        $databaseTrait = $this->getMockForTrait('App\Models\Mappers\Traits\DatabaseTrait');
        $databaseTrait->cache = $cache;

        // run
        $results = $databaseTrait->find($parameters);

        // post-run assertions
        $this->assertNull($results);
    }

    /**
     * test: find returns records from array definition if found
     */
    public function testFindReturnsRecordsFromArrayDefinitionIfFound()
    {
        // params
        $object = $this->getMockBuilder('\stdClass')
            ->disableOriginalConstructor()
            ->setMethods(['a_method'])
            ->getMock();

        $cacheKey = 'a_cache_key';
        $parameters = (object) [
            'cache' => (object) [
                'key' => $cacheKey,
                'ttl' => 321
            ],
            'database' => [
                $object, 'a_method'
            ]
        ];

        $object->expects($this->once())
            ->method('a_method')
            ->with($this->equalTo($parameters))
            ->willReturn('results');

        // mock
        $cache = $this->getMockBuilder('\stdClass')
            ->disableOriginalConstructor()
            ->setMethods(['get', 'set'])
            ->getMock();
        $cache->expects($this->once())
            ->method('get')
            ->with($this->equalTo($cacheKey))
            ->willReturn(null);
        $cache->expects($this->once())
            ->method('set')
            ->with($this->equalTo($cacheKey),
                $this->equalTo('results'),
                $this->equalTo(321));

        $databaseTrait = $this->getMockForTrait('App\Models\Mappers\Traits\DatabaseTrait');
        $databaseTrait->cache = $cache;

        // run
        $results = $databaseTrait->find($parameters);

        // post-run assertions
        $this->assertEquals('results', $results);
    }

    /**
     * test: find throws exception if invalid database definition is defined
     *
     * @expectedException \Exception
     * @expectedExceptionMessage invalid_database_closure_definition
     */
    public function testFindReturnsThrowsExceptionIfInvalidDatabaseDefinitionIsDefined()
    {
        // params
        $cacheTag = 'a_cache_tag';
        $parameters = (object) [
            'cache' => (object) [
                'key' => $cacheTag,
                'ttl' => 321
            ]
        ];

        // mock
        $cache = $this->getMockBuilder('\stdClass')
            ->disableOriginalConstructor()
            ->setMethods(['get', 'set'])
            ->getMock();
        $cache->expects($this->never())
            ->method('get');
        $cache->expects($this->never())
            ->method('set');

        $databaseTrait = $this->getMockForTrait('App\Models\Mappers\Traits\DatabaseTrait');
        $databaseTrait->cache = $cache;

        // run
        $results = $databaseTrait->find($parameters);

        // post-run assertions
        $this->assertEquals('results', $results);
    }

    // ================================
    //
    // get query

    /**
     * test: getQuery
     */
    public function testGetQuery()
    {
        // param
        $tableName = 'a_table';

        // mock
        $db = $this->getMockBuilder('\stdClass')
            ->disableOriginalConstructor()
            ->setMethods(['table'])
            ->getMock();
        $db->expects($this->once())
            ->method('table')
            ->with($this->equalTo($tableName))
            ->willReturn('table_query');

        $databaseTrait = $this->getMockBuilder('Tests\Unit\Models\Mappers\Traits\DatabaseTraitClass')
            ->disableOriginalConstructor()
            ->setMethods(['getDb'])
            ->getMock();
        $databaseTrait->expects($this->once())
            ->method('getDb')
            ->willReturn($db);

        // run
        $results = $databaseTrait->getQueryCall($tableName);

        // post-run assertions
        $this->assertEquals('table_query', $results);
    }
}
