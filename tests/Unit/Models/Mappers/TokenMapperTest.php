<?php
namespace Tests\Unit\Models\Mappers;

use Tests\TestCase;
use App\Exceptions\AppException;
use App\Helpers\{
    DependencyContainer,
    Time
};
use App\Models\Domains\User;

class TokenMapperTest extends TestCase
{
    private $tableName = 'tokens';
    private $domainName = 'Token';

    // =================================
    //
    // Find By Token
    //

    /**
     * test: find by token returns results
     */
    public function testFindByTokenReturnsResults()
    {
        // param
        $token = '123abc';
        
        //
        // mock
        //
        $exception = new AppException('invalid_token', 401,
            'Invalid token ' . $token);
        $record = new \stdClass;
        $record->token = $token;
        $results = 'an_object';
        
        // mock user mapper
        $tokenMapper = $this->getMockBuilder('\App\Models\Mappers\TokenMapper')
            ->disableOriginalConstructor()
            ->setMethods(['find', 'toObject'])
            ->getMock();
            
        $parameters = (object) [
            'data' => (object) [
                'token' => $token
            ],
            'database' => [ $tokenMapper, 'findByTokenViaDatabase'],
            'cache' => (object) [
                'key' => 'token_' . $token,
                'ttl' => Time::ONE_DAY
            ]
        ];
        
        $tokenMapper->expects($this->once())
            ->method('find')
            ->with($this->equalTo($parameters), $this->equalTo($exception))
            ->willReturn($record);
        $tokenMapper->expects($this->once())
            ->method('toObject')
            ->with($this->equalTo('Token'), $this->equalTo($record))
            ->willReturn($results);
        
        //
        // run
        //
        $results = $tokenMapper->findByToken($token);
        
        //
        // post-run assertions
        //
        $expectedResults = 'an_object';
        $this->assertEquals($expectedResults, $results);
    }

    /**
     * test: findByTokenViaDatabase
     */
    public function testFindByTokenViaDatabase()
    {
        // param
        $token = 123;
        $parameters = (object) [
            'data' => (object) [
                'token' => $token
            ]
        ];

        // mock
        $query = $this->getMockBuilder('\stdClass')
            ->disableOriginalConstructor()
            ->setMethods(['where', 'first'])
            ->getMock();
        $query->expects($this->once())
            ->method('where')
            ->with($this->equalTo('token'),
                $this->equalTo($token))
            ->willReturn($query);
        $query->expects($this->once())
            ->method('first')
            ->willReturn('return record');

        $tokenMapper = $this->getMockBuilder('App\Models\Mappers\TokenMapper')
            ->disableOriginalConstructor()
            ->setMethods(['getQuery'])
            ->getMock();
        $tokenMapper->expects($this->once())
            ->method('getQuery')
            ->with($this->equalTo('tokens'))
            ->willReturn($query);

        // run
        $results = $tokenMapper->findByTokenViaDatabase($parameters);

        // post-run assertions
        $this->assertEquals('return record', $results);
    }

    // ================================
    //
    // Insert
    
    /**
     * test: insert
     */
    public function testInsert()
    {
        // param
        $userId = 23;
        $dataset = (object) [
            'id' => $userId
        ];
        $user = new User($dataset);
        
        // mock
        $token = 'abcdef';
        
        $random = $this->getMockBuilder('\App\Helpers\Random')
            ->disableOriginalConstructor()
            ->setMethods(['generateUniqueToken'])
            ->getMock();
        $random->expects($this->once())
            ->method('generateUniqueToken')
            ->with($this->equalTo(160))
            ->willReturn($token);
        
        $query = $this->getMockBuilder('\stdClass')
            ->disableOriginalConstructor()
            ->setMethods(['insert', 'delete', 'where'])
            ->getMock();
        $query->expects($this->at(0))
            ->method('where')
            ->with(
                $this->equalTo('user_id'),
                $this->equalTo($userId)
            )
            ->willReturn($query);
        $query->expects($this->at(1))
            ->method('delete')
            ->willReturn($query);
        $query->expects($this->at(2))
            ->method('insert')
            ->with($this->equalTo([
                'token' => $token,
                'user_id' => $user->getId()
            ]));

        $container = new DependencyContainer();
        $container->set('Random', $random);
        $dependencies = (object) [
            'container' => $container
        ];
        
        $tokenMapper = $this->getMockBuilder('\App\Models\Mappers\TokenMapper')
            ->setConstructorArgs([$dependencies])
            ->setMethods(['getQuery', 'hash'])
            ->getMock();
        $tokenMapper->expects($this->any())
            ->method('getQuery')
            ->with($this->equalTo('tokens'))
            ->willReturn($query);
        
        // run
        $tokenMapper->insert($user);
    }
    
    /**
     * test: updateLastAccessed
     */
    public function testUpdateLastAccessed()
    {
        // param
        $token = 'abcdef';
        
        // mock
        $now = '2018-01-01 12:00:00';
        
        $time = $this->getMockBuilder('\App\Helpers\Time')
            ->disableOriginalConstructor()
            ->setMethods(['now'])
            ->getMock();
        $time->expects($this->once())
            ->method('now')
            ->willReturn($now);
        
        $query = $this->getMockBuilder('\stdClass')
            ->disableOriginalConstructor()
            ->setMethods(['where', 'update'])
            ->getMock();
        $query->expects($this->at(0))
            ->method('where')
            ->with(
                $this->equalTo('token'),
                $this->equalTo($token)
            )
            ->willReturn($query);
        $query->expects($this->at(1))
            ->method('update')
            ->with(
                $this->equalTo([
                    'last_accessed' => $now
                ])
            );
            
        
        $container = new DependencyContainer();
        $container->set('Time', $time);
        $dependencies = (object) [
            'container' => $container
        ];
        
        $tokenMapper = $this->getMockBuilder('\App\Models\Mappers\TokenMapper')
            ->setConstructorArgs([$container])
            ->setMethods(['getQuery'])
            ->getMock();
        $tokenMapper->expects($this->once())
            ->method('getQuery')
            ->with($this->equalTo('tokens'))
            ->willReturn($query);
        
        // run
        $tokenMapper->updateLastAccessed($token);
    }

    /**
     * test: delete old tokens
     */
    public function testDeleteOldTokens()
    {
        // params
        $thresholdTime = '2018-01-01 00:00:00';

        // mocks
        $query = $this->getMockBuilder('\stdClass')
            ->disableOriginalConstructor()
            ->setMethods(['where', 'delete'])
            ->getMock();
        $query->expects($this->once())
            ->method('where')
            ->with($this->equalTo('last_accessed'),
                $this->equalTo('<'),
                $this->equalTo($thresholdTime))
            ->willReturn($query);
        $query->expects($this->once())
            ->method('delete');

        $tokenMapper = $this->getMockBuilder('\App\Models\Mappers\TokenMapper')
            ->disableOriginalConstructor()
            ->setMethods(['getQuery'])
            ->getMock();
        $tokenMapper->expects($this->once())
            ->method('getQuery')
            ->with($this->equalTo('tokens'))
            ->willReturn($query);

        // run
        $tokenMapper->deleteOldTokens($thresholdTime);
    }

    // ================================
    //
    // deleteByUserId

    /**
     * test: deleteByUserId
     */
    public function testDeleteByUserId()
    {
        // param
        $userId = 321;

        // mock
        $query = $this->getMockBuilder('\stdClass')
            ->disableOriginalConstructor()
            ->setMethods(['where', 'delete'])
            ->getMock();
        $query->expects($this->once())
            ->method('where')
            ->with($this->equalTo('user_id'),
                $this->equalTo($userId))
            ->willReturn($query);
        $query->expects($this->once())
            ->method('delete');

        $tokenMapper = $this->getMockBuilder('\App\Models\Mappers\TokenMapper')
            ->disableOriginalConstructor()
            ->setMethods(['getQuery'])
            ->getMock();
        $tokenMapper->expects($this->once())
            ->method('getQuery')
            ->with($this->equalTo('tokens'))
            ->willReturn($query);

        // run
        $tokenMapper->deleteByUserId($userId);
    }
}