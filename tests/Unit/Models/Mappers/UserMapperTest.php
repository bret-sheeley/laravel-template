<?php
namespace Tests\Unit\Models\Mappers;

use Tests\TestCase;
use App\Exceptions\AppException;
use App\Helpers\Time;

class UserMapperTest extends TestCase
{
    // ==============================
    //
    // change password

    /**
     * test: changePassword
     */
    public function testChangePassword()
    {
        // param
        $userId = 4;
        $newPassword = 'a secret password';

        // mock
        $hashedPassword = 'encrypted unreadable string';

        $query = $this->getMockBuilder('\stdClass')
            ->disableOriginalConstructor()
            ->setMethods(['where', 'update'])
            ->getMock();
        $query->expects($this->once())
            ->method('where')
            ->with($this->equalTo('id'),
                $this->equalTo($userId))
            ->willReturn($query);
        $query->expects($this->once())
            ->method('update')
            ->with($this->equalTo(['password' => $hashedPassword]))
            ->willReturn($query);

        $userMapper =  $this->getMockBuilder('\App\Models\Mappers\UserMapper')
            ->disableOriginalConstructor()
            ->setMethods(['getQuery', 'hash'])
            ->getMock();
        $userMapper->expects($this->once())
            ->method('getQuery')
            ->with($this->equalTo('users'))
            ->willReturn($query);
        $userMapper->expects($this->once())
            ->method('hash')
            ->with($this->equalTo($newPassword))
            ->willReturn($hashedPassword);

        // run
        $userMapper->changePassword($userId, $newPassword);
    }

    // ==============================
    //
    // create
    //

    /**
     * test: create
     */
    public function testCreate()
    {
        // param
        $email = 'bob@email.com';
        $firstName = 'Bob';
        $lastName = 'Notbob';
        $password = 'drowssap';

        // mock
        $userId = 4;
        $hashedPassword = 'blahblahblah';

        $query = $this->getMockBuilder('\stdClass')
            ->disableOriginalConstructor()
            ->setMethods(['insertGetId'])
            ->getMock();
        $query->expects($this->once())
            ->method('insertGetId')
            ->with($this->equalTo([
                'email' => $email,
                'first_name' => $firstName,
                'last_name' => $lastName,
                'password' => $hashedPassword
            ]))
            ->willReturn($userId);

        $dependencies = [
            'container' => $this->container
        ];

        $userMapper =  $this->getMockBuilder('\App\Models\Mappers\UserMapper')
            ->setConstructorArgs([$dependencies])
            ->setMethods(['getQuery', 'hash', 'findById'])
            ->getMock();
        $userMapper->expects($this->once())
            ->method('getQuery')
            ->with($this->equalTo('users'))
            ->willReturn($query);
        $userMapper->expects($this->once())
            ->method('hash')
            ->with($this->equalTo($password))
            ->willReturn($hashedPassword);
        $userMapper->expects($this->once())
            ->method('findById')
            ->with($this->equalTo($userId))
            ->wilLReturn('new_user_object');

        // run
        $results = $userMapper->create($email, $firstName, $lastName, $password);

        // post-run assertions
        $this->assertEquals('new_user_object', $results);
    }

    // ==============================
    //
    // find by email
    //

    /**
     * test: findByEmail
     */
    public function testFindByEmail()
    {
        // param
        $email = 'bob@email.com';

        // mock
        $userMapper =  $this->getMockBuilder('\App\Models\Mappers\UserMapper')
            ->disableOriginalConstructor()
            ->setMethods(['toObject', 'find'])
            ->getMock();

        $parameters = (object) [
            'data' => (object) [
                'email' => $email
            ],
            'database' => [ $userMapper, 'findByEmailViaDatabase' ],
            'cache' => (object) [
                'key' => 'user_email_' . $email,
                'ttl' => Time::ONE_DAY
            ]
        ];

        $exception = new AppException('email_not_found', 500,
            'Email ' . $email . ' not found.');

        $record = 'a_record';
        $object = 'an_object';

        $userMapper->expects($this->once())
            ->method('find')
            ->with($this->equalTo($parameters),
                $this->equalTo($exception))
            ->willReturn($record);
        $userMapper->expects($this->once())
            ->method('toObject')
            ->with($this->equalTo('User'),
                $this->equalTo($record))
            ->willReturn($object);

        // run
        $results = $userMapper->findByEmail($email);

        // post-run assertions
        $this->assertEquals($object, $results);
    }

    /**
     * test: findByEmailViaDatabase
     */
    public function testFindByEmailViaDatabase()
    {
        // param
        $email = 'bob@email.com';

        $parameters = (object) [
            'data' => (object) [
                'email' => $email
            ]
        ];

        // mock
        $query = $this->getMockBuilder('\stdClass')
            ->disableOriginalConstructor()
            ->setMethods(['where', 'first'])
            ->getMock();
        $query->expects($this->once())
            ->method('where')
            ->with($this->equalTo('email'),
                $this->equalTo($email))
            ->willReturn($query);
        $query->expects($this->once())
            ->method('first')
            ->willReturn('a_record');

        $userMapper = $this->getMockBuilder('\App\Models\Mappers\UserMapper')
            ->disableOriginalConstructor()
            ->setMethods(['getQuery'])
            ->getMock();
        $userMapper->expects($this->once())
            ->method('getQuery')
            ->with($this->equalTo('users'))
            ->willReturn($query);

        // run
        $results = $userMapper->findByEmailViaDatabase($parameters);

        // post-run assertions
        $this->assertEquals('a_record', $results);

    }

    // ==============================
    //
    // find by id
    //

    /**
     * test: findById
     */
    public function testFindById()
    {
        //
        // param
        //
        $userId = 1;
        
        //
        // mock
        //
        $exception = new AppException('user_not_found', 500, 'User of short id = 1 not found.');
        $record = 'a_record';
        $results = 'an_object';
        
        // mock user mapper
        $userMapper =  $this->getMockBuilder('\App\Models\Mappers\UserMapper')
            ->disableOriginalConstructor()
            ->setMethods(['find', 'toObject'])
            ->getMock();
  
        $parameters = (object) [
            'data' => (object) [
                'userId' => $userId
            ],
            'database' => [ $userMapper, 'findByIdViaDatabase' ],
            'cache' => (object) [
                'key' => 'user_id_1',
                'ttl' => Time::ONE_DAY
            ]
        ];
            
        $userMapper->expects($this->once())
            ->method('find')
            ->with($this->equalTo($parameters), $this->equalTo($exception))
            ->willReturn($record);
        $userMapper->expects($this->once())
            ->method('toObject')
            ->with($this->equalTo('User'), $this->equalTo($record))
            ->willReturn($results);
           
        //
        // run
        //
        $results = $userMapper->findById($userId);
        
        //
        // post-run assertions
        //
        $expectedResults = 'an_object';
        $this->assertEquals($expectedResults, $results);
    }

    /**
     * test: findByIdViaDatabase
     */
    public function testFindByIdViaDatabase()
    {
        // param
        $userId = 4;

        $parameters = (object) [
            'data' => (object) [
                'userId' => $userId
            ]
        ];

        // mock
        $query = $this->getMockBuilder('\stdClass')
            ->disableOriginalConstructor()
            ->setMethods(['where', 'first'])
            ->getMock();
        $query->expects($this->once())
            ->method('where')
            ->with($this->equalTo('id'),
                $this->equalTo($userId))
            ->willReturn($query);
        $query->expects($this->once())
            ->method('first');

        $mapper = $this->getMockBuilder('\App\Models\Mappers\UserMapper')
            ->disableOriginalConstructor()
            ->setMethods(['getQuery'])
            ->getMock();
        $mapper->expects($this->once())
            ->method('getQuery')
            ->with($this->equalTo('users'))
            ->willReturn($query);

        // run
        $mapper->findByIdViaDatabase($parameters);
    }

    // =====================================
    //
    // is email available
    //

    /**
     * test: isEmailAvailable returns true if not found
     */
    public function testIsEmailAvailableReturnsTrueIfNotFound()
    {
        // param
        $email = 'bob@email.com';

        // mock
        $exception = new AppException('public_message', 'Public Message');

        $mapper = $this->getMockBuilder('\App\Models\Mappers\UserMapper')
            ->disableOriginalConstructor()
            ->setMethods(['findByEmail'])
            ->getMock();
        $mapper->expects($this->once())
            ->method('findByEmail')
            ->with($this->equalTo($email))
            ->will($this->throwException($exception));

        // run
        $results = $mapper->isEmailAvailable($email);

        // post-run assertions
        $this->assertTrue($results);
    }

    /**
     * test: isEmailAvailable returns false if found
     */
    public function testIsEmailAvailableReturnsFalseIfFound()
    {
        // param
        $email = 'bob@email.com';

        // mock
        $mapper = $this->getMockBuilder('\App\Models\Mappers\UserMapper')
            ->disableOriginalConstructor()
            ->setMethods(['findByEmail'])
            ->getMock();
        $mapper->expects($this->once())
            ->method('findByEmail')
            ->with($this->equalTo($email));

        // run
        $results = $mapper->isEmailAvailable($email);

        // post-run assertions
        $this->assertFalse($results);
    }

    // ====================================
    //
    // Login
    //

    /**
     * test: login returns user is successful
     */
    public function testLoginReturnsUserIsSuccessful()
    {
        // param
        $email = 'bob@email.com';
        $password = 'a_password';

        // mock
        $storedPassword = 'a_stored_hash';

        $user = $this->getMockBuilder('\App\Models\Domains\User')
            ->disableOriginalConstructor()
            ->setMethods(['getPassword'])
            ->getMock();
        $user->expects($this->once())
            ->method('getPassword')
            ->willReturn($storedPassword);

        $mapper = $this->getMockBuilder('\App\Models\Mappers\UserMapper')
            ->disableOriginalConstructor()
            ->setMethods(['findByEmail', 'verifyHash'])
            ->getMock();
        $mapper->expects($this->once())
            ->method('findByEmail')
            ->with($this->equalTo($email))
            ->willReturn($user);
        $mapper->expects($this->once())
            ->method('verifyHash')
            ->with($this->equalTo($password),
                $this->equalTo($storedPassword))
            ->willReturn(true);

        // run
        $results = $mapper->login($email, $password);

        // post-run assertions
        $this->assertEquals($user, $results);
    }

    /**
     * test: login throws exception if password does not match
     *
     * @expectedException App\Exceptions\AppException
     * @expectedExceptionMessage invalid_login
     */
    public function testLoginThrowsExceptionIfPasswordDoesNotMatch()
    {
        // param
        $email = 'bob@email.com';
        $password = 'a_password';

        // mock
        $storedPassword = 'a_stored_hash';

        $user = $this->getMockBuilder('\App\Models\Domains\User')
            ->disableOriginalConstructor()
            ->setMethods(['getPassword'])
            ->getMock();
        $user->expects($this->once())
            ->method('getPassword')
            ->willReturn($storedPassword);

        $mapper = $this->getMockBuilder('\App\Models\Mappers\UserMapper')
            ->disableOriginalConstructor()
            ->setMethods(['findByEmail', 'verifyHash'])
            ->getMock();
        $mapper->expects($this->once())
            ->method('findByEmail')
            ->with($this->equalTo($email))
            ->willReturn($user);
        $mapper->expects($this->once())
            ->method('verifyHash')
            ->with($this->equalTo($password),
                $this->equalTo($storedPassword))
            ->willReturn(false);

        // run
        $mapper->login($email, $password);
    }

    /**
     * test: login throws exception if email does not match
     *
     * @expectedException App\Exceptions\AppException
     * @expectedExceptionMessage invalid_login
     */
    public function testLoginThrowsExceptionIfEmailDoesNotMatch()
    {
        // param
        $email = 'bob@email.com';
        $password = 'a_password';

        // mock
        $storedPassword = 'a_stored_hash';
        $exception = new AppException('invalid_login', 'Invalid Login for email ' . $email);

        $mapper = $this->getMockBuilder('\App\Models\Mappers\UserMapper')
            ->disableOriginalConstructor()
            ->setMethods(['findByEmail'])
            ->getMock();
        $mapper->expects($this->once())
            ->method('findByEmail')
            ->with($this->equalTo($email))
            ->will($this->throwException($exception));

        // run
        $mapper->login($email, $password);
    }
}
