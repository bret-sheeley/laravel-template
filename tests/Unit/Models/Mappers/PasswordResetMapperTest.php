<?php
namespace Tests\Unit\Models\Mappers;

use App\Exceptions\AppException;
use Tests\TestCase;

class PasswordResetMapperTest extends TestCase
{
    /**
     * test: add
     */
    public function testAdd()
    {
        // params
        $email = 'email@email.com';

        // mock
        $token = 'a_random_token';

        $random = $this->getMockBuilder('\App\Helpers\Random')
            ->disableOriginalConstructor()
            ->setMethods(['generateUniqueToken'])
            ->getMock();
        $random->expects($this->once())
            ->method('generateUniqueToken')
            ->with($this->equalTo(200), $this->equalTo(\App\Helpers\Random::ALPHANUMERIC))
            ->willReturn($token);

        $this->container->set('Random', $random);

        $query = $this->getMockBuilder('\stdClass')
            ->disableOriginalConstructor()
            ->setMethods(['insert'])
            ->getMock();
        $query->expects($this->once())
            ->method('insert')
            ->with($this->equalTo([
                'email' => $email,
                'token' => $token
            ]));

        $dependency = [
            'container' => $this->container
        ];

        $mapper = $this->getMockBuilder('\App\Models\Mappers\PasswordResetMapper')
            ->setConstructorArgs([$dependency])
            ->setMethods(['getQuery'])
            ->getMock();
        $mapper->expects($this->once())
            ->method('getQuery')
            ->with($this->equalTo('password_resets'))
            ->willReturn($query);

        // run
        $results = $mapper->add($email);

        // post-run assertions
        $this->assertEquals($token, $results);
    }

    // =======================================
    //
    // findByToken
    //

    /**
     * test: findByToken throws exception if not found
     *
     * @expectedException App\Exceptions\AppException
     * @expectedExceptionMessage invalid_token
     */
    public function testFindByTokenThrowsExceptionIfNotFound()
    {
        // param
        $token = 'a_token';

        // mock
        $record = null;

        $query = $this->getMockBuilder('\stdClass')
            ->disableOriginalConstructor()
            ->setMethods(['where', 'first'])
            ->getMock();
        $query->expects($this->once())
            ->method('where')
            ->with($this->equalTo('token'),
                $this->equalTo($token))
            ->willReturn($query);
        $query->expects($this->once())
            ->method('first')
            ->willReturn($record);

        $dependency = [
            'container' => $this->container
        ];

        $mapper = $this->getMockBuilder('\App\Models\Mappers\PasswordResetMapper')
            ->setConstructorArgs([$dependency])
            ->setMethods(['getQuery'])
            ->getMock();
        $mapper->expects($this->once())
            ->method('getQuery')
            ->with($this->equalTo('password_resets'))
            ->willReturn($query);

        // run
        $mapper->findByToken($token);
    }

    /**
     * test: findByToken returns email if found
     */
    public function testFindByTokenReturnsEmailIfFound()
    {
        // param
        $token = 'a_token';

        // mock
        $record = [
            'email' => 'bob@email.com',
            'token' => $token
        ];

        $query = $this->getMockBuilder('\stdClass')
            ->disableOriginalConstructor()
            ->setMethods(['where', 'first'])
            ->getMock();
        $query->expects($this->once())
            ->method('where')
            ->with($this->equalTo('token'),
                $this->equalTo($token))
            ->willReturn($query);
        $query->expects($this->once())
            ->method('first')
            ->willReturn($record);

        $mapper = $this->getMockBuilder('\App\Models\Mappers\PasswordResetMapper')
            ->disableOriginalConstructor()
            ->setMethods(['getQuery'])
            ->getMock();
        $mapper->expects($this->once())
            ->method('getQuery')
            ->with($this->equalTo('password_resets'))
            ->willReturn($query);

        // run
        $results = $mapper->findByToken($token);

        // post-run assertions
        $this->assertEquals('bob@email.com', $results);
    }

    // =====================================
    //
    // Remove By Email
    //

    /**
     * test: removeByEmail
     */
    public function testRemoveByEmail()
    {
        // param
        $email = 'bob@email.com';

        // mock
        $query = $this->getMockBuilder('\stdClass')
            ->disableOriginalConstructor()
            ->setMethods(['where', 'delete'])
            ->getMock();
        $query->expects($this->once())
            ->method('where')
            ->with($this->equalTo('email'),
                $this->equalTo($email))
            ->willReturn($query);
        $query->expects($this->once())
            ->method('delete');

        $mapper = $this->getMockBuilder('\App\Models\Mappers\PasswordResetMapper')
            ->disableOriginalConstructor()
            ->setMethods(['getQuery'])
            ->getMock();
        $mapper->expects($this->once())
            ->method('getQuery')
            ->with($this->equalTo('password_resets'))
            ->willReturn($query);

        // run
        $mapper->removeByEmail($email);
    }

}