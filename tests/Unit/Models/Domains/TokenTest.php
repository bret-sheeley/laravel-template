<?php
namespace Tests\Unit\Models\Mappers;

use Tests\TestCase;
use App\Models\Domains\Token;

class TokenTest extends TestCase
{
    /**
     * test Get Accessors
     */
    public function testGetAccessors()
    {
        // init
        $dataset = (object) [
            'user_id' => '12d3',
            'last_accessed' => '2018-01-01 00:00:00'
        ];
        $token = new Token($dataset);

        // test gets
        $this->assertEquals('12d3', $token->getUserId());
        $this->assertEquals('2018-01-01 00:00:00', $token->getLastAccessed());
    }
}