<?php
namespace Tests\Unit\Models\Mappers;

use Tests\TestCase;
use App\Models\Domains\User;

class UserTest extends TestCase
{
    /**
     * test Get Accessors
     */
    public function testGetAccessors()
    {
        // init
        $dataset = (object) [
            'id' => 2,
            'email' => 'blah@email.com',
            'password' => 'abcdef123456',
            'firstName' => 'Bob',
            'lastName' => 'NotBob',
            'banned' => 0
        ];
        $user = new User($dataset);

        // test gets
        $this->assertEquals(2, $user->getId());
        $this->assertEquals('blah@email.com', $user->getEmail());
        $this->assertEquals('abcdef123456', $user->getPassword());
        $this->assertEquals('Bob', $user->getFirstName());
        $this->assertEquals('NotBob', $user->getLastName());
        $this->assertFalse($user->isBanned());
    }

    /**
     * testGetProfile
     */
    public function testGetProfile()
    {
        // init
        $dataset = (object) [
            'id' => 2,
            'email' => 'blah@email.com',
            'password' => 'abcdef123456',
            'firstName' => 'Bob',
            'lastName' => 'NotBob',
            'banned' => false
        ];
        $user = new User($dataset);

        // test gets
        $expectedResults = [
            'email' => 'blah@email.com',
            'firstName' => 'Bob',
            'lastName' => 'NotBob'
        ];
        $this->assertEquals($expectedResults, $user->getProfile());
    }
}