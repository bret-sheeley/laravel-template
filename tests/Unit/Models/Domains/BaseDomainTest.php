<?php
namespace Tests\Unit\Models\Domains;

use Tests\TestCase;
use App\Models\Domains\BaseDomain;

class BaseDomainTest extends TestCase
{
    /**
     * test: constructor with dataset
     */
    public function testConstructorWithDataset()
    {
        // param
        $dataset = [
            'id' => 123,
            'created_at' => '2018-01-01 00:00:00',
            'updated_at' => '2018-01-02 12:00:34',
            'visible' => false
        ];

        // mock
        $baseDomain = $this->getMockBuilder('App\Models\Domains\BaseDomain')
            ->setConstructorArgs([$dataset])
            ->setMethods(['dummy'])
            ->getMock();

        // run
        $createDate = $baseDomain->getCreateDate();
        $lastModified = $baseDomain->getLastModified();
        $visible = $baseDomain->isVisible();
        $deleted = $baseDomain->isDeleted();

        // post-run assertions
        $this->assertEquals('2018-01-01 00:00:00', $createDate);
        $this->assertEquals('2018-01-02 12:00:34', $lastModified);
        $this->assertFalse($visible);
        $this->assertTrue($deleted);
    }

    /**
     * test: accessors
     */
    public function testAccessors()
    {
        // mock
        $baseDomain = $this->getMockBuilder('App\Models\Domains\BaseDomain')
            ->setMethods(['dummy'])
            ->getMock();

        $baseDomain->setLastModified('2018-01-02 12:00:34');
        $baseDomain->setVisible(false);

        // run
        $lastModified = $baseDomain->getLastModified();
        $visible = $baseDomain->isVisible();
        $deleted = $baseDomain->isDeleted();

        // post-run assertions
        $this->assertEquals('2018-01-02 12:00:34', $lastModified);
        $this->assertFalse($visible);
        $this->assertTrue($deleted);
    }

    /**
     * test: makeUrlFriendly
     */
    public function testMakeUrlFriendly()
    {
        // params
        $string = '<p>hi, all. How are You\'s & others doing?</p>';

        // mock
        $baseDomain = new BaseDomain();

        // run
        $results = $baseDomain->makeUrlFriendly($string);

        // post-run assertions
        $expectedResults = 'phi-all-how-are-yous-others-doingp';
        $this->assertEquals($expectedResults, $results);
    }

}