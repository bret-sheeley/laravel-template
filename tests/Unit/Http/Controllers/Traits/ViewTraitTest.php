<?php
namespace Tests\Unit\Http\Controllers\Traits;

use Tests\TestCase;
use App\Http\Controllers\BaseController;

class ViewTraitTest extends TestCase
{
    /**
     * test: filter view data
     */
    public function testFilterViewData()
    {
        // params
        $data = 'Hello <b>Bret</b>, going to <script>ping();</script> Barnes & Noble?';

        // run
        $baseController = new BaseController();
        $data2 = $data;
        $baseController->filterViewData($data2);

        // post-run assertion
        $expectedResults = 'Hello <b>Bret</b>, going to &lt;script&gt;ping();&lt;/script&gt; Barnes &amp; Noble?';
        $this->assertEquals($expectedResults, $data2);
    }

}