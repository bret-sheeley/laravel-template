<?php
namespace Tests\Unit\Http\Controllers\Traits;

use Tests\TestCase;
use App\Http\Controllers\BaseController;

class ResponseTraitTest extends TestCase
{
    /**
     * test: response accessors
     */
    public function testResponseAccessors()
    {
        // case #1: data available
        $baseController = new BaseController();
        $baseController->setResponseParam('hi', 'yo');
        $this->assertEquals('yo', $baseController->getResponseParam('hi'));

        // case #2: unset
        $baseController->clearOutput();
        $this->assertNull( $baseController->getResponseParam('something'));

        // case #3: get output
        $baseController->setResponseParam('hi', 'yo');
        $results = $baseController->getOutput();
        $expectedResults = ['hi' => 'yo'];
        $this->assertEquals($expectedResults, $results);
    }

    /**
     * test: response type accessors
     */
    public function testResponseTypeAccessors()
    {
        // case #1: default
        $baseController = new BaseController();
        $this->assertEquals('json', $baseController->getResponseType());

        // case #2: set
        $baseController->setResponseType('xml');
        $this->assertEquals('xml', $baseController->getResponseType());
    }

    /**
     * test: set xml response
     */
    public function testSetXmlResponse()
    {
        $baseController = new BaseController();
        $baseController->setXmlResponse('<hi>hi</hi>');
        $this->assertEquals('xml', $baseController->getResponseType());
    }

    /**
     * test: set response string
     */
    public function testSetResponseString()
    {
        $baseController = new BaseController();
        $baseController->setResponseString('hiya');
        $this->assertEquals('string', $baseController->getResponseType());
    }
}