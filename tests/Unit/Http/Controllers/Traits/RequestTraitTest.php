<?php
namespace Tests\Unit\Http\Controllers\Traits;

use Tests\TestCase;
use App\Http\Controllers\BaseController;


class RequestTraitTest extends TestCase
{
    // =================================
    //
    // get/setRequests

    /**
     * test: getRequests
     */
    public function testGetHeaders()
    {
        // init
        $baseController = new BaseController();
        $response = $baseController->getRequests();

        // assertions
        $this->assertEquals([], $response);
    }

    // =================================
    //
    // get/setRequestParams

    /**
     * test: assessors for params
     */
    public function testAssessorsForParams()
    {
        // init
        $baseController = new BaseController();
        $baseController->setRequestParam('hi', 'yo');

        // run
        $results = $baseController->getRequestParam('hi');

        // post-run assertions
        $expectedResults = 'yo';
        $this->assertEquals($expectedResults, $results);
    }

    /**
     * test: assessors for params with allow empty
     */
    public function testAssessorsForParamsWithAllowEmpty()
    {
        // init
        $baseController = new BaseController();
        $baseController->setRequestParam('hi', '');

        // run
        $results = $baseController->getRequestParam('hi', '', false);

        // post-run assertions
        $expectedResults = '';
        $this->assertEquals($expectedResults, $results);
    }

    /**
     * test: getAllHeaders
     */
    public function testGetAllHeaders()
    {
        $_SERVER['HTTP_TESTING'] = 'test';
        $_SERVER['SOMETHING'] = 'test_2';

        $baseController = new BaseController();
        $results = $baseController->getAllHeaders();

        $expectedResults = [];
        $this->assertEquals('test', $results['Testing']);
        $this->assertEquals('test_2', $results['SOMETHING']);
    }

    // ============================
    //
    // isJson

    /**
     * test: isJson
     */
    public function testIsJson()
    {
        $baseController = new BaseController();
        $results = $baseController->isJson('{ "h":"x" }');
        $this->assertTrue($results);

        $results = $baseController->isJson('{ "h"');
        $this->assertFalse($results);
    }
}