<?php
namespace Tests\Unit\Http\Controllers\Traits;

use App\Exceptions\AppException;
use Tests\TestCase;

use App\Http\Controllers\BaseController;

class AuthorizedUserTraitTest extends TestCase
{
    /**
     * test: accessors
     */
    public function testAccessors()
    {
        // param
        $user = 'a_user';

        // run
        $baseController = new BaseController();
        $baseController->setAuthorizedUser($user);
        $results = $baseController->getAuthorizedUser();

        // post-run assertion
        $this->assertEquals($user, $results);
    }

    // ======================================
    //
    // checkUserSession

    /**
     * test: checkUserSession throws exception if not active
     *
     * @expectedException App\Exceptions\AppException
     * @expectedExceptionMessage no_active_session
     */
    public function testCheckUserSessionThrowsExceptionIfNotActive()
    {
        // param
        $userId = 'abc';
        $token = '123';

        // mock
        $nowUnixtime = 1600000000;

        $time = $this->getMockBuilder('App\Helpers\Time')
            ->disableOriginalConstructor()
            ->setMethods(['now'])
            ->getMock();
        $time->expects($this->any())
            ->method('now')
            ->with($this->equalTo('U'))
            ->willReturn($nowUnixtime);
        $this->container->set('Time', $time);

        $lastUpdated = '2018-01-01 00:00:00';

        $tokenObject = new \App\Models\Domains\Token([
            'user_id' => $userId,
            'token' => $token,
            'last_accessed' => $lastUpdated
        ]);

        $tokenMapper = $this->getMockBuilder('App\Models\Mappers\TokenMapper')
            ->disableOriginalConstructor()
            ->setMethods(['findByToken'])
            ->getMock();
        $tokenMapper->expects($this->once())
            ->method('findByToken')
            ->with($this->equalTo($token))
            ->willReturn($tokenObject);

        $this->container->set('TokenMapper', $tokenMapper);

        $userMapper = $this->getMockBuilder('App\Models\Mappers\UserMapper')
            ->disableOriginalConstructor()
            ->setMethods(['findById'])
            ->getMock();
        $userMapper->expects($this->never())
            ->method('findById');

        $this->container->set('UserMapper', $userMapper);

        $baseController = new BaseController(['container' => $this->container]);

        // run
        $baseController->checkUserSession($token, $userId);
    }

    /**
     * test: checkUserSession returns user if active
     */
    public function testCheckUserSessionReturnsUserIfActive()
    {
        // param
        $userId = 3;
        $token = '123';

        // mock
        $nowUnixtime = 1500000000;

        $time = $this->getMockBuilder('App\Helpers\Time')
            ->disableOriginalConstructor()
            ->setMethods(['now'])
            ->getMock();
        $time->expects($this->any())
            ->method('now')
            ->with($this->equalTo('U'))
            ->willReturn($nowUnixtime);
        $this->container->set('Time', $time);

        $lastUpdated = '2020-01-01 00:00:00';

        $tokenObject = new \App\Models\Domains\Token([
            'user_id' => $userId,
            'token' => $token,
            'last_accessed' => $lastUpdated
        ]);

        $tokenMapper = $this->getMockBuilder('App\Models\Mappers\TokenMapper')
            ->disableOriginalConstructor()
            ->setMethods(['findByToken'])
            ->getMock();
        $tokenMapper->expects($this->once())
            ->method('findByToken')
            ->with($this->equalTo($token))
            ->willReturn($tokenObject);

        $this->container->set('TokenMapper', $tokenMapper);

        $userMapper = $this->getMockBuilder('App\Models\Mappers\UserMapper')
            ->disableOriginalConstructor()
            ->setMethods(['findById'])
            ->getMock();
        $userMapper->expects($this->once())
            ->method('findById')
            ->with($this->equalTo($userId))
            ->willReturn('a_user');

        $this->container->set('UserMapper', $userMapper);

        $baseController = new BaseController(['container' => $this->container]);

        // run
        $results = $baseController->checkUserSession($token, $userId);

        // post-run assertions
        $this->assertEquals('a_user', $results);
    }
}