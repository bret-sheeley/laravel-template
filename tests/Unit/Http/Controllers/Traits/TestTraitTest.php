<?php
namespace Tests\Unit\Http\Controllers\Traits;

use Tests\TestCase;
use App\Http\Controllers\BaseController;

class TestTraitTest extends TestCase
{
    // =============================
    //
    // isResponse
    //

    /**
     * test: isResponse matches nested arrays
     */
    public function testIsResponseMatchesNestedArrays()
    {
        // param
        $array1 = [
            'a' => [
                'ab' => 123,
                'cd' => 456,
                'ef' => [
                    'gh', 'ij'
                ]
            ],
            'b' => 321
        ];

        // mock
        $baseController = new BaseController();

        $baseController->setResponseParam('test', $array1);

        // run
        $results = $baseController->isResponse('test', $array1);

        // post-run assertions
        $this->assertTrue($results);
    }

    /**
     * test: isResponse finds mismatched array values
     */
    public function testIsResponseFindsMismatchedArrayValue()
    {
        // param
        $array1 = [
            'a' => [
                'ab' => 123,
                'cd' => 456,
                'ef' => [
                    'gh', 'ij'
                ]
            ],
            'b' => 321
        ];
        $array2 = [
            'a' => [
                'ab' => 123,
                'cd' => 456,
                'ef' => [
                    'gh', 'ik'
                ]
            ],
            'b' => 321
        ];

        // mock
        $baseController = new BaseController();

        $baseController->setResponseParam('test', $array1);

        // run
        ob_start();
        $results = $baseController->isResponse('test', $array2);
        ob_end_clean();

        // post-run assertions
        $this->assertFalse($results);
    }

    /**
     * test: isResponse finds mismatched array structure
     */
    public function testIsResponseFindsMismatchedArrayStructure()
    {
        // param
        $array1 = [
            'a' => [
                'ab' => 123,
                'cd' => 456,
                'ef' => [
                    'gh', 'ij'
                ]
            ],
            'b' => 321
        ];
        $array2 = [
            'a' => [
                'ab' => 123,
                'cd' => 456,
                'ef' => 890
            ],
            'b' => 321
        ];

        // mock
        $baseController = new BaseController();

        $baseController->setResponseParam('test', $array1);

        // run
        ob_start();
        $results = $baseController->isResponse('test', $array2);
        ob_end_clean();

        // post-run assertions
        $this->assertFalse($results);
    }

    /**
     * test: isResponse finds mismatched array structure2
     */
    public function testIsResponseFindsMismatchedArrayStructure2()
    {
        // param
        $array1 = [
            'a' => [
                'ab' => 123,
                'cd' => 456,
                'ef' => [
                    'gh', 'ij'
                ]
            ],
            'b' => 321
        ];
        $array2 = [
            'a' => [
                'ab' => 123,
                'cd' => 456,
                'ef' => 890
            ],
            'b' => 321
        ];

        // mock
        $baseController = new BaseController();

        $baseController->setResponseParam('test', $array2);

        // run
        ob_start();
        $results = $baseController->isResponse('test', $array1);
        ob_end_clean();

        // post-run assertions
        $this->assertFalse($results);
    }

    // =========================
    //
    // test Action

    /**
     * test: action
     */
    public function testAction()
    {
        // mock
        $baseController = new BaseController();

        // run
        $baseController->testAction();

        // post-run assertions
        $this->assertTrue($baseController->isResponse('hi', 'yo'));
    }
}