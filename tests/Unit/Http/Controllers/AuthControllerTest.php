<?php
namespace Tests\Http\Controllers;

use Tests\TestCase;
use App\Http\Controllers\AuthController;
use App\Exceptions\AppException;

class AuthControllerTest extends TestCase
{
    // ===========================
    //
    // Login
    //

    /**
     * test: login
     */
    public function testLoginAction()
    {
        // params
        $email = 'bob@email.com';
        $password = 'a_password';

        // mock
        $token = 'a_token';
        $profile = 'a_profile';
        $user = $this->getMockBuilder('App\Models\Domains\User')
            ->disableOriginalConstructor()
            ->setMethods(['getProfile'])
            ->getMock();
        $user->expects($this->once())
            ->method('getProfile')
            ->willReturn($profile);

        $loginList = [
            $token,
            $user
        ];

        $authenticationService = $this->getMockBuilder('AuthenticationService')
            ->disableOriginalConstructor()
            ->setMethods(['login'])
            ->getMock();
        $authenticationService->expects($this->once())
            ->method('login')
            ->with($this->equalTo($email),
                $this->equalTo($password))
            ->willReturn($loginList);

        $this->container->set('AuthenticationService', $authenticationService);

        $dependencies = [
            'container' => $this->container
        ];

        $controller = new AuthController($dependencies);

        // run
        $this->setRequestParam('email', $email);
        $this->setRequestParam('password', $password);
        $controller->loginAction();

        // post-run assertions
        $controller->isResponse('token', $token);
        $controller->isResponse('user', $profile);
    }

    // ===========================
    //
    // Logout
    //

    /**
     * test: logout
     */
    public function testLogout()
    {
        // mock
        $userId = '123';
        $token = '321';

        $user = $this->getMockBuilder('\App\Models\Domains\User')
            ->disableOriginalConstructor()
            ->setMethods(['getId'])
            ->getMock();
        $user->expects($this->any())
            ->method('getId')
            ->willReturn($userId);

        $tokenMapper = $this->getMockBuilder('\App\Models\Mappers\TokenMapper')
            ->disableOriginalConstructor()
            ->setMethods(['deleteByUserId'])
            ->getMock();
        $tokenMapper->expects($this->once())
            ->method('deleteByUserId')
            ->with($this->equalTo($userId));

        $dependencies = new \stdClass();
        $dependencies->container = new \App\Helpers\DependencyContainer();
        $dependencies->container->set('TokenMapper', $tokenMapper);

        $controller = $this->getMockBuilder('App\Http\Controllers\AuthController')
            ->setConstructorArgs([$dependencies])
            ->setMethods(['getAuthorizedUser'])
            ->getMock();
        $controller->expects($this->once())
            ->method('getAuthorizedUser')
            ->willReturn($user);

        // run
        $controller->logoutAction();

        // post-run assertions
        $controller->isResponse('status', 1);
    }

    // ====================================
    //
    // Request Password Reset
    //

    /**
     * test: requestPasswordReset successful
     */
    public function testRequestPasswordResetSuccessful()
    {
        // param
        $email = 'bob@email.com';

        // mocks
        $validator = $this->getMockBuilder('App\Helpers\Validator')
            ->disableOriginalConstructor()
            ->setMethods(['validateEmail'])
            ->getMock();
        $validator->expects($this->once())
            ->method('validateEmail')
            ->with($this->equalTo($email));

        $passwordResetService = $this->getMockBuilder('App\Services\PasswordResetService')
            ->disableOriginalConstructor()
            ->setMethods(['makeRequest'])
            ->getMock();
        $passwordResetService->expects($this->once())
            ->method('makeRequest')
            ->with($this->equalTo($email));

        $this->container->set('Validator', $validator);
        $this->container->set('PasswordResetService', $passwordResetService);

        $controller = new AuthController([
            'container' => $this->container
        ]);

        // run
        $this->setRequestParam('email', $email);
        $controller->requestPasswordResetAction();

        // post-run assertions
        $controller->isResponse('status', 1);
    }

    /**
     * test: requestPasswordReset quietly fails
     */
    public function testRequestPasswordResetQuietlyFails()
    {
        // param
        $email = 'bob@email.com';

        // mocks
        $validator = $this->getMockBuilder('App\Helpers\Validator')
            ->disableOriginalConstructor()
            ->setMethods(['validateEmail'])
            ->getMock();
        $validator->expects($this->once())
            ->method('validateEmail')
            ->with($this->equalTo($email));

        $this->container->set('Validator', $validator);

        $exception = new AppException('something', 'Something');

        $passwordResetService = $this->getMockBuilder('App\Services\PasswordResetService')
            ->disableOriginalConstructor()
            ->setMethods(['makeRequest'])
            ->getMock();
        $passwordResetService->expects($this->once())
            ->method('makeRequest')
            ->with($this->equalTo($email))
            ->will($this->throwException($exception));

        $this->container->set('PasswordResetService', $passwordResetService);

        $dependencies = [
            'container' => $this->container
        ];

        $controller = new AuthController($dependencies);

        // run
        $this->setRequestParam('email', $email);
        $controller->requestPasswordResetAction();

        // post-run assertions
        $controller->isResponse('status', 1);
    }

    // ==================================
    //
    // Reset Password
    //

    /**
     * test: resetPassword
     */
    public function testResetPassword()
    {
        // param
        $token = 'a_token';
        $password = 'a_password';

        // mock
        $validator = $this->getMockBuilder('App\Helpers\Validator')
            ->disableOriginalConstructor()
            ->setMethods(['validatePassword'])
            ->getMock();
        $validator->expects($this->once())
            ->method('validatePassword')
            ->with($this->equalTo($password));

        $this->container->set('Validator', $validator);

        $passwordResetService = $this->getMockBuilder('App\Services\PasswordResetService')
            ->disableOriginalConstructor()
            ->setMethods(['set'])
            ->getMock();
        $passwordResetService->expects($this->once())
            ->method('set')
            ->with($this->equalTo($token), $this->equalTo($password));

        $this->container->set('PasswordResetService', $passwordResetService);

        $dependencies = [
            'container' => $this->container
        ];

        $controller = new AuthController($dependencies);

        // run
        $this->setRequestParam('token', $token);
        $this->setRequestParam('password', $password);
        $controller->resetPasswordAction();

        // post-run assertions
        $controller->isResponse('status', 1);
    }
}