<?php
namespace Tests\Http\Controllers;

use Tests\TestCase;
use App\Http\Controllers\UserController;
use App\Models\Domains\User;

class UserControllerTest extends TestCase
{
    // ===========================
    //
    // Create
    //

    /**
     * test: create
     */    
    public function testCreate()
    {
        // param
        $email = 'somebody@somewhere.me';
        $password = 'abc123';
        $firstName = 'Some';
        $lastName = 'Body';
        
        // mocks
        $validator = $this->getMockBuilder('Validator')
            ->disableOriginalConstructor()
            ->setMethods(['validateEmail', 'validateName', 'validatePassword'])
            ->getMock();
        $validator->expects($this->at(0))
            ->method('validateEmail')
            ->with($this->equalTo($email));
        $validator->expects($this->at(1))
            ->method('validateName')
            ->with($this->equalTo($firstName));
        $validator->expects($this->at(2))
            ->method('validateName')
            ->with($this->equalTo($lastName));
        $validator->expects($this->at(3))
            ->method('validatePassword')
            ->with($this->equalTo($password));
        
        $token = 'a_token';
        $userId = 'a_user_id';
        
        $userService = $this->getMockBuilder('UserService')
            ->disableOriginalConstructor()
            ->setMethods(['create'])
            ->getMock();
        $userService->expects($this->once())
            ->method('create')
            ->with($this->equalTo($email), 
                $this->equalTo($firstName), 
                $this->equalTo($lastName), 
                $this->equalTo($password))
            ->willReturn($token);
            
        $this->container->set('Validator', $validator);
        $this->container->set('UserService', $userService);
            
        $dependencies = (object) [
            'container' => $this->container
        ];
        $userController = new UserController($dependencies);
        
        // run
        $userController->setRequestParam('email', $email);
        $userController->setRequestParam('password', $password);
        $userController->setRequestParam('firstName', $firstName);
        $userController->setRequestParam('lastName', $lastName);
        
        $userController->createAction();
        
        // post-run assertion
        $userController->isResponse('token', $token);
        $userController->isResponse('user_id', $userId);
    }

    // ===========================
    //
    // Change Password
    //

    /**
     * test: change password
     */
    public function testChangePassword()
    {
        // param
        $userId = 2;
        $user = new User((object) [
            'id' => $userId
        ]);
        $newPassword = 'this_is_a_secret';

        // mock
        $userMapper = $this->getMockBuilder('\App\Models\Mappers\UserMapper')
            ->disableOriginalConstructor()
            ->setMethods(['changePassword'])
            ->getMock();
        $userMapper->expects($this->once())
            ->method('changePassword')
            ->with($this->equalTo($userId), $this->equalTo($newPassword));

        $tokenMapper = $this->getMockBuilder('\App\Models\Mappers\TokenMapper')
            ->disableOriginalConstructor()
            ->setMethods(['deleteByUserId'])
            ->getMock();
        $tokenMapper->expects($this->once())
            ->method('deleteByUserId')
            ->with($this->equalTo($userId));

        $this->container->set('TokenMapper', $tokenMapper);
        $this->container->set('UserMapper', $userMapper);

        $userController = $this->getMockBuilder('\App\Http\Controllers\UserController')
            ->setMethods(['getAuthorizedUser'])
            ->getMock();
        $userController->expects($this->once())
            ->method('getAuthorizedUser')
            ->willReturn($user);

        // run
        $this->setRequestParam('newPassword', $newPassword);
        $userController->changePasswordAction();

        // post-run assertions
        $userController->isResponse('status', 1);
    }
}