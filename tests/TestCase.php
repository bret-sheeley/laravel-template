<?php
namespace Tests;

use Illuminate\Support\Facades\Request as Input;

use Illuminate\Foundation\Testing\TestCase as FoundationTestCase;
use Illuminate\Contracts\Console\Kernel;

use App\Helpers\{
    DependencyContainer,
    Logger
};
use App\Http\Controllers\BaseController;

class TestCase extends FoundationTestCase
{
	private static $_input = null;
	protected $container;
	protected $logger;

	/**
	 * container
	 *
	 * @param DependencyContainer $container
     * @param Logger $logger
	 */
	public function __construct(DependencyContainer $container = null, Logger $logger = null)
	{
	    $this->container = ($container == null) ? new DependencyContainer() : $container;
        $this->logger = ($logger == null) ? new Logger() : $logger;
	    
	    parent::__construct();
	}

	public function mockLog()
	{
		$log = $this->getMockBuilder('App\Helpers\Logger')
			->disableOriginalConstructor()
			->setMethods(['debug', 'info', 'notice', 'warning', 'error', 'critical', 'alert', 'emergency'])
			->getMock();
		$this->logger = $log;

		return $log;
	}

	public function mockController($controllerName = 'BaseController', $returnedUserId = 1, $methods = [])
	{
		if ($controllerName[0] !== '\\') {
			$controllerName = '\App\Http\Controllers\\' . $controllerName;
		}

		$methods = array_merge(array('checkUserSession', 'checkNurseSession', 'checkAdminSession', 'checkWebsiteSession', 'checkTimeZoneShift'), $methods);

		$controller = $this->getMockBuilder($controllerName)
			->disableOriginalConstructor()
			->setMethods($methods)
			->getMock();
		$controller->expects($this->any())
			->method('checkUserSession')
			->willReturn($returnedUserId);
		$controller->expects($this->any())
			->method('checkAdminSession')
			->willReturn($returnedUserId);
		$controller->expects($this->any())
			->method('checkNurseSession')
			->willReturn($returnedUserId);
		$controller->expects($this->any())
			->method('checkWebsiteSession')
			->willReturn(true);

		return $controller;
	}

 	/**
 	 * getRequests
 	 */
 	public function getRequests()
 	{
 		if (self::$_input == null) {
	 		self::$_input = new Input();
 		}

 		return self::$_input;
 	}

	protected function setRequestParam($key, $value)
	{
		$_POST[$key] = $value;
		$_REQUEST[$key] = $value;

		BaseController::setRequestParam($key, $value);
	}

	protected function setHeader($key, $value)
	{
		BaseController::setHeaderResponse($key, $value);
	}

	private function reset()
	{
		$_POST = [];
		$_REQUEST = [];
        BaseController::clearRequests();

		$this->mockLog();
	}

	public function setUp()
	{
		parent::setUp();
		$this->reset();
	}

	public function tearDown()
	{
		parent::tearDown();
		$this->reset();
	}

	/**
	 * Creates the application.
	 */
	public function createApplication()
	{
		$app = require __DIR__.'/../bootstrap/app.php';
		$app->make(Kernel::class)->bootstrap();

		return $app;
	}

}
