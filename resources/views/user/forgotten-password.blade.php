<html>
<body>
<p>
    Hello {{ $firstName }}
</p>
<p>
    You are receiving this email due to a request made to reset your password. If you wish to do so, click the link below.
</p>
<p>
    http://some.link.com/?token={{ $token }}
</p>
<p>
    Thank you,<br />
    The Game Team
</p>
</body>
</html>