<?php

namespace App\Http\Middleware;

use App\Helpers\Traits\DependencyTrait;
use Illuminate\Cookie\Middleware\EncryptCookies as Middleware;

class BaseMiddleware extends Middleware
{
    use DependencyTrait;
    
    /**
     * construct
     */
    public function __construct($dependencies = null)
    {
        $this->setDependencies($dependencies);
    }
}