<?php

namespace App\Http\Middleware;

use App\Exceptions\AppException;
use App\Http\Controllers\BaseController;
use Illuminate\Support\Facades\Response;
use Exception;
use Closure;

class Authentication extends BaseMiddleware
{
    private $fatalErrorMessage = 'fatal_error';
    
    /**
     * fatalErrorCheck
     */
    public function fatalErrorCheck()
    {
        $error = error_get_last();
        if ($error !== null) {
            $this->logger->error(json_encode(['error' => $this->fatalErrorMessage, 'code' => 500]));
            exit;
        }
    }
    
    /**
     * Respond
     * 
     * @param integer $httpCode Http Code
     */
    public function respond($httpCode = 200)
    {
        $baseController = new BaseController();
        $output = $baseController->getOutput();

        switch ($baseController->getResponseType()) {
            case 'json':
                $this->logger->info(json_encode($output) . ', ' . $httpCode);
                return Response::json($output, $httpCode);
            case 'string':
                $this->logger->info(BaseController::$_string);
                return BaseController::$_string;
            case 'xml':
                $this->logger->info(BaseController::$_xmlResponse);
                return Response::make(trim(BaseController::$_xmlResponse))
                    ->header('Content-Type', 'text/xml');
            default:
                return Response::json(['error' => 'fatal_response_error']);
        }
    }

    /**
     * get token
     *
     * @param $request
     */
    public function getToken($request)
    {
        $authorization = $request->header('Authorization');

        if ($authorization) {
            $splitAuthorization = explode(' ', $authorization);
            if (count($splitAuthorization) == 2 && $splitAuthorization[0] == 'Bearer') {
                return $splitAuthorization[1];
            }
        }

        throw new AppException('invalid_token', 'Invalid Authorization Token', 400);
    }
    
    /**
     * run controller
     */
    public function runController($request, $next, $guard, $authenticate)
    {
        register_shutdown_function([$this, 'fatalErrorCheck']);
        
        try {
            if ($authenticate) {

                $token = $this->getToken($request);

                $user = $this->container->get('AuthenticationService')
                    ->findAuthenticatedUser($token);

                $baseController = new BaseController();
                $baseController->setAuthorizedUser($user);
            }

            // run API call (Controller on down)
            $response = $next($request);

            // handle exceptions
            $exception = $response->exception;
            if ($exception instanceof \Exception) {
                throw $exception;
            }

            // handle response
            $results = $this->respond();

        } catch (AppException $e) {

            // Application Exception Handling
            $responseData = ['error' => $e->getMessage()];
            if (env('APP_DEBUG') == true) {
                $responseData['trace'] = $e->getTraceAsString();
            }
            $results = Response::json(['error' => $responseData], $e->getCode());

        } catch (\Exception $e) {

            // Error Handling
            $message = (env('APP_DEBUG', false) == true)
                ? $e->getMessage()
                : $this->fatalErrorMessage;

            $responseData = ['error' => $message];
            if (env('APP_DEBUG') == true) {
                $responseData['trace'] = $e->getTraceAsString();
            }
            $results = Response::json(['error' => $responseData, 500]);
        }
        
        return $results;
    }
    
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        return $this->runController($request, $next, $guard, true);
    }
}
