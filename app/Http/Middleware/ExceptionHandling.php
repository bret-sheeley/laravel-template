<?php

namespace App\Http\Middleware;

use Closure;

class ExceptionHandling extends Authentication
{
    
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
        return $this->runController($request, $next, $guard, false);
    }
}