<?php
namespace App\Http\Controllers;

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\View;

use App\Exceptions\{
    AppException
};

use App\Helpers\Traits\DependencyTrait;
use App\Http\Controllers\Traits\{
    AuthorizedUserTrait,
    RequestTrait,
    ResponseTrait,
    TestTrait,
    ViewTrait
};

class BaseController extends Controller
{
    use AuthorizedUserTrait;
    use DependencyTrait;
    use RequestTrait;
    use ResponseTrait;
    use TestTrait;
    use ViewTrait;

    /**
     * constructor
     */
    public function __construct($dependencies = null)
    {
        $this->setDependencies($dependencies);
    }
}
