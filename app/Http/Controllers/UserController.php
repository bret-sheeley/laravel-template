<?php
namespace App\Http\Controllers;

class UserController extends BaseController
{
    /**
     * change password
     */
    public function changePasswordAction()
    {
        $user = $this->getAuthorizedUser();
        $newPassword = $this->getRequestParam('newPassword');

        // change password
        $this->container->get('UserMapper')
            ->changePassword($user->getId(), $newPassword);

        // log the user out
        $this->container->get('TokenMapper')
            ->deleteByUserId($user->getId());

        // response successfully
        $this->setResponseParam('status', 1);
    }

    /**
     * create
     */
    public function createAction()
    {
        $email = $this->getRequestParam('email');
        $password = $this->getRequestParam('password');
        $firstName = $this->getRequestParam('firstName');
        $lastName = $this->getRequestParam('lastName');
        
        $validator = $this->container->get('Validator');
        
        $validator->validateEmail($email);
        $validator->validateName($firstName);
        $validator->validateName($lastName);
        $validator->validatePassword($password);

        $token = $this->container->get('UserService')
            ->create($email, $firstName, $lastName, $password);

        $this->setResponseParam('token', $token);
    }
}
