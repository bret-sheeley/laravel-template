<?php
namespace App\Http\Controllers;

use App\Exceptions\AppException;

class AuthController extends BaseController
{
    /**
     * login
     */
    public function loginAction()
    {
        $email = $this->getRequestParam('email');
        $password = $this->getRequestParam('password');

        list($token, $user) = $this->container->get('AuthenticationService')
            ->login($email, $password);

        $this->setResponseParam('token', $token);
        $this->setResponseParam('user', $user->getProfile());
    }

    /**
     * logout
     */
    public function logoutAction()
    {
        $user = $this->getAuthorizedUser();

        $this->container->get('TokenMapper')
            ->deleteByUserId($user->getId());

        $this->setResponseParam('status', 1);
    }

    /**
     * request password reset
     */
    public function requestPasswordResetAction()
    {
        $email = $this->getRequestParam('email');

        $validator = $this->container->get('Validator');
        $validator->validateEmail($email);

        try {
            $this->container->get('PasswordResetService')
                ->makeRequest($email);
        } catch (AppException $e) {
            // do nothing
        }

        $this->setResponseParam('status', 1);
    }

    /**
     * reset password
     */
    public function resetPasswordAction()
    {
        $token = $this->getRequestParam('token');
        $password = $this->getRequestParam('password');

        $validator = $this->container->get('Validator');
        $validator->validatePassword($password);

        $this->container->get('PasswordResetService')->set($token, $password);

        $this->setResponseParam('status', 1);
    }

}