<?php
/**
 * Created by PhpStorm.
 * User: bretsheeley
 * Date: 5/21/18
 * Time: 2:23 PM
 */

namespace App\Http\Controllers\Traits;


trait ViewTrait
{
    /**
     * Render View
     */
    public function renderView($template, $data = array())
    {
        array_walk_recursive($data, 'self::filterViewData');
        return view($template, $data);
    }

    /**
     * filter view data
     *
     * @param $value
     */
    public function filterViewData(&$value)
    {
        $value = htmlspecialchars($value, ENT_QUOTES, 'UTF-8');
        $allowedHtml = array('p', 'br', 'b', 'i', 'u', 'h1', 'h2', 'h3', 'h4', 'h5', 'strong');

        foreach ($allowedHtml as $tag) {
            $value = str_replace('&lt;'  . $tag . '&gt;', 	'<' . $tag . '>'	, $value);
            $value = str_replace('&lt;/' . $tag . '&gt;', 	'</' . $tag . '>'	, $value);
            $value = str_replace('&lt;'  . $tag . ' /&gt;', '<' . $tag . ' />'	, $value);
        }
    }

}