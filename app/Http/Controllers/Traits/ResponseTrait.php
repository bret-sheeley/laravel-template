<?php
namespace App\Http\Controllers\Traits;

trait ResponseTrait
{
    protected static $_output = [];

    protected static $_response = null;
    protected static $_responseType = 'json';

    protected static $_version = null;
    public static $_xmlResponse = '';
    protected static $_xmlRootElementName = 'root';

    public static $_string = '';

    public function getOutput()
    {
        return static::$_output;
    }

    public function getResponseParam($key)
    {
        return (isset(self::$_output[$key])) ? self::$_output[$key] : null;
    }

    public static function clearOutput()
    {
        static::$_output = [];
    }

    /**
     * set header
     */
    public static function setHeaderResponse($key, $value)
    {
        static::$_headers = array(
            $key => $value
        );
    }

    /**
     * Get Response Type
     */
    public function getResponseType()
    {
        return static::$_responseType;
    }

    /**
     * Set Response Type
     */
    public function setResponseType($value)
    {
        $validTypes = array('json', 'xml', 'string');
        $value = strtolower($value);

        static::$_responseType = (in_array($value, $validTypes)) ? $value : 'json';
    }

    /**
     * Set Xml Response
     */
    public function setXmlResponse($value)
    {
        $this->setResponseType('xml');
        static::$_xmlResponse = $value;
    }

    public function responseParamExists($key)
    {
        return array_key_exists($key, self::$_output);
    }

    public function setResponseParam($key, $value)
    {
        $valueString = (is_array($value)) ? json_encode($value) : $value;
        static::$_output[$key] = $value;
    }

    public function setResponseString($text)
    {
        static::$_responseType = 'string';
        static::$_string = $text;
    }

    /**
     * set output header
     */
    public function setOutputHeader($header)
    {
        header($header);
    }

    public function echoString($message)
    {
        if ($this->isDebug()) {
            echo PHP_EOL . $message . PHP_EOL;
        }
    }

    public function isDebug()
    {
        return env('APP_DEBUG', false);
    }

}