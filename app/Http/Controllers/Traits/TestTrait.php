<?php
namespace App\Http\Controllers\Traits;

trait TestTrait
{
    /**
     * test
     */
    public function testAction()
    {
        $this->setResponseParam('hi', 'yo');
    }

    /**
     * are identical arrays?
     */
    private function areIdenticalArrays($firstArray, $secondArray)
    {
        return ($this->arrayDiffAssocRecursive($firstArray, $secondArray) === array());
    }

    /**
     * Recursive array_diff_assoc comparison
     */
    private function arrayDiffAssocRecursive($array1, $array2)
    {
        $difference = [];
        if (!is_array($array1)) {
            $difference = $array1;
        } else {
            foreach ($array1 as $key => $value) {
                if (is_array($value)) {
                    if (!isset($array2[$key]) || !is_array($array2[$key])) {
                        $difference[$key] = $value;
                    } else {
                        $newDiff = $this->arrayDiffAssocRecursive($value, $array2[$key]);
                        if(!empty($newDiff)) {
                            $difference[$key] = $newDiff;
                        }
                    }
                } else if(!is_array($array2) || !array_key_exists($key,$array2) || $array2[$key] !== $value) {
                    $difference[$key] = $value;
                }
            }
        }

        if ($difference !== array()) {
            $this->echoString('Expected ' . $this->varDump($array2) . ' does not match ' . $this->varDump($array1));
        }

        return $difference;
    }

    /**
     * isResponse (for testing)
     */
    public function isResponse($key, $value)
    {
        if (!$this->responseParamExists($key)) return false;

        $responseParam = $this->getResponseParam($key);

        if (!is_array($responseParam) && !is_array($value)) {
            $isMatch = ($responseParam === $value);

            if (!$isMatch) {
                $this->echoString('Expected: ' . $value. ' does not match ' . $responseParam);
            }

        } else {
            $isMatch = $this->areIdenticalArrays($responseParam, $value);
        }

        return $isMatch;
    }

    /**
     * varDump
     */
    public function varDump($stuff)
    {
        ob_start();
        var_dump($stuff);
        return ob_get_clean();
    }

}