<?php
namespace App\Http\Controllers\Traits;

use App\Exceptions\AppException;
use App\Helpers\Time;

trait AuthorizedUserTrait
{
    protected static $_user = null;

    /**
     * Check User Session
     */
    public function checkUserSession($token)
    {
        $tokenMapper = $this->container->get('TokenMapper');
        $time = $this->container->get('Time');
        $userMapper = $this->container->get('UserMapper');

        $exception = new AppException('no_active_session',
            'No active session for token, ' . $token);

        try {
            $token = $tokenMapper->findByToken($token);
            $nowUnixtime = $time->now('U');
            if (strtotime($token->getLastAccessed()) + Time::ONE_DAY <= $nowUnixtime) {
                throw $exception;
            }
        } catch (AppException $e) {
            throw $exception;
        }

        return $userMapper->findById($token->getUserId());
    }

    /**
     * set authorized user
     */
    public function setAuthorizedUser($user)
    {
        static::$_user = $user;
    }

    /**
     * get authorized user
     */
    public function getAuthorizedUser()
    {
        return static::$_user;
    }
}