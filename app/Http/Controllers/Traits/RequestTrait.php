<?php
namespace App\Http\Controllers\Traits;

use Illuminate\Http\Request;

trait RequestTrait
{

    protected static $_request = null;
    protected static $_headers = null;
    protected static $_input = null;

    /**
     * clear requests
     */
    public static function clearRequests()
    {
        static::$_request = null;
        static::$_headers = null;
        static::$_input = null;
        $_REQUEST = [];
        $_GET = [];
        $_POST = [];
    }

    /**
     * get request param
     *
     * @param $key
     * @param null $default
     * @param bool $allowEmptyString
     * @return array|mixed|null|string
     */
    public function getRequestParam($key, $default = null, $allowEmptyString = true)
    {
        if (static::$_input === null) {
            $logData = $this->getRequests();
            unset($logData['password']); // don't log passwords
            unset($logData['ssn']); // don't log ssn
            $this->logger->info(\Route::currentRouteName() . PHP_EOL . json_encode($logData));
        }

        $results = (array_key_exists($key, static::$_input)) ? static::$_input[$key] : null;

        $decodedResults = (is_string($results)) ? json_decode($results, true) : null;
        $selectedResults = (!is_array($decodedResults) || $decodedResults === []) ? $results : $decodedResults;

        if (!$allowEmptyString) {
            if (is_string($selectedResults) && trim($selectedResults) === '') {
                $selectedResults = null;
            }
        }

        return (is_null($selectedResults)) ? $default : $selectedResults;
    }

    /**
     * getRequests
     */
    public function getRequests()
    {
        if (self::$_input == null) {
            $request = $this->getRequest();

            $data = $request->getContent();
            $getData = $_REQUEST;

            if ($this->isJson($data)) {
                $data = json_decode($data, true);
            } else if (!is_array($data)) {
                $tempData = $data;
                parse_str($tempData, $data);
            }

            $data = (is_array($data)) ? array_merge($getData, $data) : $getData;

            self::$_input = (is_array($data)) ? $data : array();
        }

        return self::$_input;
    }

    /**
     * getRequest
     *
     * @return Request|null
     */
    public function getRequest()
    {
        if (is_null(self::$_request)) {
            self::$_request = new Request();
        }

        return self::$_request;
    }

    public static function setRequestParam($key, $value)
    {
        $_POST[$key] = $value;
        $_REQUEST[$key] = $value;
        self::$_input[$key] = $value;
    }

    public function getAllHeaders()
    {
        foreach ($_SERVER as $key => $value) {
            if (substr($key, 0, 5) === 'HTTP_') {
                $key = str_replace(' ', '-', ucwords(strtolower(str_replace('_', ' ', substr($key, 5)))));
                $results[$key] = $value;
            } else {
                $results[$key] = $value;
            }
        }

        return $results;
    }

    public function getHeader($tag, $default = null)
    {
        $header = $this->getHeaders();
        return (isset($header[$tag])) ? $header[$tag] : $default;
    }

    /**
     * getHeaders
     *
     * @return null
     */
    public function getHeaders()
    {
        if (is_null(static::$_headers)) {
            static::$_headers = $this->getAllHeaders();
        }

        return static::$_headers;
    }

    /**
     * isJson
     */
    public function isJson($string) {
        json_decode($string, true);
        return (json_last_error() == JSON_ERROR_NONE);
    }

}