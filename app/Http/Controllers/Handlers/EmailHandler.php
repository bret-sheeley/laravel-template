<?php
namespace App\Http\Controllers\Handlers;

use App\Http\Controllers\BaseController;

class EmailHandler extends BaseController implements HandlerInterface
{
    /**
     * Process
     */
    public function process($message)
    {
        try {
            $to = $message['to'];
            $type = $message['email_type'];
            $data = (isset($message['data'])) ? $message['data'] : array();
            $ccs = (isset($message['ccs'])) ? $message['ccs'] : array();
            $bccs = (isset($message['bccs'])) ? $message['bccs'] : array();

            // send a copy of the order to support
            $this->container->get('EmailWrapper')->processMessage($to, $type, $data, $ccs, $bccs);

            $status = true;

            usleep(250000);
        } catch (\Exception $e) {
            $this->container->get('Logger')->error('Error: ' . $e->getMessage());
            $status = false;
            sleep(1);
        }

        return $status;
    }
}