<?php
namespace App\Http\Controllers\Handlers;
interface HandlerInterface
{
    public function process($message);
}