<?php
namespace App\Models\Mappers;

use App\Exceptions\AppException;
use App\Helpers\{
    Random,
    Time
};

/**
 * Token Mapper
 * @author bretsheeley
 *
 */
class PasswordResetMapper extends BaseMapper
{
    private $tableName = 'password_resets';

    /**
     * add
     */
    public function add($email)
    {
        $token = $this->container->get('Random')
            ->generateUniqueToken(200, Random::ALPHANUMERIC);

        $this->getQuery($this->tableName)
            ->insert([
                'email' => $email,
                'token' => $token
            ]);

        return $token;
    }

    /**
     * findByToken
     */
    public function findByToken($token)
    {
        $record = $this->getQuery($this->tableName)
            ->where('token', $token)
            ->first();

        if (!isset($record['email'])) {
            throw new AppException('invalid_token', 'Invalid Token Provided ' . $token);
        }

        return $record['email'];
    }

    /**
     * remove by email
     */
    public function removeByEmail($email)
    {
        $this->getQuery($this->tableName)
            ->where('email', $email)
            ->delete();
    }
}