<?php
namespace App\Models\Mappers;

use App\Helpers\Traits\DependencyTrait;

use App\Models\Mappers\Traits\{
    AuthenticationTrait,
    DatabaseTrait,
    HydrationTrait
};

class BaseMapper
{
    use DependencyTrait;
    use AuthenticationTrait;
    use DatabaseTrait;
    use HydrationTrait;

    /**
     * Constructor
     */
    public function __construct($dependencies = null, $databaseName = null)
    {
        $this->setDependencies($dependencies, ['cache', 'container', 'factory', 'logger']);
        
        $this->initDb($databaseName);
    }

}