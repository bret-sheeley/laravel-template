<?php
namespace App\Models\Mappers;

use App\Exceptions\AppException;
use App\Helpers\Time;

/**
 * Token Mapper
 * @author bretsheeley
 *
 */
class TokenMapper extends BaseMapper
{
    private $tableName = 'tokens';
    private $domainName = 'Token';
    private $cachePrefixToken = 'token_';

    // ========================================================
    //
    // findByToken
    //
    
    /**
     * findByTokenViaDatabase
     */
    public function findByTokenViaDatabase($parameters)
    {
        return $this->getQuery($this->tableName)
            ->where('token', $parameters->data->token)
            ->first();
    }

    /**
     * find by token
     *
     * @param $token
     * @return boolean
     */
    public function findByToken($token)
    {
        // define exception
        $exception = new AppException('invalid_token', 401,
            'Invalid token ' . $token);
        
        // find token data
        $parameters = (object) [
            'data' => (object) [
                'token' => $token,
            ],
            'database' => [ $this, 'findByTokenViaDatabase'],
            'cache' => (object) [
                'key' => $this->cachePrefixToken . $token,
                'ttl' => Time::ONE_DAY    
            ]
        ];
        
        $record = $this->find($parameters, $exception);

        // Return token object
        return $this->toObject($this->domainName, $record);
    }

    // =====================================
    //
    // insert
    
    /**
     * insert 
     * @param $user
     */
    public function insert($user)
    {
        // generate new token
        $token = $this->container->get('Random')
            ->generateUniqueToken(160);
        
        // clear out old tokens
        $this->getQuery($this->tableName)
            ->where('user_id', $user->getId())
            ->delete();
        
        // add new token
        $this->getQuery($this->tableName)
            ->insert([
                'token' => $token,
                'user_id' => $user->getId()
            ]);
            
        return $token;
    }
    
    /**
     * updateLastAccessed
     */
    public function updateLastAccessed($token)
    {
        $now = $this->container->get('Time')->now();
        
        $this->getQuery($this->tableName)
            ->where('token', $token)
            ->update(['last_accessed' => $now]);
    }

    // =============================
    //
    // delete

    /**
     * delete
     *
     * @param string $userId
     */
    public function deleteByUserId($userId)
    {
        $this->getQuery($this->tableName)
            ->where('user_id', $userId)
            ->delete();
    }

    /**
     * delete old tokens
     */
    public function deleteOldTokens($thresholdTime)
    {
        $this->getQuery($this->tableName)
            ->where('last_accessed', '<', $thresholdTime)
            ->delete();
    }
}