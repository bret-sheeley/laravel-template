<?php
namespace App\Models\Mappers\Traits;

use Illuminate\Support\Facades\DB;
use App\Exceptions\{
    AppException,
    Exception
};


trait DatabaseTrait
{
    protected static $databases = [];
    protected static $dbMap = [];
    
    /**
     * get default connection tag
     */
    protected function getDefaultConnectionTag()
    {
        $isTest = (defined('APPLICATION_MODE') && APPLICATION_MODE == 'test');
        $connectionName = ($isTest) ? env('DB_TEST_CONNECTION') : env('DB_CONNECTION');
        
        return $connectionName;
    }
    
    /**
     * init db
     */
    protected function initDb($databaseName = null)
    {
        if (is_null($databaseName)) {

            $dbConnectionTag = $this->getDefaultConnectionTag();
            
            $database = DB::connection($dbConnectionTag);
            $database->enableQueryLog();
            $this->setDb($database);
        } else {
            $database = DB::connection($databaseName);
            $database->enableQueryLog();
            $this->setDb($database, $databaseName);
        }
    }
    
    /**
     * get db
     */
    protected function getDb()
    {
        $class = get_called_class();
        
        $dbTag = (isset(static::$dbMap[$class]))
            ? static::$dbMap[$class]
            : $this->getDefaultConnectionTag();
        
        DB::enableQueryLog();
        
        return static::$databases[$dbTag];
    }
    
    /**
     * get query
     *
     * @return
     */
    protected function getQuery($tableName)
    {
        return $this->getDb()->table($tableName);
    }
    
    /**
     * set Db
     *
     * @param $database
     * @param $databaseName
     */
    public function setDb($database, $databaseName = null)
    {
        $class = get_called_class();
        
        if ($databaseName === null) {
            $dbConnectionTag = $this->getDefaultConnectionTag();
        }
        
        static::$dbMap[$class] = $dbConnectionTag;
        static::$databases[$dbConnectionTag] = $database;
    }
    
    /**
     * get query expression
     */
    protected function getQueryExpression($expression)
    {
        return DB::raw($expression);
    }
    
    /**
     * find
     *
     * @param Object $parameters
     * @param AppException|Exception|null $exception
     */
    public function find($parameters, $exception = null)
    {
        // validate db definition
        if (!property_exists($parameters, 'database') ||
            !is_array($parameters->database) ||
            count($parameters->database) !== 2) {
            throw new \Exception('invalid_database_closure_definition');
        }


        // get cached record if available
        $results = null;
        if (isset($parameters->cache->key)) {
            $this->cache->get($parameters->cache->key);
        }


         // if the record wasn't found,
         // search the db
        if ($results == null) {

            $obj = $parameters->database[0];
            $methodName = $parameters->database[1];

            $results = $obj->$methodName($parameters);

            // if still not found, 
            //   throw the exception (if given)
            //   or (if not given) return null
            if ($results == null) {
                if ($exception !== null) {
                    throw $exception;
                } else {
                    return null;
                }
            }

            // cache the results
            if (property_exists($parameters->cache, 'key') &&
                property_exists($parameters->cache, 'ttl')) {
                $this->cache->set($parameters->cache->key, $results, $parameters->cache->ttl);
            }
        }
        
        // return results
        return $results;
    }
}