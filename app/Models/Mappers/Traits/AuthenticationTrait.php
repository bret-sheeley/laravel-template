<?php
namespace App\Models\Mappers\Traits;

trait AuthenticationTrait
{
    /**
     * Hash/salt new passwords
     * @return string
     */
    public function hash($password)
    {
        return password_hash($password, PASSWORD_DEFAULT);
    }
    
    /**
     * verify password
     */
    public function verifyHash($password, $hash)
    {
        return password_verify($password, $hash);
    }
}