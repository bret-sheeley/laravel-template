<?php
namespace App\Models\Mappers\Traits;

trait HydrationTrait
{
    /**
     * to object
     */
    public function toObject($className, $record)
    {
        $classNameWithNamespace = 'App\Models\Domains\\' . $className;
        return new $classNameWithNamespace($record);
    }
    
    /**
     * to objects
     */
    public function toObjects($className, $records, $idKey = null)
    {
        $objects = array();
        foreach ($records as $key => $record) {
            $key = (is_null($idKey)) ? $key . '' : $record[$idKey] . '';
            $objects[$key] = $this->toObject($className, $record);
        }
        
        return $objects;
    }
    
    /**
     * toGroupedObjects
     */
    public function toGroupedObjects($className, $groupedRecords)
    {
        foreach ($groupedRecords as $key => $records)
        {
            $groupedRecords[$key] = $this->toObjects($className, $records);
        }
        
        return $groupedRecords;
    }
}