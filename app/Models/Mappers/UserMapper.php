<?php
namespace App\Models\Mappers;

use App\Exceptions\AppException;
use App\Helpers\{
    Random,
    Time
};

class UserMapper extends BaseMapper
{    
    private $domainName = 'User';
    private $tableName = 'users';
    private $cachePrefixId = 'user_id_';
    private $cachePrefixEmail = 'user_email_';
    

    // ========================================================
    //
    // Create
    //
    
    /**
     * create
     */
    public function create($email, $firstName, $lastName, $password)
    {
        $userId = $this->getQuery($this->tableName)
            ->insertGetId([
                'email' => $email,
                'first_name' => $firstName,
                'last_name' => $lastName,
                'password' => $this->hash($password)
            ]);
        
        return $this->findById($userId);
    }

    // ========================================================
    //
    // Change Password

    /**
     * change Password
     *
     * @param int $userId
     * @param string $newPassword
     */
    public function changePassword($userId, $newPassword)
    {
        $this->getQuery($this->tableName)
            ->where('id', $userId)
            ->update([
                'password' => $this->hash($newPassword)
            ]);
    }
    
    // ========================================================
    //
    // Find By Email
    //
    
    /**
     * findByEmailViaDatabase
     */
    public function findByEmailViaDatabase($parameters)
    {
        return $this->getQuery($this->tableName)
            ->where('email', $parameters->data->email)
            ->first();
    }
    
    /**
     * findByEmail
     */
    public function findByEmail($email)
    {
        // define exception
        $exception = new AppException('email_not_found', 500, 
            'Email ' . $email . ' not found.');
        
        // find record
        $parameters = (object) [
            'data' => (object) [
                'email' => $email
            ],
            'database' => [ $this, 'findByEmailViaDatabase' ],
            'cache' => (object) [
                'key' => $this->cachePrefixEmail . $email,
                'ttl' => Time::ONE_DAY
            ]
        ];

        $record = $this->find($parameters, $exception);

        // hydrate results
        return $this->toObject($this->domainName, $record);
    }
    
    /**
     * isEmailAvailable
     *
     * @param string $email
     */
    public function isEmailAvailable($email)
    {
        try {
            $this->findByEmail($email);
        } catch (AppException $e) {
            return true;
        }
        
        return false;
    }
    
    /**
     * login
     */
    public function login(string $email, string $password)
    {
        $exception = new AppException('invalid_login', 500,
            'Invalid Login for email ' . $email);
        
        try {
            $user = $this->findByEmail($email);

            if (!$this->verifyHash($password, $user->getPassword())) {
                throw $exception;
            }
        } catch (AppException $e) {
            throw $exception;
        }
        
        return $user;
    }
    
    // ========================================================
    //
    //  Find By User Id
    //
    
    /**
     * findByIdViaDatabase
     */
    public function findByIdViaDatabase($parameters)
    {
        return $this->getQuery($this->tableName)
            ->where('id', $parameters->data->userId)
            ->first();
    }

    /**
     * find by id
     *
     * @param integer $userId
     * 
     * @throws AppException
     */
    public function findById(int $userId)
    {
        // define exception
        $exception = new AppException('user_not_found', 500, 'User of id = ' . $userId . ' not found.');

        // find record
        $parameters = (object) [
            'data' => (object) [
                'userId' => $userId
            ],
            'database' => [ $this, 'findByIdViaDatabase' ],
            'cache' => (object) [
                'key' => $this->cachePrefixId . $userId,
                'ttl' => Time::ONE_DAY
            ]
        ];
        $record = $this->find($parameters, $exception);

        // hydrate results
        return $this->toObject($this->domainName, $record);
    }
}