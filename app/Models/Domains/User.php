<?php
namespace App\Models\Domains;

class User extends BaseDomain
{
    public function getId()
    {
        return $this->getProperty('id');
    }

    public function getEmail()
    {
        return $this->getProperty('email');
    }
    
    public function getPassword()
    {
        return $this->getProperty('password');
    }
    
    public function getFirstName()
    {
        return $this->getProperty('firstName');
    }
    
    public function getLastName()
    {
        return $this->getProperty('lastName');
    }
    
    public function isBanned()
    {
        return $this->toBoolean($this->getProperty('banned'));
    }

    public function getProfile()
    {
        return [
            'email' => $this->getEmail(),
            'firstName' => $this->getFirstName(),
            'lastName' => $this->getLastName()
        ];
    }
}