<?php
namespace App\Models\Domains;

class Token extends BaseDomain
{
    /**
     * get user id
     * 
     * @return string
     */
    public function getUserId()
    {
        return $this->getProperty('userId');
    }
    
    /**
     * get last accessed 
     * 
     * @return string
     */
    public function getLastAccessed()
    {
        return $this->getProperty('lastAccessed');
    }
}