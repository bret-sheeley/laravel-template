<?php
namespace App\Models\Domains;

use Hamcrest\UnknownType;
use App\Helpers\Logger;

/**
 * BaseDomain
 * 
 * @author bretsheeley
 */
class BaseDomain
{
    protected $_data = null;
    
    /**
     * get property
     * 
     * @param String $tag
     * @param UnknownType $default
     * @return string
     */
    protected function getProperty(String $tag, $default = null)
    {
        return property_exists($this->_data, $tag)
            ? $this->_data->$tag
            : $default;
    }
    
    /**
     * set property
     * 
     * @param String $tag
     * @param UnknownType $value
     */
    protected function setProperty(String $tag, $value)
    {
        $this->_data->$tag = $value;
    }
    
    /**
     * to integer
     * @param UnknownType $value
     * @return UnknownType|number
     */
    protected function toInteger(UnknownType $value)
    {
        if ($value === 0 || $value === 1) {
            return $value;
        }
        
        return ($value == true) ? 1 : 0;
    }
    
    /**
     * to boolean
     * 
     * @param $value
     * @param boolean $allowNull
     * @return NULL|UnknownType|bool
     */
    protected function toBoolean($value, bool $allowNull = false)
    {
        if (is_null($value) && $allowNull) {
            return null;
        }
        
        if ($value === true || $value === false)  {
            return $value;
        }
        
        return ($value == 1) ? true : false;
    }
    
    /**
     * convert keys to camel case
     */
    private function convertKeysToCamelCase($dataset)
    {
        $convertedDataset = new \stdClass();
        foreach ($dataset as $key => $value) {
            $newKey = lcfirst(implode('', array_map('ucfirst', explode('_', $key))));
            $convertedDataset->$newKey = $value;
        }
        
        return $convertedDataset;
    }
    
    /**
     * construct 
     * @param array $dataset
     */
    public function __construct($dataset = null)
    {
        if ($dataset !== null) {
            $dataset = $this->convertKeysToCamelCase($dataset);
        }

        $this->_data = ($dataset !== null) 
            ? $dataset
            : new \stdClass();
    }
    
    /**
     * Is deleted?
     */
    public function isDeleted()
    {
        return !$this->toBoolean($this->getProperty('visible', true));
    }
    
    /**
     * Is visible?
     */
    public function isVisible()
    {
        return (boolean) !$this->isDeleted();
    }
    
    /**
     * Get Create Date
     *
     * @return string
     */
    public function getCreateDate()
    {
        return $this->getProperty('createdAt', null);
    }
    
    /**
     * Get Last Modified
     *
     * @return string
     */
    public function getLastModified()
    {
        return $this->getProperty('updatedAt', null);
    }
    
    /**
     * Make Url Friendly
     */
    public function makeUrlFriendly($string)
    {
        $string = strtolower($string);
        $string = preg_replace("/[^A-Za-z0-9 \-]/", '', $string);
        $string = preg_replace('/[[:space:]]+/', '-', $string);
        $string = str_replace('_', '-', $string);
        
        return $string;
    }
    
    /**
     * Set Last Modified
     *
     * @param mixed $value Value
     */
    public function setLastModified($value)
    {
        $this->setProperty('updatedAt', $value);
    }
    
    /**
     * Set Visible
     *
     * @param mixed $value Value
     */
    public function setVisible($value = true)
    {
        $value = ($value === true || $value === 1) ? true : false;
        
        $this->setProperty('visible', $value);
    }
}