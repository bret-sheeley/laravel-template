<?php
namespace App\Services;

use App\Exceptions\AppException;

class UserService extends BaseService
{
    /**
     * create
     */
    public function create($email, $firstName, $lastName, $password)
    {   
        $userMapper = $this->container->get('UserMapper');
        $tokenMapper = $this->container->get('TokenMapper');
        
        // make sure email is available
        if (!$userMapper->isEmailAvailable($email)) {
            throw new AppException('email_unavailable', 400,
                'Email ' . $email . ' is not available.');
        }

        // create user record
        $user = $userMapper->create($email, $firstName, $lastName, $password);

        // create token
        $token = $tokenMapper->insert($user);

        // return login information
        return $token;
    }
}