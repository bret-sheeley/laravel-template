<?php
namespace App\Services;

use App\Exceptions\AppException;
use App\Helpers\EmailWrapper;

class PasswordResetService extends BaseService
{
    /**
     * set
     */
    public function set($token, $password)
    {
        // get dependencies
        $passwordResetMapper = $this->container->get('PasswordResetMapper');
        $userMapper = $this->container->get('UserMapper');

        // Get the associated user
        $email = $passwordResetMapper->findEmailByToken($token);
        $user = $userMapper->findByEmail($email);

        // Change the Password
        $userMapper->changePassword($user->getId(), $password);

        // Clear out old reset requests
        $passwordResetMapper->removeByEmail($email);
    }

    /**
     * make request
     *
     * @param $email
     */
    public function makeRequest($email)
    {
        $user = $this->container->get('UserMapper')->findByEmail($email);
        $token = $this->container->get('PasswordResetMapper')->add($email);

        // Send email with token
        $data = [
            'email' => $email,
            'firstName' => $user->getFirstName(),
            'token' => $token
        ];
        $this->container->get('EmailWrapper')->queue($email, EmailWrapper::RESET_PASSWORD, $data);
    }
}