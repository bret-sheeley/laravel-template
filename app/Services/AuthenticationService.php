<?php
namespace App\Services;

use App\Helpers\Time;

class AuthenticationService extends BaseService
{
    /**
     * clearOldTokens
     */
    public function clearOldTokens()
    {
        $time = $this->container->get('Time');
        $threshold = $time->ago(Time::ONE_WEEK);

        $tokenMapper = $this->container->get('TokenMapper');
        $tokenMapper->deleteOldTokens($threshold);
    }
    
    /**
     * find authenticated user
     */
    public function findAuthenticatedUser($tokenString)
    {
        // get dependencies
        $tokenMapper = $this->container->get('TokenMapper');
        $userMapper = $this->container->get('UserMapper');

        // get token and user id
        $token = $tokenMapper->findByToken($tokenString);
        $userId = $token->getUserId();

        // update last accessed timestamp
        $tokenMapper->updateLastAccessed($tokenString);

        // get user
        return $userMapper->findById($userId);
    }

    /**
     * login
     */
    public function login($email, $password)
    {
        // get dependencies
        $userMapper = $this->container->get('UserMapper');
        $tokenMapper = $this->container->get('TokenMapper');

        // login
        $user = $userMapper->login($email, $password);

        // create token
        $token = $tokenMapper->insert($user);

        // return login information
        return [$token, $user];
    }
}