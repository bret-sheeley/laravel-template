<?php
namespace App\Services;

use App\Helpers\Traits\DependencyTrait;

class BaseService
{
    use DependencyTrait;
    
    protected $container;
    protected $factory;
    protected $logger;
    
    /**
     * constructor
     */
    public function __construct($dependencies = null)
    {
        $this->setDependencies($dependencies);
    }
}