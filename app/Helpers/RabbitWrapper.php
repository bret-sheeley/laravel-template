<?php
namespace App\Helpers;

class RabbitWrapper extends BaseHelper
{
    const APP_EXCHANGE = 'cerebus.topic';

    const EMAIL_ROUTE_KEY = 'email.process';
    const NOTIFICATION_ROUTE_KEY = 'notification.process';

    private $exchanges = [];
    private $queues = [];

    private $queueConfigurations = array(
        'email' => [
            'handler' => 'EmailHandler',
            'routeKey' => self::EMAIL_ROUTE_KEY,
            'exchange' => self::APP_EXCHANGE,
            'failureRouteKey' => 'email.failure'
        ],
        'email-failure' => [
            'handler' => 'EmailHandler',
            'routeKey' => 'email.failure',
            'exchange' => self::APP_EXCHANGE,
            'failureRouteKey' => null
        ],
        'escrow' => [
            'handler' => 'EscrowHandler',
            'routeKey' => 'escrow.process',
            'exchange' => self::APP_EXCHANGE,
            'failureRouteKey' => 'escrow.failure'
        ],
        'escrow-failure' => [
            'handler' => 'EscrowHandler',
            'routeKey' => 'escrow.failure',
            'exchange' => self::APP_EXCHANGE,
            'failureRouteKey' => null
        ],
        'notification' => [
            'handler' => 'NotificationHandler',
            'routeKey' => self::NOTIFICATION_ROUTE_KEY,
            'exchange' => self::APP_EXCHANGE,
            'failureRouteKey' => 'notification.failure'
        ],
        'notification-failure' => [
            'handler' => 'NotificationHandler',
            'routeKey' => 'notification.failure',
            'exchange' => self::APP_EXCHANGE,
            'failureRouteKey' => null
        ],
    );

    /**
     * RabbitWrapper constructor.
     * @param $dependencies
     */
    public function __construct($dependencies = null)
    {
        parent::__construct($dependencies, ['container', 'logger']);
    }

    /**
     * create exchange
     */
    public function createExchange($exchangeName)
    {
        return new RabbitExchange($exchangeName);
    }

    /**
     * getExchange
     */
    public function getExchange($exchangeName)
    {
        if (!array_key_exists($exchangeName, $this->exchanges)) {
            $this->exchanges[$exchangeName] = $this->createExchange($exchangeName);
        }

        return $this->exchanges[$exchangeName];
    }

    /**
     * create queue
     */
    public function createQueue($queueName)
    {
        $exchangeName = $this->queueConfigurations[$queueName]['exchange'];

        return new RabbitQueue(
            $queueName,
            $this->queueConfigurations[$queueName]['routeKey'],
            $this->getExchange($exchangeName)
        );
    }

    /**
     * Get Queue
     */
    public function getQueue($queueName)
    {
        if (!array_key_exists($queueName, $this->queues)) {
            $this->queues[$queueName] = $this->createQueue($queueName);
        }

        return $this->queues[$queueName];
    }

    /**
     * publishMessage
     */
    public function publishMessage($exchangeName, $routeKey, $message)
    {
        $exchange = $this->getExchange($exchangeName);

        $exchange->publish($message, $routeKey);
    }

    /**
     * runSubscriber
     */
    public function runSubscriber($queueName)
    {
        $handlerName = $this->queueConfigurations[$queueName]['handler'];
        $failureRouteKey = $this->queueConfigurations[$queueName]['failureRouteKey'];

        $rabbitQueue = $this->getQueue($queueName);
        $rabbitQueue->run($handlerName, $failureRouteKey);
    }
}