<?php
namespace App\Helpers;

class Factory
{
    /**
     * getDomain
     */
    public function getDomain(String $domainName, Array $args = null)
    {
        $namespacedName = 'App\Models\Domains\\' . $domainName;
        
        return ($args == null)
            ? new $namespacedName()
            : new $namespacedName(... $args);
    }
}