<?php
namespace App\Helpers;

use App\Exceptions\AppException;

class Validator extends BaseHelper
{
    private $minimumLength;
    
    private $minimumNameLength = 2;
    private $minimumPasswordLength = 8;
    
    /**
     * constructor
     */
    public function __construct($dependencies = [], $whiteList = ['container', 'factory', 'logger'])
    {
        parent::__construct($dependencies, $whiteList);

        $this->minimumLength = (object) [
            'name' => 2,
            'password' => 8
        ];
    }
    
    /**
     * validate email
     * 
     * @param string $email
     */
    public function validateEmail(string $email)
    {
        if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
            throw new AppException('invalid_email', 500, 'Invalid email ' . $email);
        }

    }
    
    /**
     * validate name
     */
    public function validateName($name)
    {
        $name = trim($name);
        if (strlen($name) < $this->minimumLength->name) {
            throw new AppException('invalid_name', 500, 'Invalid name ' . $name);
        }
    }
    
    /**
     * validate password
     */
    public function validatePassword(string $password)
    {
        $password = trim($password);
        if (strlen($password) < $this->minimumLength->password) {
            throw new AppException('invalid_password', 500, 'Invalid password');
        }
    }
}