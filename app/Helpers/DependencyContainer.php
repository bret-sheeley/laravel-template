<?php
namespace App\Helpers;

use \Psr\Container\ContainerInterface;
use \App\Exceptions\NotFoundException;
use \App\Exceptions\ContainerException;
use \Exception;

class DependencyContainer implements ContainerInterface
{
    private static $dependencies = [];
    private static $delegateContainer;
    private static $delegateKeys = [];

    /**
     * construct
     */
    public function __construct($delegateContainer = null)
    {
        if ($delegateContainer !== false) {
            self::$delegateContainer = ($delegateContainer == null)
                ? new DependencyContainer(false) : $delegateContainer;
        }
    }

    /**
     * clear
     */
    public function clear()
    {
        self::$dependencies = [];
        self::$delegateContainer = null;
        self::$delegateKeys = [];
    }

    /**
     * get
     */
    public function get($key)
    {
        try {
            $this->initialize($key);
            return self::$dependencies[$key];
        } catch (NotFoundException $e) {
            throw $e;
        } catch (Exception $e) {
            echo $e->getTraceAsString();
            throw new ContainerException($e->getMessage(), $e->getCode());
        }
    }

    /**
     * has
     */
    public function has($key)
    {
        try {
            $this->initialize($key);
        } catch (NotFoundException $e) {
            return false;
        } catch (Exception $e) {
            throw new ContainerException($e->getMessage());
        }

        return true;
    }

    /**
     * set
     */
    public function set($key, $object, $delegates = [])
    {
        self::$dependencies[$key] = $object;
        self::$delegateKeys[$key] = array_keys($delegates);
        foreach ($delegates as $delegateKey => $delegateDependency) {
            self::$delegateContainer->set($delegateKey, $delegateDependency);
        }
    }

    /**
     * initialize
     */
    private function initialize($key)
    {
        $delegateDependencies = [];
        if (self::$delegateContainer !== false && isset(self::$delegateKeys[$key])) {
            foreach (self::$delegateKeys[$key] as $delegateKey) {
                $delegateDependencies[] = self::$delegateContainer->get($delegateKey);
            }
        }

        if (!isset(self::$dependencies[$key])) {
            switch (true) {
                case (class_exists('\App\Helpers\\' . $key)):
                    $namespaced = '\App\Helpers\\' . $key;
                    self::$dependencies[$key] = new $namespaced(... $delegateDependencies);
                    break;
                case (class_exists('\App\Models\Domains\\' . $key)):
                    $namespaced = '\App\Models\Domains\\' . $key;
                    self::$dependencies[$key] = new $namespaced(... $delegateDependencies);
                    break;
                case (class_exists('\App\Models\Mappers\\' . $key)):
                    $namespaced = '\App\Models\Mappers\\' . $key;
                    self::$dependencies[$key] = new $namespaced(... $delegateDependencies);
                    break;
                case (class_exists('\App\Services\\' . $key)):
                    $namespaced = '\App\Services\\' . $key;
                    self::$dependencies[$key] = new $namespaced(... $delegateDependencies);
                    break;
                default:
                    throw new NotFoundException('dependency_not_found');
                    break;
            }
        }
    }
}