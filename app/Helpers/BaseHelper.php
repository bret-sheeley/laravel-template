<?php
namespace App\Helpers;

use App\Helpers\Traits\DependencyTrait;

class BaseHelper
{
    use DependencyTrait;
    
    /**
     * constructor
     */
    public function __construct($dependencies = null, $whitelist = ['container', 'factory', 'logger'])
    {
        $this->setDependencies($dependencies, $whitelist);
    }

    /**
     * Get Application Mode
     */
    public function getApplicationMode()
    {
        return (defined('APPLICATION_MODE')) ? APPLICATION_MODE : null;
    }
    
    /**
     * is Test Mode
     */
    public function isTestMode()
    {
        return ($this->getApplicationMode() === 'test');
    }

    /**
     * Get Environment
     */
    public function getEnvironment()
    {
        return (defined('APP_ENVIRONMENT')) ? APP_ENVIRONMENT : null;
    }

    /**
     * renderView
     */
    public function renderView($view, $data = [])
    {
        return view($view, $data);
    }
    
}