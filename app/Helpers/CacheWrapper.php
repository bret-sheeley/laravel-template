<?php
namespace App\Helpers;

use Illuminate\Support\Facades\Cache;

class CacheWrapper extends BaseHelper
{
	/**
	 * convert key
	 */
	private function convertKey($key)
	{
		$key = str_replace(' ', '-', $key);
		$environment = $this->getEnvironment();
		$appMode = $this->getApplicationMode();
		$app = (is_null($appMode)) ? 'default' : $appMode;
		return $app  . ':' . $environment . ':' . $key;
	}

	/**
	 * convert tag
	 */
	private function convertTag($tag)
	{
		$environment = $this->getEnvironment();
		$appMode = $this->getApplicationMode();
		$app = (is_null($appMode)) ? 'default' : $appMode;

		return $app . ':' . $environment . ':' . $tag;
	}

	/**
	 * Get
	 */
	public function get($key, $tag = null)
	{
		if ($this->isTestMode()) return null;

		$value = null;
		$convertedKey = $this->convertKey($key);

		if (trim($tag) !== '') {
			$convertedTag = $this->convertTag($tag);
			$cacheRecord = Cache::tags($convertedTag)
				->get($convertedKey, null);
		} else {
			$cacheRecord = Cache::get($convertedKey, null);
		}

		// data found
		if (!is_null($cacheRecord) && is_array($cacheRecord)) {

			$value = $this->deserialize($cacheRecord['data']);

			// stampede-prevention
			$ttl = $this->getTTL($key);

			if ($ttl <= 0) {
				$cacheRecord['expiresAt'] = $this->container->get('Time')->add(2);

				if (trim($tag) !== '') {
					Cache::tags($convertedTag)->put($convertedKey, $cacheRecord, 1);
				} else {
					Cache::put($convertedKey, $cacheRecord, 1); // extend data existance in cache for 1 minute
				}
				$value = null; // return nothing (to trigger db query)
			}
		}

		return $value;
	}

	/**
	 * getTTL
	 */
	private function getTTL($key)
	{
		if ($this->isTestMode()) return null;

		$key = $this->convertKey($key);
		$cacheRecord = Cache::get($key, null);

		if (!is_null($cacheRecord) && isset($cacheRecord['expiresAt'])) {

			$expirationTime = $cacheRecord['expiresAt'];

			$ttl = $this->container->get('Time')->sub($expirationTime);

			if ($ttl < 0) {
				$ttl = 0;
			}
		} else {
			$ttl = 0;
		}

		return $ttl;
	}

	/**
	 * Set
	 */
	public function set($key, $value, $ttl = Time::ONE_DAY, $tagName = null)
	{
        if ($this->isTestMode()) return;

		$key = $this->convertKey($key);
		$expiresAt = $this->container->get('Time')->add($ttl);

		$cacheRecord = [
			'data' => $this->serialize($value),
			'expiresAt' => $expiresAt
		];

		$ttlInMinutes = ceil($ttl/60.0) + 1;
		if (trim($tagName) == '') {
			Cache::put($key, $cacheRecord, $ttlInMinutes);
		} else {
			Cache::tags($this->convertTag($tagName))
                ->put($key, $cacheRecord, $ttlInMinutes);
		}
	}

	/**
	 * clear
	 */
	public function clear($key)
	{
		if ($this->isTestMode()) return;
		Cache::forget($this->convertKey($key));
	}

	/**
	 * clearTag
	 */
	public function clearTag($tagName)
	{
		if ($this->isTestMode()) return;
		Cache::tags($this->convertTag($tagName))->flush();
	}

	/**
	 * serialize
	 */
	public function serialize($data)
	{
		return \igbinary_serialize($data);
	}

	/**
	 * deserialize
	 */
	public function deserialize($data)
	{
		return \igbinary_unserialize($data);
	}
}