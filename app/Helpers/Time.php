<?php
namespace App\Helpers;

use \DateTime;
use \DateInterval;


/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
class Time
{   
	const ONE_YEAR = 31557600; // 365.25 days
	const ONE_LEAP_YEAR = 31622400; // 366 days
	const ONE_STANDARD_YEAR = 31536000; // 365 days
	const THIRTY_DAYS = 2592000; // 30 days
	const TWO_WEEKS = 1209600;
	const ONE_WEEK = 604800;
	const SIX_DAYS = 518400;
	const THREE_DAYS = 259200;
	const TWO_DAYS = 172800;
	const THIRTY_SIX_HOURS = 129600;
	const ONE_DAY = 86400;
	const TWELVE_HOURS = 43200;
	const ONE_HOUR = 3600;
	const FIVE_MINUTES = 300;
	
	private static $months = array(
		1 => 'January',
		2 => 'February',
		3 => 'March',
		4 => 'April',
		5 => 'May',
		6 => 'June',
		7 => 'July',
		8 => 'August',
		9 => 'September',
		10 => 'October',
		11 => 'November',
		12 => 'December'
	);
	
    /**
     * Ago
     * 
     * @param integer $sub  Amount to sub to "now"
     * @param integer  $time Time
     * 
     * @return string
     */
    public function ago($sub, $time = null, $format = 'Y-m-d H:i:s')
    {
        if (is_null($time)) {
            $time = $this->now();
        }
        
        return date($format, strtotime($time) - $sub);
    }
	
	/**
	 * microtimestamp
	 */
	public function microtimestamp()
	{
		return floor(microtime(true) * 1000);
	}
	
	/**
	 * Now
	 * 
	 * @return string
	 */
    public function now($format = 'Y-m-d H:i:s')
    {
        return date($format);
    }

    /**
     * unixtime
     */
    public function unixtime()
    {
    	return time();
    }
    
    /**
     * Add
     * 
     * @param integer $add  Amount to add to "now"
     * @param integer  $time Time
     * 
     * @return string
     */
    public function add($add, $time = null, $format = 'Y-m-d H:i:s')
    {
        if (is_null($time)) {
            $time = $this->now();
        }

        return date($format, strtotime($time) + $add);
    }

    /**
     * Add Month
     * 
     * @param integer $add  Amount of months to add to "now"
     * @param integer  $time Time
     * 
     * @return string
     */
    public function addMonths($addMonths, $time = null, $format = 'Y-m-d H:i:s')
    {
        if (is_null($time)) {
            $time = $this->now();
        }

        $date = new DateTime($time);
        $startDay = $date->format('j');
        
        $date->modify('+' . $addMonths . ' month');
        $endDay = $date->format('j');

        if ($startDay != $endDay) {
        	$date->modify('last day of last month');
        }

        return $date->format($format);
    }
    
    /**
     * get end of month
     */
    public function getEndOfMonthTimestamp($month, $year)
    {
    	if ($year < 100) {
    		$year += 2000;
    	}

		$timestamp = sprintf("%4d-%02d-01 00:00:00", $year, $month);
        
        $date = new DateTime($timestamp);
        $date->add(new DateInterval('P1M'));
        
        $date->sub(new DateInterval('PT1S'));
        
        return $date->format('Y-m-d 23:59:59');
    }
    
    /**
     * Until
     * 
     * Returns the number of seconds until the given UTC time arrives. If it has
     * already passed, the call will return 0.
     * 
     * @param string $utcTimestamp UTC Timestamp
     * 
     * @return integer
     */
    public function until($utcTimestamp)
    {
    	$difference = strtotime($utcTimestamp) - strtotime($this->now());
    	
    	if ($difference < 0) {
    		$difference = 0;
    	}
    	
    	return $difference;
    }
    
    /**
     * 
     */
    public function getDate($format = 'Y-m-d', $string = null)
    {
    	return (is_null($string))
    		? date($format)
    		: date($format, strtotime($string));
    }
    
    /**
     * Reformat time
     */
    public function reformatTime($time, $format)
    {
    	return date($format, strtotime($time));
    }
    
    /**
     * format time
     */
    public function formatTime($time, $format = 'Y-m-d H:i:s')
    {
    	if (!is_numeric($time)) {
    		$time = $this->convertToUnixtime($time);
    	}
    	
    	return date($format, $time);
    }
    
    /**
     * convert to unixtime
     */
    public function convertToUnixtime($time)
    {
    	return strtotime($time);
    }
    
    /**
     * convert from unixtime
     */
    public function convertFromUnixtime($unixtime, $format = 'Y-m-d H:i:s')
    {
    	return date($format, $unixtime);
    }
    
    /**
     * sub
     */
    public function sub($time1 = null, $time2 = null)
    {
    	if ($time1 === null || $time2 === null) {
    		$now = $this->now();
	    	if ($time1 === null) {
	    		$time1 = $now;
	    	}
	    	if ($time2 === null) {
	    		$time2 = $now;
	    	}
    	}
    	
    	$delta = strtotime($time1) - strtotime($time2);
 
    	return $delta;
    }
    
    /**
     * to last mod
     */
    public function toLastMod($delta, $time = null, $format = 'Y-m-d H:i:s')
    {
    	if ($time === null) {
    		$time = $this->now();
    	}
    	
    	$unixtime = strtotime($time);
    	$timeSinceLastMod = $unixtime % $delta;
    	$lastMod = $unixtime - $timeSinceLastMod;
    	
    	return date($format, $lastMod);
    }
    
    /**
     * Get month name
     */
    public function getMonthName($month)
    {
    	return self::$months[(int) $month];
    }
    
	/**
	 * unixtime to mysql datetime
	 */
	public function unixtimeToMysqlDatetime($unixtime)
	{
		if (trim($unixtime) == '') return $unixtime;
		return date('Y-m-d H:i:s', $unixtime);
	}
	
	/**
	 * mysql datetime to unixtime
	 */
	public function mysqlDatetimeToUnixtime($datetime)
	{
		if (trim($datetime) == '') return $datetime;
		return strtotime($datetime);
	}
}