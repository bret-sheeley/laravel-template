<?php

namespace App\Helpers;

use App\Helpers\Traits\DependencyTrait;
use Psr\Log\LoggerInterface;
use Psr\Log\LogLevel;

class Logger extends BaseHelper implements LoggerInterface
{
    use DependencyTrait;
    
    private static $levelValue = 2;
    private static $logDir = null;
    private $filename = 'cerebus';
    private $apiCallId = null;
    private static $levels = [
        1 => LogLevel::DEBUG,
        2 => LogLevel::INFO,
        3 => LogLevel::NOTICE,
        4 => LogLevel::WARNING,
        5 => LogLevel::ERROR,
        6 => LogLevel::CRITICAL,
        7 => LogLevel::ALERT,
        8 => LogLevel::EMERGENCY
    ];

    /**
     * construct
     */
    public function __construct($dependencies = [], $whiteList = ['container'])
    {
        parent::__construct($dependencies, $whiteList);

        $storageDir = env('STORAGE_DIRECTORY');

        $logDir = ($storageDir)
            ? $storageDir . '/logs'
            : '/dev/null';
        $this->setLogDirectory($logDir);

        $level = env('LOG_LEVEL', 'error');
        $this->setLogLevel($level);
    }

    /**
     * isDebug
     */
    public function isDebug()
    {
        return env('APP_DEBUG', false);
    }

    /**
     * log
     *
     * {@inheritdoc}
     *
     * @see \Psr\Log\LoggerInterface::log()
     */
    public function log($logLevel, $message, array $context = [])
    {
        $levelValue = array_search($logLevel, self::$levels);

        // log level check
        if ($levelValue < self::$levelValue)
            return;

        // add data
        $contextString = '';
        if (is_array($context) && count($context) > 0) {
            if (isset($context['exception']) && is_a($context['exception'], '\Exception')) {
                $context['exception'] = $context['exception']->getMessage();
                if ($this->isDebug()) {
                    $context['exception'] .= ', ' . $context['exception']->getTraceAsString();
                }
            }
            $contextString = json_encode($context);
        }

        // get time stamp
        $time = $this->container->get('Time');
        $now = $time->now();
        $date = $time->reformatTime($now, 'Ymd');
        $hour = $time->reformatTime($now, 'YmdH');

        $filename = ($this->isTestMode())
            ? '__test__' . $this->filename
            : $this->filename;

        // build directory based off of date
        $dir = $this->buildDateDirectory($date);

        // Build Full FileName
        $fullFileName = $dir . '/' . $filename . '_' . $hour . '.log';

        // write log message
        $outputString = (trim($contextString) === '')
            ? $message
            : $message . ', ' . $contextString;

        $this->write(strtoupper($logLevel), $now, $outputString, $fullFileName);
    }

    /**
     * build date directory
     */
    public function buildDateDirectory($date)
    {
        $dir = self::$logDir . '/' . $date;
        if (!is_dir($dir)) {
            $oldMask = umask(0);
            mkdir($dir, 0777, true);
            umask($oldMask);
        }

        return $dir;
    }

    /**
     * write
     */
    public function write($logLevel, $now, $outputString, $fullFileName)
    {
        // create file if needed
        if (! file_exists($fullFileName)) {
            touch($fullFileName, 0777);
            chmod($fullFileName, 0777);
        }

        // log
        error_log('[' . strtoupper($logLevel) . '|' . $this->getId() . '|' . $now . '] ' .
            $outputString . PHP_EOL, 3, $fullFileName);
    }

    /**
     * debug
     *
     * @param string $message
     * @param array $context
     */
    public function debug($message, array $context = [])
    {
        $this->logMethod(LogLevel::DEBUG, $message, $context);
    }

    /**
     * info
     *
     * @param string $message
     * @param array $context
     */
    public function info($message, array $context = [])
    {
        $this->logMethod(LogLevel::INFO, $message, $context);
    }

    /**
     * notice
     *
     * @param string $message
     * @param array $context
     */
    public function notice($message, array $context = [])
    {
        $this->logMethod(LogLevel::NOTICE, $message, $context);
    }

    /**
     * warning
     *
     * @param string $message
     * @param array $context
     */
    public function warning($message, array $context = [])
    {
        $this->logMethod(LogLevel::WARNING, $message, $context);
    }

    /**
     * error
     *
     * @param string $message
     * @param array $context
     */
    public function error($message, array $context = [])
    {
        $this->logMethod(LogLevel::ERROR, $message, $context);
    }

    /**
     * critical
     *
     * @param string $message
     * @param array $context
     */
    public function critical($message, array $context = [])
    {
        $this->logMethod(LogLevel::CRITICAL, $message, $context);
    }

    /**
     * alert
     *
     * @param string $message
     * @param array $context
     */
    public function alert($message, array $context = [])
    {
        $this->logMethod(LogLevel::ALERT, $message, $context);
    }

    /**
     * emergency
     *
     * @param string $message
     * @param array $context
     */
    public function emergency($message, array $context = [])
    {
        $this->logMethod(LogLevel::EMERGENCY, $message, $context);
    }

    /**
     * log method
     *
     * @param string $logLevel
     * @param string $message
     * @param array $context
     */
    private function logMethod($logLevel, $message, $context = [])
    {
        $message = $this->prependMethodToMessage($message);
        $this->log($logLevel, $message, $context);
    }

    /**
     * set log directory
     */
    private function setLogDirectory($dir = null)
    {
        if ($dir === null || ! is_dir ( $dir ))
            return;

            self::$logDir = $dir;
    }

    /**
     * set log level
     *
     * @param string $level
     */
    private function setLogLevel(string $level = null)
    {
        if ($level === null)
            return;

        $level = strtolower($level);

        if (in_array($level, self::$levels)) {
            self::$levelValue = array_search ($level, self::$levels);
        }
    }

    /**
     * get Id
     *
     * @return string
     */
    private function getId()
    {
        if ($this->apiCallId == null) {
            $this->apiCallId = $this->container->get('Random')->generateRandomString(32);
        }

        return $this->apiCallId;
    }

    /**
     * prepend method to message
     *
     * @param string $message
     * @return string
     */
    public function prependMethodToMessage(string $message)
    {
        $traceLine = debug_backtrace()[3];
        return $traceLine['class'] . '::' . $traceLine['function'] . ': ' . $message;
    }
}