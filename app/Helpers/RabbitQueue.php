<?php
namespace App\Helpers;

use App\Exceptions\{
    AppException
};

use \AMQPQueue;

class RabbitQueue extends BaseHelper
{
	private $exchange = null;
	private $queueName = null;
	private $routeKey = null;
	private $amqpQueue = null;

	const DUPLICATION_BUFFER = 3;

    /**
     * __construct
     *
     * RabbitQueue constructor.
     * @param $queueName
     * @param $routeKey
     * @param $rabbitExchange
     * @param array $dependencies
     * @param array $whiteList
     * @throws \AMQPChannelException
     * @throws \AMQPConnectionException
     */
	public function __construct($queueName, $routeKey, $rabbitExchange, $queue = null,
                                $dependencies = [], $whiteList = ['container', 'cache', 'logger'])
	{
	    parent::__construct($dependencies, $whiteList);

		$this->queueName = $queueName;
		$this->routeKey = $routeKey;
		$this->exchange = $rabbitExchange;
		$this->amqpQueue = ($queue == null) ? $this->getQueue() : $queue;
	}

    /**
     * generateAmqpQueue
     *
     * @param $channel
     * @return AMQPQueue
     * @throws \AMQPConnectionException
     * @throws \AMQPQueueException
     */
	public function generateAmqpQueue($channel)
    {
        return new AMQPQueue($channel);
    }

    /**
     * init queue
     */
    public function initQueue()
    {
        $channel = $this->exchange->getChannel();
        $queue = $this->generateAmqpQueue($channel);

        $queue->setName($this->queueName);
        $queue->setFlags(AMQP_DURABLE);
        $queue->declareQueue();

        $queue->bind($this->exchange->getExchangeName(), $this->routeKey);

        $this->amqpQueue = $queue;
    }

    /**
     * getQueue
     *
     * @return AMQPQueue|mixed|null
     * @throws \AMQPChannelException
     * @throws \AMQPConnectionException
     */
	public function getQueue()
	{
		if (is_null($this->amqpQueue)) {
			$this->initQueue();
		}

		return $this->amqpQueue;
	}

    /**
     * isDuplicate
     *
     * @param $deliveryTag
     * @param $jsonMessage
     * @return bool
     */
	public function isDuplicate($deliveryTag, $jsonMessage)
	{
		$cacheKey = 'process_' . $deliveryTag;

		$cachedMessage = $this->cache->get($cacheKey);
		if ($cachedMessage == $jsonMessage) {
			$this->logger->info('Delivery Tag ' . $deliveryTag . ' is Duplicate! ' . $cacheKey . ' = ' . $jsonMessage);
			return true;
		}

		$this->cache->set($cacheKey, $jsonMessage, self::DUPLICATION_BUFFER);
		return false;
	}

    /**
     * run
     *
     * @param $handlerName
     * @param null $failureRouteKey
     * @throws AppException
     * @throws \AMQPChannelException
     * @throws \AMQPConnectionException
     */
	public function run($handlerName, $failureRouteKey = null)
	{
		$emptyQueueDelay = env('QUEUE_EMPTY_DELAY', 1);
		$queueProcessThreshold = env('QUEUE_PROCESS_THRESHOLD', 3);

		$handlerNameWithNamespace = '\App\Http\Controllers\Handlers\\' . $handlerName;

		if (!class_exists($handlerNameWithNamespace)) {
			throw new AppException('handler_not_found', 'handler', 'Provided Handler Name (' . $handlerName . ') not found.');
		}

		$handler = new $handlerNameWithNamespace;

		$this->logger->info('Start handler: ' . $handlerName);

		while(true) {
			$envelope = $this->amqpQueue->get();
			if ($envelope !== false) {

				$jsonMessage = $envelope->getBody();
				$deliveryTag = $envelope->getDeliveryTag();

				if ($this->isDuplicate($deliveryTag, $jsonMessage)) {

					// duplicated messages are instantly failed.
					$this->amqpQueue->ack($deliveryTag);

					if (!is_null($failureRouteKey)) {
                        $this->logger->info('CRITICAL ERROR: Push failed message to failure queue ' . $failureRouteKey .  ',   deliveryTag: ' . $deliveryTag);
						$this->exchange->publish($jsonMessage, $failureRouteKey);
					}
					continue;
				}

				$numberOfAttempts = 0;

                $this->logger->info('Process message with delivery tag: ' . $deliveryTag);

				while (true) {

					try {
						$message = json_decode($jsonMessage, true);
                        $this->logger->info('Send message: ' .  $jsonMessage . ', deliveryTag = ' . $deliveryTag . ', handler = ' . get_class($handler));
						$status = $handler->process($message);
                        $this->logger->info('Message Processed: ' . $deliveryTag);
					} catch (Exception $e) {
						$status = false;
                        $this->logger->info('Message Failed: ' . $e->getMessage());
					}

					if ($status === true) {
                        $this->logger->info('Acknowledge message with delivery tag: ' . $deliveryTag);
						$this->amqpQueue->ack($deliveryTag);
						break;
					} else {
                        $this->logger->info('Message failure #' . $numberOfAttempts . ' with delivery tag: ' . $deliveryTag);

						$numberOfAttempts++;

						if ($numberOfAttempts >= $queueProcessThreshold) {
                            $this->logger->info('Message failure reached threshold ' . $queueProcessThreshold .  ',   deliveryTag: ' . $deliveryTag);
							$this->amqpQueue->ack($deliveryTag);

							if (!is_null($failureRouteKey)) {
                                $this->logger->info('Push failed message to failure queue ' . $failureRouteKey .  ',   deliveryTag: ' . $deliveryTag);
								$this->exchange->publish($jsonMessage, $failureRouteKey);
							}

							break;
						}
					}
				}

			} else {
				sleep($emptyQueueDelay);
			}
		}

		// one last chance to ack if something went wrong
		if ($status !== true) {
			$this->amqpQueue->ack($deliveryTag);
		}
	}
}