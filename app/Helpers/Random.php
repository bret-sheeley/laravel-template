<?php
namespace App\Helpers;

class Random extends BaseHelper
{
    const ALPHANUMERIC = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    const HEXADECIMAL = '0123456789abcdef';
    /**
     * generate unique token
     */
    public function generateUniqueToken($maximumSubstringLength = 8, $characters = '0123456789abcdef')
    {
        $maximumCharacterIndex = strlen($characters) - 1;
        
        $microtimestamp = $this->container->get('Time')->microtimestamp();
        
        for ($index = 0; $index < $maximumSubstringLength; $index++) {
            $microtimestamp .= $characters[rand(0, $maximumCharacterIndex)];
        }
        
        return $microtimestamp;
    }
    
    /**
     * generate random string
     */
    public function generateRandomString($length = 256, $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ')
    {
        $maximumCharacterIndex = strlen($characters) - 1;
        
        $string = '';
        for ($i = 0; $i < $length; $i++) {
            $string .= $characters[rand(0, $maximumCharacterIndex)];
        }
        
        return $string;
    }
}