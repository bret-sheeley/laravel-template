<?php
namespace App\Helpers;

use Aws\Ses\SesClient;

use App\Exceptions\{
	AppException
};
use App\Helpers\Traits\{
    EnvironmentTrait,
    SendGridTrait
};

use \Exception;

class EmailWrapper extends BaseHelper
{
    use EnvironmentTrait;
    use SendGridTrait;

    const FROM_NAME = 'Wellevate';

    const EMAIL_SENDGRID = 'sendgrid';
    const EMAIL_SES = 'ses';
    const EMAIL_MAIL = 'mail';

	const RESET_PASSWORD = 'reset-password';

    /**
     * constructor
     */
    public function __construct($dependencies = null, $whitelist = ['container', 'config', 'factory', 'logger', 'queue'])
    {
        parent::__construct($dependencies, $whitelist);
    }

    /**
     * get ses client
     */
    public function getSesClient($args = [])
    {
        return SesClient::factory($args);
    }

    /**
     * sendEmail
     */
    public function sendEmail($to, $subject, $html, $headers)
    {
        mail($to, $subject, $html, implode('\r\n', $headers));
    }

	/**
	 * send email
	 */
	public function send($to, $subject, $view, $data, $ccs = [], $bccs = [])
	{
		$this->logger->debug('start');

		// don't send emails out during automated tests
		if ($this->isTestMode()) return;

		$this->logger->debug('To: ' . $to . ', Subject: ' . $subject . ', View: ' . $view);

		try {
            $emailSystem = $this->getEmailSystem();
			$supportEmail = $this->getEnv('SUPPORT_EMAIL_ADDRESS');
			$fromEmail = $this->getEnv('EMAIL_FROM_ADDRESS');

			$html = $this->renderView($view, $data);

			if ($emailSystem == self::EMAIL_SENDGRID) {

				$categories = [$view];

				if ($this->isEnvDefined('APP_ENVIRONMENT')) {
					$categories[] = $this->getEnv('APP_ENVIRONMENT');
				}

				$email = $this->buildSendGridMail(
					null,
					$to,
					self::FROM_NAME,
					$fromEmail,
					$supportEmail,
					$subject,
					'text/html',
					$html,
					$categories,
					$ccs,
					$bccs
				);

				$this->logger->debug('post email');
				$this->getSendgrid()->client->mail()->send()->post($email);

			} else if ($emailSystem == self::EMAIL_SES){

				$client = $this->getSesClient([
					'credentials' => [
						'key' => $this->getEnv('AWS_ACCESS_KEY'),
						'secret' => $this->getEnv('AWS_SECRET_KEY')
					],
					'region' => 'us-west-2',
					'version' => 'latest'
				]);

				$structure = [
					'Source' => $fromEmail,
					'Destination' => [
						'ToAddresses' => [$to]
					],
					'Message' => [
						'Subject' => [
							'Data' => $subject
						],
						'Body' => [
							'Html' => [
								'Data' => $html,
								'Charset' => 'UTF-8'
							]
						]
					]
				];

				if (is_array($ccs) && count($ccs) > 0) {
					$structure['Destination']['CcAddresses'] = $ccs;
				}
				if (is_array($bccs) && count($bccs) > 0) {
					$structure['Destination']['BccAddresses'] = $bccs;
				}

				$client->sendEmail($structure);
			} else {
                $headers = [];
                $headers[] = 'MIME-Version: 1.0';
                $headers[] = 'Content-type: text/html; charset=iso-8859-1';
                $headers[] = 'From: ' . $fromEmail;
                $headers[] = 'X-Mailer: PHP/' . phpversion();

                if (count($ccs) > 0) {
                    $headers[] = 'Cc: ' . implode(', ', $ccs);
                }
                if (count($ccs) > 0) {
                    $headers[] = 'Bcc: ' . implode(', ', $bccs);
                }

                $this->sendEmail($to, $subject, $html, $headers);
            }

		} catch (Exception $e) {
			$this->logger->error('error message: ' . $e->getMessage());

			if ($emailSystem == self::EMAIL_SENDGRID) {
				$this->resetSendgrid();
			}
			throw $e;
		}

		$this->logger->debug('end');
	}

	/**
	 * queue
	 */
	public function queue($to, $emailType, $data = array(), $ccs = array(), $bccs = array())
	{
		if ($this->isTestMode()) return;

		$message = array(
			'to' => $to,
			'email_type' => $emailType,
		);

		if (is_array($ccs) && count($ccs) > 0) {
			$message['ccs'] = $ccs;
		}

		if (is_array($bccs) && count($bccs) > 0) {
			$message['bccs'] = $bccs;
		}

		if (is_array($data) && count($data) > 0) {
			$message['data'] = $data;
		}

		$this->queue->publishMessage(RabbitWrapper::APP_EXCHANGE,
            RabbitWrapper::EMAIL_ROUTE_KEY,
            $message);
	}

	/**
	 * processMessage
	 */
	public function processMessage($to, $emailType, $data = [], $ccs = [], $bccs = [])
	{
		switch ($emailType)
		{
			case self::RESET_PASSWORD:
				$subject = 'Forgotten Password?';
				$view = 'emails.user.forgotten-password';
				break;

			default:
				throw new AppException('invalid_email_type',
                    'Email Type "' . $emailType . '" for ' . $to);
		}

		$this->send($to, $subject, $view, $data, $ccs, $bccs);
    }
}