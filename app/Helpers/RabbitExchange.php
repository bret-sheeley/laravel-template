<?php
namespace App\Helpers;

use \AMQPConnection;

use \Exception;

class RabbitExchange extends BaseHelper
{
    private $conn = null;
    private $channel = null;
    private $exchange = null;
    private $exchangeName = null;

    /**
     * Connect to queue exchange
     */
    private function connect()
    {
    	$vhost = $this->getVhost();
    	$host = env('QUEUE_HOST');
    	$port = env('QUEUE_PORT');
    	$username = env('QUEUE_USERNAME');
    	$password = env('QUEUE_PASSWORD');

    	$conn = new AMQPConnection();
    	$conn->setVhost($vhost);
    	$conn->setHost($host);
    	$conn->setPort($port);
    	$conn->setLogin($username);
    	$conn->setPassword($password);

    	$success = $conn->pconnect();

    	return ($success) ? $conn : null;
    }

    public function __construct($exchangeName)
    {
        parent::__construct();

    	$this->exchangeName = $exchangeName;
        $this->conn = $this->getConnection();
        $this->channel = $this->getChannel();
		$this->exchange = $this->getExchange();
    }

    public function __destruct()
    {
    	try {
	        // Unset the reference to the connection.
    	    if (isset($this->conn)) {
        		$this->conn->disconnect();
            	unset($this->conn);
        	}
    	} catch (Exception $e) {}
    }

    /**
     * get channel
     *
     * @return \AMQPChannel
     */
    public function getChannel()
    {
    	if (is_null($this->channel)) {
	    	$this->channel = new \AMQPChannel($this->conn);
    	}

    	return $this->channel;
    }

    /**
     * Get Connection
     */
    public function getConnection()
    {
    	if (is_null($this->conn)) {

	        // Get the configuation
	        $connAttempts = env('QUEUE_CONNECTION_ATTEMPTS', 5);
	        $connSleep = env('QUEUE_CONNECTION_SLEEP', 1);

	        for ($i = 0; $i < $connAttempts; ++$i) {

	            try {
	                $this->conn = $this->connect();
	                if ($this->conn instanceof \AMQPConnection) {
	                    break;
	                }
	            } catch (\Exception $e) { }

	            usleep((int) $connSleep);
	        }

	        if (!($this->conn instanceof \AMQPConnection)) {
	            throw new \Exception('Could not connect to AMQP server after ' . $connAttempts . ' attempts.');
	        }
    	}

        return $this->conn;
    }

    /**
     * Lazy Load Exchange
     */
    public function getExchange()
    {
        // Singleton for the exchange.
        if (!isset($this->exchange) || !($this->exchange instanceof AMQPExchange)) {
            $exchange = new \AMQPExchange($this->channel);
            $exchange->setName($this->exchangeName);
            $exchange->setType(AMQP_EX_TYPE_TOPIC);
            $exchange->setFlags(AMQP_DURABLE);
        	$exchange->declareExchange();
            $this->exchange = $exchange;
        }
        return $this->exchange;
    }

    /**
     * Get Name
     */
    public function getExchangeName()
    {
    	return $this->exchangeName;
    }

    /**
     * getVhost
     */
    public function getVhost()
    {
    	return ($this->isTestMode()) ? 'test' : env('QUEUE_VHOST');
    }

    /**
     * Publish
     */
    public function publish($message, $routeKey)
    {
		if (is_array($message) || is_object($message)) {
			$message = json_encode($message);
		}

		$this->logger->info('Publish Message to ' . $routeKey . ': ' . $message);

		$this->exchange->publish($message, $routeKey);
    }
}
?>
