<?php
namespace App\Helpers\Traits;

trait SendGridTrait
{
    private $sendgrid = null;

    /**
     * Get Sendgrid
     */
    public function getSendgrid()
    {
        if ($this->sendgrid === null) {

            $apiKey = env('SENDGRID_KEY');

            /*
            $options = array(
                'turn_off_ssl_verifiction' => true
            );
            */

            $this->sendgrid = new \SendGrid($apiKey);
        }

        return $this->sendgrid;
    }

    /**
     * Reset sendgrid
     */
    public function resetSendgrid()
    {
        unset($this->sendgrid);
        $this->sendgrid = null;
    }

    /**
     * Get SendGrid Email
     */
    public function getSendGridEmail($name, $email)
    {
        return new \SendGrid\Email($name, $email);
    }

    /**
     * Get SendGrid Personalization
     */
    public function getSendGridPersonalization()
    {
        return new \SendGrid\Personalization();
    }

    /**
     * Get SendGrid Content
     */
    public function getSendGridContent($type, $message)
    {
        return new \SendGrid\Content($type, $message);
    }

    /**
     * Get SendGrid Mail
     */
    public function getSendGridMail($from, $subject, $to, $content)
    {
        return new \SendGrid\Mail($from, $subject, $to, $content);
    }

    /**
     * Get SendGrid Reply To
     */
    public function getSendGridReplyTo($replyTo)
    {
        return new \SendGrid\ReplyTo($replyTo);
    }

    /**
     * Get SendGrid Mail
     */
    protected function buildSendGridMail($toName, $to, $fromName, $from,
                                    $replyTo, $subject, $contentType,
                                    $message, $categories = [],
                                    $ccs = [], $bccs = [])
    {
        $toObject = $this->getSendGridEmail($toName, $to);
        $fromObject = $this->getSendGridEmail($fromName, $from);
        $content = $this->getSendGridContent($contentType, $message);

        $mail = $this->getSendGridMail($fromObject, $subject, $toObject, $content);

        if (trim($replyTo) != '') {
            $replyToObject = $this->getSendGridReplyTo($replyTo);
            $mail->setReplyTo($replyToObject);
        }

        foreach ($categories as $category) {
            $mail->addCategory($category);
        }

        if (is_array($ccs) && count($ccs) > 0) {
            $personalization = $this->getSendGridPersonalization();

            foreach ($ccs as $cc) {
                $ccObj = $this->getSendGridEmail(null, $cc);
                $personalization->addTo($ccObj);
            }

            $mail->addPersonalization($personalization);
        }

        if (is_array($bccs) && count($bccs) > 0) {
            $personalization2 = $this->getSendGridPersonalization();

            foreach ($bccs as $bcc) {
                $bccObj = $this->getSendGridEmail(null, $bcc);
                $personalization2->addTo($bccObj);
            }
            $mail->addPersonalization($personalization2);
        }

        return $mail;
    }

}