<?php
namespace App\Helpers\Traits;

use App\Helpers\{
    CacheWrapper,
    DependencyContainer,
    Factory,
    Logger,
    RabbitWrapper
};

trait DependencyTrait
{
    /** @var CacheWrapper */
    protected $cache;

    /** @var DependencyContainer */
    protected $container;

    /** @var Factory */
    protected $factory;

    /** @var Logger */
    protected $logger;

    /** @var RabbitWrapper */
    protected $queue;

    /**
     * @param $dependencies
     * @param array $whitelist
     */
    public function setDependencies($dependencies, $whitelist = ['container', 'factory', 'logger', 'queue'])
    {
        if (in_array('cache', $whitelist)) {
            $this->cache = (is_object($dependencies) && property_exists($dependencies, 'cache'))
                ? $dependencies->cache : new CacheWrapper();
        }

        if (in_array('container', $whitelist)) {
            $this->container = (is_object($dependencies) && property_exists($dependencies, 'container'))
                ? $dependencies->container : new DependencyContainer();
        }
        
        if (in_array('factory', $whitelist)) {
            $this->factory = (is_object($dependencies) && property_exists($dependencies, 'factory'))
                ? $dependencies->factory : new Factory();
        }

        if (in_array('logger', $whitelist)) {
            $this->logger = (is_object($dependencies) && property_exists($dependencies, 'logger'))
                ? $dependencies->logger : new Logger();
        }

        if (in_array('queue', $whitelist)) {
            $this->queue = (is_object($dependencies) && property_exists($dependencies, 'queue'))
                ? $dependencies->queue : new RabbitWrapper();
        }
    }
}