<?php
/**
 * Created by PhpStorm.
 * User: bretsheeley
 * Date: 5/25/18
 * Time: 6:17 PM
 */

namespace App\Helpers\Traits;


trait EnvironmentTrait
{
    /**
     * get env
     */
    public function getEnv($tag)
    {
        return env($tag);
    }

    /**
     * is env defined
     */
    public function isEnvDefined($tag)
    {
        return defined($tag);
    }

    /**
     * get email system
     */
    public function getEmailSystem()
    {
        return env('EMAIL_SYSTEM', 'mail');
    }

}