<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class SubscriberKeepAlive extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'subscriber:keep-alive {queue} {mode=prod}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Subscriber Keep Alive';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $queueName = $this->argument('queue');
        $applicationMode = $this->argument('mode');

        $artisanDir = realpath(__dir__ . '/../../../');

        $cmd = 'php ' . $artisanDir . '/artisan subscriber:run ' . $queueName . ' ' . $applicationMode;
        while (true) {
            \system($cmd);
            \sleep(1);
            echo 'Restarting daemon';
        }
    }
}
