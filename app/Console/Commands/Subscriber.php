<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class Subscriber extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'subscriber:run {queue} {mode=prod}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run Rabbit Subscriber';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $queueName = $this->argument('queue');
        $applicationMode = $this->argument('mode');

        define('APPLICATION_MODE', $applicationMode);
        define('APP_PATH', realpath(__dir__ . '/../..'));
        define('TEST_PATH', realpath(__dir__ . '/../../../tests'));

        if ($applicationMode === 'test') {
            define('APPLICATION_DATABASE', '__test_vide');
            define('SESSION_DATABASE', '__test_vide_session');
            define('ANALYTIC_DATABASE', '__test_vide_analytics');
        }

        echo 'Run Subscriber for queue: ' . $queueName . ', in applicationMode: ' . $applicationMode . PHP_EOL;
        echo PHP_EOL;

        $rabbitWrapper = new \App\Helpers\RabbitWrapper();
        $rabbitWrapper->runSubscriber($queueName);
    }
}
