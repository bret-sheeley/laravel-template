<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class InitStorage extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'storage:init';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Initializes Storage Directory Structure';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * init directory
     *
     * @param string $path
     * @param int $mode
     * @param boolean $recursive
     */
    public function initDirectory($path, $mode = 0777, $recursive = true)
    {
        if (!is_dir($path)) {
            mkdir($path, $mode, $recursive);
        }
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $storageDirectory = env('STORAGE_DIRECTORY');
        if ($storageDirectory) {
            $this->initDirectory($storageDirectory);
            $this->initDirectory($storageDirectory . '/logs');
            $this->initDirectory($storageDirectory . '/framework/sessions');
            $this->initDirectory($storageDirectory . '/framework/views');
            $this->initDirectory($storageDirectory . '/framework/cache');
        }
    }
}
