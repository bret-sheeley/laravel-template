<?php
namespace App\Models;

class EscapeException extends \Exception
{
    /**
     * construct
     */
    public function __construct($message = null, $code = 0) 
    {   
        parent::__construct($message, $code, null);
    }
}