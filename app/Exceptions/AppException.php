<?php
namespace App\Exceptions;

class AppException extends \Exception
{
	private $_code = null;
    private $_type = null;
    private $_privateMessage = null;
    private $_data = array();

    /**
     * construct
     */
    public function __construct($message = null, $privateMessage = null, $data = [], $code = 500)
    {
        parent::__construct($message, $code, null);
        if (is_null($privateMessage)) {
            	$privateMessage = $type;
        }
        $this->_code = $code;

        $this->_privateMessage = $privateMessage;

        if (is_array($data) && count($data) > 0) {
	        $this->_data = $data;
        }
    }

    /**
     * get data
     */
    public function getData()
    {
        return $this->_data;
    }

    /**
     * get private message
     */
    public function getPrivateMessage()
    {
        return $this->_privateMessage;
    }

    /**
     * toArray
     *
     * @return array
     */
    public function toArray()
    {
        $data = $this->getData();

        $data['error'] = $this->getMessage();

        return $data;
    }
}